---
title: Your first 90 days as CTO or VP Engineering.
date: 2020-01-13
---
[lethain.com](https://lethain.com/first-ninety-days-cto-vpe/)

Your first 90 days as CTO or VP Engineering.
============================================

24-30 minutes

* * *

Whenever I transition to a new opportunity, I think about how to _start well_. How can I ramp up as effectively as possible? How do I balance the urge to “show value” early with making the right decisions?

The canonical book on starting a new role is [The First 90 Days](https://www.amazon.com/dp/B00B6U63ZE/ref=dp-kindle-redirect?_encoding=UTF8&btkr=1), and I got a fair amount out of it when I read it six or seven years ago. That said, its advice skews generic at times. Yes, I should learn as much as possible. Yes, I should achieve alignment and negotiate for resources. Yes, I should build a team and partner with other organizations. In my first leadership role, these ideas reshaped my perspective, but these days I want more.

For my next role transition, happening later this month, I wanted a set of tactical best practices for starting as a Chief Technology Officer or VP of Engineering, so I did some research and collected these recommendations.

VPE vs CTO
----------

One challenge of writing about these roles is that different companies define VP Engineering (VPE) and CTO roles very differently.

Some companies treat the CTO as the senior-most individual contributor (e.g. non-manager), along the lines of a Chief Architect who is also part of the senior leadership team. Other companies view the CTO as the senior-most engineering contributor, responsible for both people and technical aspects of the engineering organization, in addition to being involved in the broader business and strategy as a member of the senior leadership team.

The same ambiguity exists for VP of Engineering roles. Some companies treat the VPE as the engineering people leader, reporting into the CTO. Or reporting into the CEO. Others still treat the VPE as a proto-CTO, operating along the lines of one of the CTO models but wanting to keep the CTO role open for potentially hiring a more senior leader later on.

For these recommendations, I’m going to focus on the broadest possible definition: an individual who is a member of the executive team, is responsible for technical execution, and is responsible for people management within the engineering organization.

Priorities and goals
--------------------

Every role will have a different set of priorities and different means to measure them. What’s most important for a CTO starting at Series A, a pre-IPO and Google are going to vary _a lot_. That said your discovery process – figuring out what is important – requires some structure, and I’d offer up these priorities as a starting place:

1.  **How does the business work?** Where does the money come from? Where does the money go? How much money do you have in the bank? What needs to be true in the next year for the business to make a step function increase in value? What are the knowledge gaps between folks in engineering and folks operating the business? In some cases you’ve never operated in the business’ domain before, e.g. having to learn how banking or real estate works, and in those cases it’s important to dig in there as well.
2.  **What defines the culture?** What are the company’s true values? How do decisions really get made? Who is valued and why? Whose role has grown and whose role has stagnated?
3.  **Healthy relationships with peers and stakeholders.** What do your peers need from engineering and how you can help them succeed? What are the stakes that define success for your stakeholders? How can you build a relationship _before_ your first source of conflict?
4.  **Team is executing effectively on the right work.** How does an idea turn into finished work? How does work get assigned? Who steps in to handle emergencies?
5.  **Technical quality is high.** How well are tools supporting the team’s day-to-day work? What are the [key technical behaviors and properties](https://lethain.com/reclaim-unreasonable-software/)? What projects are considered impossible due to technical limitations?
6.  **Team is inclusive, with high morale.** Who is successful within your team? Who isn’t finding success? Why? What are the active inclusion efforts, and who leads them? Is that work valued? What is energizing or stealing energy from the team?
7.  **Pace is sustainable for the long-haul.** What do you need to stay engaged and energized for the long-haul? What are lines you’ll temporarily cross that you’ll explicitly walk back after you’ve ramped up?

For each of those priorities, identify a couple of measurable goals that’ll be useful in tracking your long-term progress. The act of identifying the specific goals is part of the learning process, but you could imagine measuring team execution using both “number of experiments run” and [the productivity metrics from Accelerate](https://lethain.com/accelerate-developer-productivity/). Team health can be measured with skip-levels, CultureAmp results, coffee discussions with cohorts within the team, and so on. Pace is tricky, but you can identify what you personally need for work to be sustainable and measure how frequently you’re satisfying those conditions – one executive I’ve worked with described needing one 30 minute working block each day to stay engaged, maybe for you it’s more or something different, and that’s fine!

Making the right system changes
-------------------------------

Equipped with priorities and goals, folks often want to jump into making changes, which will become the right instinct soon, but will lead you astray in the moment. Your goal as a senior leader is to make _durable_ improvements towards these goals, which results not from making changes but making the _right_ changes. Further, _durable_ improvements depend on _creating systems_ that create changes, not performing tactical actions that create the ephemeral appearance of improvement.

These system changes are slow to show effect for good or ill, and your organization can only tolerate the overhead of adopting a small number concurrently. This is why to be a truly effective executive you must start by understanding the organizations and systems at play before you move towards changing them.

If you skip understanding, you can _emulate_ _success_ in the short-term, but you’ll be back to working on the same problems a few months later with less trust and feedback from the team.

* * *

If we had to create a short list of the top onboarding traps that new executives fall into, rushing to make changes before understanding the problem’s shape is undoubtedly the first. The two other frequent mistakes are judging without context, “Ah, this technology is terrible, what sort of fools made this decision?” and the infuriating refrain of “At my last job, we …”.

Tasks for your first 90 days
----------------------------

When you read the literature, the platonic ideal of your first ninety days is time spent exclusively on learning, preparing for measured, effective execution on the ninety-first day. Depending on your circumstances, this might be feasible, but it might be misguided.

If you start the learning process and uncover something deeply unwell, you have my permission to _stop going down the list_ and instead spend your time on getting that thing fixed. In that case, you might get around to some of these recommendations nine months instead of three, and that’s ok.

Even if you focus on firefighting an emergency, try to avoid spending _all_ your time on that fire. Executive leadership requires succeeding in both the short and long-term, so don’t get so distracted by the joy of fixing emergencies that you forgo the strategic.

Another high-order bit when you consider your initial engagement is the size and complexity of the company you’re joining. A twenty person company’s complexity shouldn’t take you three months of learning, whereas a two thousand person company likely will. Like all rules, adapt them to your context.

### Learning and building trust

Once you’ve started establishing your support network outside of the company, the next critical step is establishing your support network within the company by meeting the folks you’ll be working with, understanding how the business and company works, and learning as much as possible.

1.  **Ask your manager – probably the CEO or CTO – to write their explicit expectations for you.** The classic question to ask is “What will success look like for my role?” The answers may be very broad, along the lines of “go figure out how to be useful”, which is totally fine, but if they do have more explicit expectations you should get them clearly articulated.
2.  **Figure out if something is _really_ wrong and needs immediate attention.** Companies are surprisingly resilient, enduring wounds that at first seem unbearable, but in some cases the organization you’re joining has a dire problem, and in that case you want to shift away from this generalized approach to focus on downgrading from dire to distressed. (In some cases folks are so familiar with the dire problem that they’ve forgotten it, which can be a jarring experience.)
3.  **Go on a listening tour**. If your organization is less than thirty, aim to meet with each individual privately over the first ninety days. As it gets larger, you’ll need to have a mix of team and individual meetings to condense meeting more folks into the same time period. Get out of your organization as well, get time to meet folks across the company using more informal opportunities like lunches for folks that you won’t work with as directly.
4.  **Setup recurring one-on-ones and skip-levels.** You’ll need to establish your cadence for one-on-ones and skip-level one-on-ones early, as this is a key way to learn from the team.Decide on the amount of time per week you want to spend on skip-levels instead of a target frequency, to keep from being overwhelmed as the organization grows. Some executives suggested waiting until after the first ninety days before scheduling recurring meetings to support learning broadly before learning deeply – it’s important to start recurring one-on-ones early, but there’s some room here to experiment. When you’re establishing these one-on-ones, two important aspects to remember are meeting with your peers and stakeholders – not just your team – and getting to know each other as humans, not just as entwined functional gears.
5.  **Share what you’re observing.** Part of earning trust in your new organization is to simultaneously show that you’re listening and that you’re not jumping to judgement. I’ve found [weekly emails](https://lethain.com/weekly-updates/) to be an effective format for this, but you could also use a monthly “all hands” meeting or other venue.
6.  **Attend routine forums.** Start attending existing forums and observe how they work. This works well for forums outside of your organization, but generally the presence of a new executive “breaks” meetings that you’re expected to start leading such as your team’s staff meeting. I find it’s still helpful to ask folks to keep running it as it was for the first several iterations, but just be aware that your presence fundamentally changes the experience.
7.  **Shadow support tickets.** Many companies exert a powerful reality distortion field on how things are actually going, but customers are always a reality reservoir. The most scalable way to hear from your customers early on is plugging into the support tickets. If you have one, shadow your customer success team reviewing incoming tickets, if not get a Zendesk login and do some digging yourself.
8.  **Shadow customer meetings, partner meetings or user testing.** Exactly which of these you’ll want will depend on the kind of product and business you’re running. The priority is learning how the company interfaces with the external folks who are essential to their success.
9.  **Find your business analytics and how to query them.** Find where your business analytics are stored, probably a data warehouse, and learn how to run your own queries. You shouldn’t run around assuming your queries are right – data is a subtle liar – but it’s important to be able to pull your own data enough to perform initial explorations when you run into interesting problems.

### Support system

The further you get into your career, the fewer folks at your company will have filled your current role, and the more important it becomes to build a network of support that extends beyond your current team, peers and managers. So many of the decisions that you’ll make in these senior roles are slow to show results, which makes taking advantage of others’ experience even more important.

1.  **Build an external support group of folks in similar roles.** In senior roles there is no one else performing your role within your company, so you have to start looking externally for role models and mentors. Find a small leadership community to join, whether that’s a private Slack team, a weekly breakfast group, or something else. Having a broad group to run your challenges by will vastly accelerate your learning.
2.  **Get an executive coach**. If you take the benefits of an extended support group and compacted them into a small diamond, that’s the experience of working with a great executive coach. They’ve seen or heard of a dozen folks going through the same challenge you’re experiencing, they understand your personal context, and they’re paid to give you feedback that you may not be comfortable hearing. (An ever so slightly contrarian take that one executive mentioned is that executive coaches are most helpful once you already have the working context, so you might want to engage with them a bit later as opposed to in the first three months.)
3.  **Create space for self-care**. Invest in yourself so that you have the energy to invest in others. Attend therapy, prioritize getting enough sleep, schedule regular physical exercise.

It can be tempting to skip or postpone these, but take advantage of the magical time when you’re starting something new and your patterns are being reset. Reaching out with, “Hey, I’m starting a new role that you’ve been doing well for a long time, and I’d love to run these couple of questions by you!” works surprisingly well when combined with a short, well-structured question.

### Organizational health and process

Many management roles train their inhabitants to avoid engaging with organizational processes because of undue friction preventing improvement and change, but it’s essential that folks in senior roles don’t carry that mentality forward with them. You can’t change organizational processes overnight – it takes many hands and great attention to detail to make effective organizational change – but it’s essential that you’re paying attention from the beginning and identify any structural gaps to address.

1.  **Document existing organizational process.** Many organizational processes are undocumented outside of the minds operating them. Write down the processes that you run into, and use those documents to ensure you’re understanding them (by checking them against folks on the team) and to ease starting for the next hires.
2.  **Implement at most one or two changes.** As you understand the organization, you’ll identify a long list of changes you think might help. Winnow that list down to one or two, work with the existing team to align it to their challenges, and then continue [to evolve that process until it works](https://lethain.com/good-process-is-evolved/).
3.  **Plan organizational growth for next year.** Most organizations are missing a clear document describing the current size, the target size next year, and the series of translations that will occur to evolve the organization along the way. Write that plan, in particular identifying the critical roles that will need to be filled.
4.  **Setup communication pathways.** Setup a monthly Q&A so folks can ask you questions, start sending [weekly 5-15s](https://lethain.com/weekly-updates/) to make it easier for folks to track your attention and progress, setup mailing lists – the details here will depend on how the company wants to communicate, but establish a few clear paths and communicate them out to your team.
5.  **Pay attention to non-engineering roles within your org.** It’s common for functions beyond software engineering to exist within an engineering organization, such as [TPM or SRE](https://lethain.com/specialized-roles/). Go spend time with them! These functions are always remarkable impactful and generally feel ignored, invest time into listening to them.
6.  **Spot check organizational inclusion.** When you first get started, it’s easy to spend most of your time on the folks getting the most support, which can give you a misleading sense of the organization’s health. Keep your eyes open by doing a [compensation review](https://increment.com/teams/pay-fair/). Check in folks who work [outside of the office you’re joining](https://increment.com/teams/a-guide-to-distributed-teams/), particularly folks who work from home.

### Hiring

Most senior leaders identify hiring as their most important contribution, and as a functional leader hiring is a key focus for you on two different dimensions: personally sourcing and closing senior candidates, and structuring an effective overall engineering hiring process.

Good areas to start understanding are:

1.  **Shadow existing interviews, onboarding and closing calls.** Watch the existing interview process and get a sense for how it works. Keep in mind that your presence in the room is going to change things.
2.  **Decide if an overhaul is necessary.** If you haven’t previously had a strong recruiting or recruiting-oriented engineering leader, then it’s possible that there simply isn’t an existing process to work from, in which case you’ll have to lay out the pieces from scratch. Don’t overfit on preserving what exists if even the existing team doesn’t think it’s worth salvaging.
3.  **Identify your (three or fewer) key missing roles.** You only have so much recruiting bandwidth and each role you’re hiring for has a fixed overhead to recruit against. This means your overall hiring velocity is the highest when you focus the most, and that requires identifying a small number of key roles to hire against. If every hire is a priority, nothing is a priority.
4.  **Track [funnel metrics](https://lethain.com/hiring-funnel/) and hiring pipeline.** Your hiring funnel metrics are the doorway to understanding your hiring process and where to invest into your hiring process. Getting to a place where you can review those metrics and hiring pipeline is a key transition from a recruiting process that _happens_ to a recruiting process that _learns_. You’ll want to get a recurring recruiting-oriented meeting onto the calendar as well, either weekly or bi-weekly that pulls recruiters and hiring managers into a room to operate and improve together.
5.  **Offer to close priority candidates.** Jumping onto closing calls for high priority candidates is one of the earliest ways that a new executive can help the team. In addition, these are invaluable for building understanding into your recruiting process and company brand.
6.  **Kickoff engineering brand efforts.** Part of being effective engineering recruiting is building the brand around your engineering efforts that make folks excited to join. This is a slow process, which makes it particularly valuable to start early. This is getting an engineering blog up, getting the team speaking at conferences, establishing an active presence on Twitter, and so on – all with the focus of exciting folks to work with your team.

### Execution

Engineering’s long-term value to a company is opening up new avenues of potential, but the short-term value is continued execution on the product and company roadmap. The biggest mistake a new leader can make is disrupting the current systems of execution before they have a working alternative. Instead, start with the approach of keeping what works, iterating on what doesn’t, and measuring along the way.

1.  **Figure out if what’s happening now is working and scales.** Before getting into measurement, take a moment to understand how folks feel about the existing process and where is or isn’t working. There are so many different approaches to execution that folks often start recreating what they’ve done previously rather than listening.
2.  **Establish internal measures of engineering velocity.** I’d recommend starting with the measures from [Accelerate](https://lethain.com/accelerate-developer-productivity/), although it’ll require significant tooling and process investment to make those apply in some cases, particularly mobile development.
3.  **Establish team-external measure of velocity.** It’s important that some of your measures for velocity in terms of business impact (revenue growth) or inputs to business impact (experiments conducted). Broad definitions keep you thinking broadly, considering how all the business’ pieces fit together.
4.  **Consider adding a small amount of process and controls.** While I don’t recommend changing _a lot_, the two changes that I’d suggest considering are adding an operational review meeting on a weekly cadence and [moving into an organizational structure](https://lethain.com/running-an-engineering-reorg/) that cleans up any structural impediments to execution. If you do make a structural change, make sure that the new structure is durable for _at least_ another year.

### Technology

Supporting, curating and advocating for the company’s approach to technology is an important part of the role. It is _slightly_ more common for folks to engage in major technology changes after the first ninety days, but once again it comes back to identifying if there are critical changes that need to be made now rather than later.

1.  **Is the existing technology effective?** Ask folks working with your company’s various technology stacks how well the tools are serving their needs. Where are the rough edges that are stealing their time?
2.  **How are high-impact technical decisions made?** Some companies have [technology reviews or architecture groups](https://lethain.com/scaling-consistency/) that make large or controversial technical decisions, but even more rely on informal mechanisms. Study how these work in your new organization.
3.  **Build a trivial change and deploy it.** It’s extremely valuable to build up your mental model of how development works within your company, and the best way to do this is occasionally making a small change and shepherding it all the way out to production.
4.  **Do an on-call rotation.** You probably don’t have enough context to help in an incident, but you can add yourself on Pagerduty (or whatnot) as an additional page target and get a sense of the experience of being on-call.
5.  **Attend incident review.** Smaller companies may not have [an incident program](https://lethain.com/incident-response-programs-and-your-startup/) yet, but if they do, these meetings and the reports are a uniquely valuable source of learning, and you should attend them.
6.  **Record the technology history.** It’s at best counterproductive to challenge folks on the technology decisions they made before you joined, but I do think it’s useful to understand the history and factors that drove decision making. Most bad decisions today were great decisions within a context that no longer exists: write down what those contexts were for you and future hires to understand the evolution.
7.  **Document the existing technology strategy.** Surprisingly few companies have a [written technology](https://lethain.com/magnitudes-of-exploration/) [strategy](https://lethain.com/strategies-visions/), so start documenting what you learn about the implicit technology strategy into a document and share those notes back to the team – is this the technology strategy we want?

The biggest anti-pattern to avoid when it comes to technology is _having too many opinions_. It’s relatively low impact for early career folks to have too many opinions – folks take them with a grain of salt – but executives who express too many lightly-held opinions create thrash in the organizations they work with, and some executives from an engineering background haven’t trained themselves out of that habit.

Resources
---------

Some of the resources I’ve found helpful while putting together this post:

*   [How to build a startup engineering team](https://increment.com/teams/how-to-build-a-startup-engineering-team/) by Kevin Stewart
*   [VP Engineering Vs CTO](https://avc.com/2011/10/vp-engineering-vs-cto/) by Fred Wilson
*   Some great career ladders from [Rent the Runway](https://docs.google.com/spreadsheets/d/1k4sO6pyCl_YYnf0PAXSBcX776rNcTjSOqDxZ5SDty-4/edit#gid=0), [Glossier](https://ladder.glossier.io/), [Patreon](https://levels.patreon.com/), [Duo](https://duo.com/assets/pdf/engineering-career-ladder.pdf), [Meetup](https://github.com/meetup/engineering-roles/). There is also [progression.fyi](https://www.progression.fyi/) which links to many more
*   [Pay fair](https://increment.com/teams/pay-fair/) by Lara Hogan
*   [A guide to distributed teams](https://increment.com/teams/a-guide-to-distributed-teams/) by Juan Pablo Buriticá and Katie Womersley
*   [First Round Review’s engineering articles](https://firstround.com/review/engineering/) are uniformly high quality
*   [The Value of Networking for Technical Managers](https://www.volusion.com/blog/the-value-of-networking-for-technical-managers/) by Jason Vertrees (I wrote a similar piece on [Meeting People](https://lethain.com/meeting-people/))

* * *

I’ve previously written up my [recommended book](https://lethain.com/best-books/), but a more focused list for this topic:

*   [The Manager’s Path](https://www.amazon.com/Managers-Path-Leaders-Navigating-Growth/dp/1491973897/) by Camille Fournier
*   [The E-Myth Revisited](https://www.amazon.com/Myth-Revisited-Small-Businesses-About/dp/0887307280/) by Michael Gerber
*   [The First 90 Days](https://www.amazon.com/First-Days-Updated-Expanded-Strategies-ebook/dp/B00B6U63ZE/ref=sr_1_1?s=digital-text&ie=UTF8&qid=1532438854&sr=1-1&keywords=the+first+90+days) by Michael Watkins.
*   [Peopleware: Productive Projects and Teams](https://www.amazon.com/Peopleware-Productive-Projects-Teams-3rd/dp/0321934113/ref=sr_1_1?s=books&ie=UTF8&qid=1532354245&sr=1-1&keywords=peopleware) by DeMarco and Lister
*   [Slack: Getting Past Burnout, Busywork, and the Myth of Total Efficiency](https://www.amazon.com/Slack-Getting-Burnout-Busywork-Efficiency-ebook/dp/B004SOVC2Y/ref=sr_1_1?s=books&ie=UTF8&qid=1532354289&sr=1-1&keywords=slack+demarco) by Tom DeMarco
*   [Accelerate: Building and Scaling High Performing Technology Organizations](https://www.amazon.com/Accelerate-Software-Performing-Technology-Organizations-ebook/dp/B07B9F83WM/ref=sr_1_1?s=books&ie=UTF8&qid=1532354658&sr=1-1&keywords=accelerate+devops) by Forsgren, Humble and Kim.
*   [Escaping The Build Trap](https://lethain.com/notes-escaping-the-build-trap/) by Melissa Perri

* * *

Books that were recommended to me by others when I asked this question, that I haven’t personally read:

*   [A Seat at the Table: IT Leadership in the Age of Agility](https://www.amazon.com/dp/B075TD7JJ3/) by Mark Schwartz
*   [Simple Habits for Complex Times](https://www.amazon.com/Simple-Habits-Complex-Times-Practices-ebook/dp/B00T0392IY/ref=tmm_kin_swatch_0?_encoding=UTF8&qid=&sr=) by Jennifer Berger and Keith Johnston
*   [Unlocking Leadership Mindtraps](https://www.amazon.com/Unlocking-Leadership-Mindtraps-Thrive-Complexity-ebook-dp-B07NWWRXC5/dp/B07NWWRXC5/ref=mt_kindle?_encoding=UTF8&me=&qid=) by Jennifer Berger
*   [The Art of Business Value](https://www.amazon.com/dp/B07B425Y3K/ref=dp-kindle-redirect?_encoding=UTF8&btkr=1) by Mark Schwartz, Gene Kim
*   [Traction: Get a Grip on Your Business](https://www.amazon.com/Traction-Get-Grip-Your-Business-ebook/dp/B007QWLLV2/ref=tmm_kin_swatch_0?_encoding=UTF8&qid=1576866140&sr=1-3) by Gino Wickman
*   [Who: The A Method For Hiring](https://www.amazon.com/dp/B001EL6RWY/) by Geoff Smart and Randy Street (I’ll caveat from reading a summary that I find language like “A Players” and “B Players” to present a fixed mindset worldview, which I’m not fond of)

Ending thoughts
---------------

There’s the saying, “With great power comes great responsibility.” and a variation that I think on occasionally is, “With great privilege comes great accountability.” One of the changes when becoming an executive is that you’re no longer constrained by others holding you to their standards, but instead have to hold yourself to the standard. This is true in terms of [not making exceptions for yourself](https://lethain.com/work-policy-not-exceptions/), and also in terms of doing durably good work rather than creating the appearance of progress. You have to hold yourself accountable for your approach and your results.

With that in mind, go into your first ninety days with an open mind, a deep enthusiasm for learning, and start uncovering how you can best support the team and the company.

* * *

_Written with the help of the many folks who chimed into this [Twitter thread on resources for CTO/VPE roles](https://twitter.com/Lethain/status/1208076170853408768)_, _and reviewed by [Cos](https://twitter.com/getCos), [Garry](https://twitter.com/garryturk), [Jake](https://twitter.com/Jakelear), [Jason](https://twitter.com/Jasonvertrees), [Johnny](https://twitter.com/Recursivefunk), [Kevin](https://twitter.com/kstewart), [Michael](https://twitter.com/Michael_does), [Michael](https://twitter.com/mhamrah), and [Subbu](https://twitter.com/sallamar)._