
---
title: How to sell a B2B product by Calvin French-Owen
date: 2020-03-21
---
[calv.info](https://calv.info/how-to-sell-b2b)

How to sell a B2B product by Calvin French-Owen
===============================================

Calvin French-Owen

17-21 minutes

* * *

Lately, a bunch of early stage founders have emailed me to ask for go-to-market (GTM) advice. They have a real product, it solves a real problem for their handful of customers…

_…so why isn’t it selling?_

It’s a problem faced by nearly every single technical founder I talk to.

The unfortunate truth is that the first year of sales will tend to make or break the company. Sales provides the initial momentum for a startup to survive. It’s the difference between a Series A, and an [Our Incredible Journey](https://ourincrediblejourney.tumblr.com/) Medium post.

The trouble is… I’ve always felt at a bit of a loss when asked for sales advice.

I’ve worked way more on the product and tech side. Until recently, I’d never had a strong framework for understanding how sales actually works.

That changed after I recently attended a two-day sales training. Our CRO hired a consulting firm called [Force Management](https://www.forcemanagement.com/) (_I know, I know, it sounds like soul-crushing corporate training, but bear with me_) to teach all of our GTM teams about sales. I was fortunate enough to sit in.

Candidly, I was impressed by everything the instructors taught us. They were handing me the exact answers to the questions these founders were asking me for.

My goal in this post is to relay all of those learnings as concrete advice for early stage founders. By reading it, you should be armed with the framework you need to sell a SaaS product to other businesses (and have a view into the enterprise).

We’ve gotten Segment surprisingly far without having any of this sort of framework. But frankly, I wish we’d known this much earlier on. There’s no reason not to be great at this from day one.

Without further ado; here’s everything I’ve learned when it comes to selling a B2B product.

![](https://assets.contents.io/asset_YCoBsVolsigzIBTw.png)

Before getting into tactics, I want to spend a minute on the importance of sales, particularly in the enterprise.

I used to think that “products just sell themselves”. A great product naturally spreads like wildfire.

I’ve since realized that I was dead wrong. A great product may be the backbone of a company, but it’s not sufficient all on it’s own. Here’s why.

Think about all of the products you buy today… food, gas, coffee. For the most part, these products are _commodities._ The cost of the good is dictated by the raw ingredients and the competition; maybe there’s some markup for additional quality.

What that means is that you might encounter a $5 cup of coffee on your morning commute… but a $50 cup of coffee would be unheard of.

Now in the enterprise, these rules go out the window_._ For most SaaS business, the cost to deliver your product rounds down to zero\[1\]. The infrastructure used to run our Salesforce instance probably costs somewhere on the order of $100 per month. Yet, they charge us tens of thousands of dollars per month… how?

In enterprise software, customers pay for software based upon the **value that software provides, not the cost to deliver or build that software.**

The same product can be sold for $50, or $5,000,000–and the price comes down to **how much value the buyer is getting** from the software. As long as the buyer sees it as a positive return on investment, it’s worth it to pay the price.

A great salesperson steers the conversation towards that value. If you’re not anchoring against the value, you are literally leaving money on the table.

![](https://assets.contents.io/asset_8mefjmplpGdwXy3y.png)

The first lesson in sales is: don't think of it as sales.

_...wait...what?_

When I think of salespeople… I typically think of people _without_ my best interests at heart. They’re trying to close the deal. They don’t really know or care about the things I want.

Honestly, this rule applies to 99% of sales interactions I’ve had. Why shouldn’t this stereotype hold if I’m talking to someone trying to sell me something?

Don’t be that 99% of my inbox.

When selling, you have to approach the discussion from a totally different mindset all together. Great sales isn’t about closing the deal… it starts with understanding the customer.

Don’t think of yourself as constantly trying to pitch them on your company. Instead, think of yourself as a consultant or a trusted advisor. You’re trying to help the customer get the outcome they want _(and maybe that’s not buying your product!)_.

Getting there requires two key skills: 1) actually listening to the business needs and 2) demonstrating that you’re listening.

To do this, I’ve seen a few different tactics be effective

*   **Do your homework:** read 10-Qs, earnings reports, and press briefings. Try out the product or service, and see what customers are saying. Look at reviews on G2Crowd, Capiche, and Yelp. Have an idea of what matters to your customer before that first call. Companies (especially public ones) put out a ton of information on what they want to get done. It’s on you to find it.
    
*   **Ask “what does success look like?”:** anyone considering buying a tool will have an idea of what they want to get out of it. There’s likely some quarterly goal that they want to achieve, or some metric that they want to impact. It’s a great idea to understand what matters most to the customer.
    
*   **Tie back to a business objective:** at the end of the day, businesses traditionally value just a handful of things: increased revenue, decreased cost, reduced risk, or time to outcome. It’s worth understanding what the overall goals for the business are here, because your product should support one of those top-level goals. Saying “we have a better UI” doesn’t really paint the ROI story that you need.
    
*   **Write it all down:** there's one big commonality between our best salespeople: they are _constantly_ taking notes in a notebook. No one will object to you taking notes—and it’s a key way to remember exactly what the customer said. You’ll want to use these notes in follow-up emails and conversations. It’s really easy to do and makes you seem like a pro.
    

At the end of the day, selling well means that you are helping the customer actually understand their problem, and the path to evaluate a solution. Don’t sell, consult!

![](https://assets.contents.io/asset_XgUd444OzzC437ah.png)

Okay, once you’ve started learning about what the customer cares about, you need to get a first meeting with them.

I’ve historically seen two ways work well for this, inbound and outbound.

**Inbound**

Getting customers coming to you means that you first have to put something interesting out there.

When it comes to content, my #1 rule for this is **teach people something they didn’t already know**. It has to be non-obvious, and combine some set of deep research or your own data.

A good rule of thumb is asking yourself “do I wish I’d read this post six months ago?” It’s easy to become “too familiar” with content that’s actually really interesting.

Priceonomics has the definitive guide on building out interesting content, so if you want to go this route, check them out first: [The Content Marketing Handbook](https://priceonomics.com/the-content-marketing-handbook/).

I’ll warn you right now that inbound is going to be a slower path. But it’s far more likely to succeed for some audiences (developers in particular).

In [the early days](https://segment.com/blog/show-hn-to-series-d/), we grew Segment by:

*   Putting up blog posts — we shared posts about anything, [what we’d learned](https://segment.com/blog/how-to-make-async-requests-in-php/) and [what we’d built internally](https://segment.com/blog/ui-testing-with-nightmare/).
    
*   Open sourcing internal tools — we [open sourced](https://github.com/segmentio/nightmare) a [lot of](https://github.com/segmentio/metalsmith) useful [utilities](https://github.com/segmentio/evergreen). For each one, we’d design a logo and put up a landing page.
    
*   Launching Analytics Academy — we realized people were eager to learn more about analytics, [and the best way to highlight Segment was to teach them](https://segment.com/academy/).
    

**Outbound**

If you need a faster path to getting users (or your market requires it), you may have to rely more on cold emails.

As a rule, most people _hate_ getting outbound emails. I certainly do (_we actually used to have a rule at Segment that we didn’t send outbound emails ever)._

Most outbound emails I get aren’t personalized. Best case, they feel like spam, worst case, they are downright annoying. I archive almost all of them immediately.

I’ve since come around a bit, mainly because I’ve realized: _outbound emails don't have to suck_. A few ground rules:

*   **Do your homework:** sound familiar? Make sure that you’ve researched the person you’re emailing. Look at LinkedIn, Twitter, recent talks they’ve given. Make sure you can put yourself in their shoes.
    
*   **20 emails are better than 1,000:** I’ve found time-and-time-again that a handful of emails are better than a random blast. If you need early user feedback, focus on those few users you really want.
    
*   **Answer: Why me? Why now?** If I'm going to get an email from you, you have to make it clear that you’ve done your homework. You have to mention something that’s both relevant to me, and explain why you’re emailing now. _e.g. I saw your talk on handling outages, and it got me thinking about entrypoints. I realized that there should be some sort of tool to make those more apparent. We’ve built a first version that I’d love to show you. It incorporates your feedback of …_
    
*   **Get an intro** so many of the founders I talk to don’t lean on their investors hard enough for intros. Yet, it makes me 10x more likely to talk to a new startup if someone else recommends them.
    

![](https://assets.contents.io/asset_reCEfKTG2Kx2DoSk.png)

Now, let’s shift gears to what happens when you’re actually talking to the prospect.

The truth of the matter is that 99% of salespeople start with the same exact pitch. It’s pre-canned, it’s not personal, and it’s irrelevant. In other words… it’s far less likely to succeed. It’s the dead opposite of what we want to do.

Remember, let's start the conversation by thinking like a consultant, not a salesperson.

Before ever talking about “what it is you do”, you need to gain a clear understanding of the problems the **customer is trying to solve**. And you need to have a crystal clear idea of how they are doing things today.

That means asking open-ended, illustrative questions. As an example, I’ll share a few that are particularly relevant for Segment:

*   _What does success look like? What metrics are you looking to move?_
    
*   _Tell me how you answer a question about your funnel today?_
    
*   _Walk me through the process to gauge the success of a new product launch._
    

These questions help establish the “before” state; it’s the status quo.

The before state probably has some rough edges… maybe it’s expensive or a waste of time. Go deep here, you need to figure out where the real pain for the customer comes from. You need to be able to articulate that pain later, so ask questions even they may seem obvious.

Once you understand the status quo… start probing for what the business should look like in a year.

This is where the “after” state comes into the picture. If the customer could have their dreams fulfilled, what set of things would they want to exist?

Write down that “after” state diligently. Ideally the after state should have a set of “axioms that are now true” as well as concrete business outcomes and metrics.

![](https://assets.contents.io/asset_ScHpWmJUyHUxcD8U.png)

Once you and the customer both have a clear picture of the “before” and “after” states, then there’s the natural question of “how do we get there?”

That’s where we get into creating **required capabilities**. These are the list of requirements that need to be true to transform from the before state to the after state.

_What_ these are is not nearly as important as _how_ they are created.

Crafting the required capabilities should be a collaborative affair. If you’re just listing out a set of requirements, you’re doing it wrong. You’ve stopped listening.

Imagine it in the same way that you'd build your product. You don’t just go into a room solo and emerge with a list of strict requirements. You’ll whiteboard them, pressure-test them, and give and get feedback from the customer.

It’s your goal to understand what their list of requirements is. At the end of the day, you want to agree on that list.

To see what this looks like, suppose I’m trying to sell Datadog, an application monitoring tool.

My list of required capabilities might look a little like this:

*   Engineers must be able to create dashboards to monitor their own services
    
*   Engineers must be able to define alerts which trigger our existing pager system (Pagerduty)
    
*   We must be able to record our own custom application-level metrics
    
*   We require SSO support
    

Those capabilities are requirements that you and the customer have mutually agreed are important.

Remember, the customer is the expert on their organization, and the needs it has. You are likely the expert on how companies evaluate and buy your software. The two of you need to strategize together to figure out what makes sense.

![](https://assets.contents.io/asset_9GAeUsZYIKq9yVFM.png)

Finally, it’s the moment you’ve been waiting for! The time to tell the customer about all of the great things you’ve built.

The way _not_ to do this is to put together a laundry list of features you’ve built. Instead, you want to speak to the value you provide—and that means bucketing the problems your product solves into several different buckets.

There are only a handful of types of value propositions when it comes to selling to businesses:

*   increased speed
    
*   increased revenue
    
*   reduced cost
    
*   reduced risk
    

At all costs, you should be able to map some part of your product and its features to these four outcomes.

When I think about the products that we pay for an use at Segment, all of them fall into these different buckets:

*   AWS increases our speed of iteration
    
*   Salesforce increases our revenue
    
*   GSuite reduces the fully loaded cost of our email servers
    
*   Okta reduces the risk of stolen credentials
    

Of course, some of these value propositions are correlated (speed often decreases cost), but the key is that they can contribute meaningfully to the business model in some manner.

![](https://assets.contents.io/asset_lPEGS8u2ehN7YWzW.png)

Finally, there’s The Mantra.

If you remember nothing else from this post, just memorize this section. The mantra is a handy tool to use to wrap up conversations, re-affirm what you just talked about, and discuss next steps.

The mantra should be “happy birthday” level memorized, so that you can repeat it by heart at a moment’s notice.

*   If I understand correctly, you want to achieve the following business outcomes…
    
*   Which require the capabilities to...
    
*   And impact the following metrics...
    
*   Here’s how we do it…
    
*   And here’s how we outpace our competition…
    
*   For more info, you can check out the following proof points…
    

These six lines help establish mutual understanding. It shows that you’ve actually listened to the customer and their problems, and affirms that your product can really help them.

![](https://assets.contents.io/asset_lulekJwE5Jex5k1p.png)

And we’re done! Well, _almost_.

No deal is fully complete without a set of qualification criteria.

Plain and simple, a sales criteria ensures that you’ve covered your bases. It’s a rigorous tool you can use to evaluate a deal and see where you might have blind spots.

Think of criteria in the same way that you’d think of the checklists that NASA or SpaceX use to launch rockets. If any field isn’t checked… you might be missing a key piece of the picture.

The one we use is called MEDDPICC. I’ve detailed it out below.

**Metrics**

Metrics are one of the most important parts of a sale. You and the buyer should both be agreed on which metrics the product will impact for the business. That’s how you’ll measure the success of the product (and ideally the ROI).

**Economic Buyer**

The economic buyer is the person who actually buys a product—the catch is that they may or may not be the person who wants to use it. In every case, you want to know who can actually make the purchasing decisions.

**Decision Criteria**

The decision criteria are more or less the “required capabilities” I talked about earlier on in the post. They determine how the customer will evaluate the product you’re selling.

In every case, a viable option for the customer will be to “do nothing”. They don’t have to buy your product, or any product for that matter.

The best way to prove the case for your product is with your metrics. Doing nothing should be a very expensive option.

**Decision Process**

Of course, the criteria are only a piece of the puzzle. The decision process governs how customers will actually purchase a piece of software.

This can vary wildly from company to company—so you should take care to understand how your customer actually buys software. Ask questions to confirm:

*   Who you should talk to in order to make a decision?
    
*   What does the timeline look like to get there?
    

**Paper Process**

The paper process refers to all the forms, paperwork, and contracts as part of a deal. While it sounds similar to the decision process, DO NOT IGNORE THIS STEP. We’ve had a number of deals fail to close over the years just because we weren’t sure of how the paperwork actually got signed.

**Identified Pain**

Though it’s far down in the acronym, the identified pain is really where you should start with the customer. You should be able to answer in great detail why their life is currently painful, and how they handle the problems you solve today.

**Champion**

Your champion is the customer who is rooting for you and wants to buy your product. In the early stage, it’s likely that your champion and buyer are the same person. But as you grow and expand your deal size, the champion might need to get permission to buy your product.

When this happens, you need to be giving your champion firepower! Help them provide collateral to the rest of their organization to teach them why your product is the best!

**Competition**

Lastly, you should know your competition. It could be internal efforts, it could be competitors, or it could be doing nothing at all. In each case, make sure you know why the customer is considering your competition.

At the end of the day, selling well means **understanding your customer.**

In some sense, sales is just another form of finding product market fit. Instead of helping a customer use a product, you are helping them make a decision.

If you take nothing else from this post, remember that your customer is the expert on the problems they want solved. They have an extremely detailed view of what matters to their business, likely crafted over years. It’s your job to help them understand how those problems map to the different products on the market, and how to make the decision to buy your product in particular.

As a final parting thought, I think most of the lessons here aren’t just applicable for startup founders. They’re useful for anyone who wants to sell an idea within their company, or establish a culture of elite sales from day one.

Seek to understand first. The rest will tend to follow.

* * *

\[1\]: A notable exception to this are Infrastructure-as-a-Service (IAAS) providers.

_Thanks to Ivan Lee, Peter Reinhardt, and Kevin Niparko for providing feedback._

Loading…