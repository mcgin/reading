---
title: The Content Marketing Handbook
date: 2015-12-09
---
[priceonomics.com](https://priceonomics.com/the-content-marketing-handbook/)

The Content Marketing Handbook
==============================

162-206 minutes

* * *

_![](https://pix-media.priceonomics-media.com/blog/1085/13.jpg)_

_\*\*\* Preface \*\*\*_

When we finished writing a 30,000+ word book about content marketing, we did not plan to publish it as one, unreasonably long blog post. And yet, here we are.

Then again, when we started writing a company blog in December of 2011, we never expected to hire 5 full-time writers, publish several books, or turn into a content company that makes money by selling products. 

This is a long blog post. It contains everything we know about how to make content and get it to spread on the Internet. It’s the story of Priceonomics, and how we built a business turning data into content. It started out as a brief handbook we gave to new Priceonomics writers; now we’ve expanded it.

If you’d prefer a summary of the Handbook, we made a 6-page version [**that you can find here**.](http://eepurl.com/bVruOT) We’ll also publish a PDF and ePub version of the full Handbook in the next couple of days. If you’re interested, [leave your email here](http://eepurl.com/bVruOT).

Consider this Handbook a companion to our release of [Content Tracker](https://priceonomics.com/content-tracker-by-priceonomics/) last week. Content Tracker is our software program for measuring how well your content marketing is performing. Does it get media mentions, traffic, social shares, and customer leads?

But how do you make content that performs, and how do you distribute it? The Content Marketing Handbook (this blog post) is our attempt to share the “secret formula” of the Priceonomics Blog so that when you write something, customers, journalists, and readers take notice. Is there a formula to the Priceonomics Blog that other companies can copy? Sort of. Here it is: 

Write about information. Make it good. Have a plan for how it will spread.

\*\*\*

This blog post was supposed to be our next book, hence the length. 

But before we published it, we paused to consider whether selling a $15 book was the best way to productize this information. Our first two books, _[Everything is Bullshit](http://www.amazon.com/dp/B00L9G96NG/?tag=priceonomic0c-20)_ and [_Hipster Business Models_](http://www.amazon.com/dp/B00QXXFVWA/?tag=priceonomic0c-20), sold pretty well, but the money generated has been modest compared to our revenue from selling things to businesses. Plus we thought it would be nice to avoid all the unpleasant work involved in turning a draft of a book into an actual book!

Instead, we ran an experiment. 

We decided to put together a presentation at our office where we would talk about the “Priceonomics way” of doing content marketing. We had no idea if anyone wanted to hear us talk for an hour about how to turn company data into content and make it spread. But we posted the event details of this talk on a few startup forums and email lists with the hopes of attracting an audience. 

Our not-so-secret plan for this presentation, however, was to present an “offer” to help the companies that attended the talk implement the strategies of our book. Could that “offer” (whatever it was) generate more revenue than merely selling a book? Our ideas about what exactly to offer were vague, but it would involve helping companies create better content marketing through our software or a service that helped companies turn their data into content.

To our surprise, 200 people, mostly startup founders and marketing executives of fairly sizable companies, signed up to attend our talk.

The Priceonomics office isn’t that big, so we couldn’t possibly accommodate 200 people. This size constraint, however, held an advantage: We had to run 10 small presentations instead of one big one, which meant we could test 10 slightly different “offers”.

And so we gave ten presentations that were identical until the very end. On the last slide of the presentation, we presented different “offers” for how we could help companies make better content. 

\*\*\*

We learned a lot from presenting the knowledge from this Handbook to other companies. 

First, it was clear that our approach was reproducible by other companies, even without our help. Every company should write about its data! Within a few weeks of the first presentation, we received emails from attendees who said that the articles they wrote about their company’s data got picked up by the media, rose to the top of Reddit, and outperformed anything they’d ever done by 10 to 100x.

We also learned a lot by presenting 10 different “offers” of how Priceonomics could help companies make better content.

Right off the bat, we knew people wanted to use our [Content Tracker software](https://priceonomics.com/content-tracker-by-priceonomics/). People loved it! While Tracker is complex behind the scenes, its frontend is a simple dashboard that tracks the performance of all your content marketing. If you make content all day, having Tracker is really, truly helpful, especially if you integrate it with Slack. 

From talking to companies, however, we discovered a bigger problem that software alone could not solve. Many companies had a huge, “hair-on-fire” problem with content: They spent a ton of money and time on their blogs, and nothing ever happened! No traffic, no media mentions, no customer leads, nothing. They just kept checking the “we should do a blog post” box, but there was no return on that invested time. For many companies, Tracker could only show them what they already knew: their blog was a ghost town.

So, we next tested something different. We offered to _help_ companies get better at making content. We would help them come up with ideas for how to turn their data into great stories and edit their pieces so they’d be really good. We charged $2,000 per month for this service, which was like hiring Priceonomics as the editor of your company blog. 

A lot of people signed up for this service! 

Companies paid us $2,000 per month, and we helped them develop ideas based on their data, edit their articles, and do their best work. And they could see the results in Content Tracker, so they knew it was working! Right away we made way more money than we would have from book sales.

But there was a problem. 

Actually, for half the customers, there was no problem. These companies loved the service and published some amazing work with a little bit of guidance and editorial assistance. It worked great. After years of having a blog that no one visited, they started publishing information that journalists covered and people shared. 

But the other half of the companies never published anything! We’d meet, brainstorm some ideas about interesting nuggets in their data, and they’d go off to work on a draft and then disappear! The companies always got too busy to finish the blog posts or run the analysis. They kept paying us $2,000 a month for the service, but they weren’t fully using it. That didn’t feel very sustainable, even if the margins were enviable.

And then, towards the end of this process, we hit on it: The optimal way to take the knowledge and software we’ve developed at Priceonomics. A better way to productize the knowledge from this Handbook.

We’d make content for companies based on their data and then just charge them _based on the performance_. 

Companies wanted performance (articles that generated customers, PR, and traffic), so that’s what we’d sell! We knew how to look at their data, turn it into content, and make it shareable. As long as companies used Content Tracker, we could charge them based on performance.

So we made this final offer, which we wanted to be a no-brainer for companies to accept. 

We offered to help companies turn their data into awesome content marketing. We’d put an insane amount of effort into it, and we’d do it at cost. But, if the content got 5,000 views or 5 press mentions, they’d pay an additional success fee. And, conveniently, we could measure the performance in [Tracker](https://priceonomics.com/content-tracker-by-priceonomics/). 

When you consider the time and effort involved in creating content, we knew many of the companies we talked to were already spending way more than thousands dollars per month on their content marketing. But they weren’t getting results. Paying us to make content was a low-risk proposition with completely aligned incentives.

Companies started buying content from us. Some tried it once and then asked if they could sign year-long deals. One company bought one piece of content from us. The next month they bought 4. The next month they bought 10. This was nuts! It worked because we were selling _performance_, not content. Companies don’t need content; they need _things to happen_ when they publish content. Good content is just the catalyst for performance.

This is why we are announcing our new content marketing agency, the Priceonomics Data Studio. It’s sort of half analysis firm, half content firm, just like Priceonomics. It’s a content marketing agency that is only available to users of our [Content Tracking software](https://priceonomics.com/content-tracker-by-priceonomics/). (Which is free , and you should use it.) 

We’ll take your company’s data, analyze it, and turn it into great content marketing that you publish on your site. And we charge mostly based on performance. So far, we hit the success metrics around 75% of the time.

If you want to learn more about our data-driven content marketing agency, just [**send us a message**](https://priceonomics.com/contact/), and we’ll set up a time to talk about your data. It’ll be fun. It works.

\*\*\*

Everything in this book works. You can put it to work, you can hire us to put it to work, and you can use our software for to measure if it’s working. 

Throughout this book, we urge you to think of content marketing like a campaign that must be _waged_ with tremendous effort. Nothing good ever happens by accident. You need to start with great ideas, you need to have a plan for how to distribute that content, and then you need to execute that plan. Your content will not be picked up by the media or go viral by accident. You have to engineer that sort of outcome.

The book is about three things. How to make content. How to spread content. And how to pull this off if you don’t have tons of resources. It’s based entirely on our experience at Priceonomics.

The book is split into eight chapters:

**Chapter 1: Getting Started**

The unlikely origin story of the Priceonomics Blog.

**Chapter II: Information Marketing**

The data and information that is a byproduct of your business is valuable.

**Chapter III: The World Is Flat (The Level Playing Field)**

Our answer to Peter Thiel’s question, “What do you believe that no one else believes?” We believe that if you make a good piece of content, it increasingly doesn’t matter if you publish it on _The New York Times_ website or your company’s blog.

More specifically, this chapter describes the mechanisms by which content spreads.

**Chapter IV: Social Networks**

Why do people share? Mostly to confirm their existing biases. (But you already knew that.)

**Chapter V: The Writer’s Playbook**

A primer on how to write for the Internet. 

**Chapter VI: Writing Hacks**

One weird trick to get really good at producing content marketing: think of it like a campaign, and don’t let yourself fail.

**Chapter VII: Hiring**

How we hire writers at Priceonomics, and our approach to identifying and training talent.

**Chapter VIII: Why Content Matters**

Should content marketers feel icky?  Content marketing is essentially content that is monetized by selling products. Traditional media is content that is monetized by selling user attention and personal data. To us, it’s an open question which is actually ickier.

We also conclude by summarizing the economic case and the opportunity for companies to make better content. Content that is of high quality, that is less absurdly self-promotional, and that actually performs. 

\*\*\*

So without further adieu, we present the rest of “The Content Marketing Handbook.” Or, “How to Write about Information and Make it Spread.” Or, An “Elaborate Piece of Content Marketing to Sell Content Marketing Software and Services.”

Again, if the 30,000-word version is too long, here is a [**6-page summary instead**](http://eepurl.com/bVruOT).

And now, let’s start at the beginning.

**Chapter I: Getting Started**

The Priceonomics Blog almost didn’t happen. Today, we get tens of millions of free visitors to our site each year who buy books, data, and highly-valued business services from us -- but we almost screwed things up from the the get-go.

In late 2011, Priceonomics (our company) started off as an online price guide for used items. We built web crawlers that checked how much people were selling their old iPhones, bikes, and cars for, analyzed the price data for these items, and then posted it on our website. We were like Kelly Blue Book for everything.

So, we built and launched a website -- and guess what happened? Nothing. This wasn’t our first internet startup, so we expected that. The internet is a vast place: people won’t notice a new website or app unless you figure out a clever way for people to find it.

Our goal was for people to discover the Priceonomics price guides through Google. People look up prices and conduct product research all the time on Google, so we needed our results to show up there. If you wanted to find out how much a used iPhone was worth, it was imperative that Priceonomics appeared in the search results.

To rank well in Google Search results, a website needs two things: content and links to it from other websites. Our content strategy was less than ideal, but that was the nature of our product. Our algorithms had created thousands of web pages for every product we’d priced. They were not the rich, handmade, content that Google loves; they were thin pages with the name of the product, its price, and links to a few places where you could buy the product. 

In the short term, our website had so many of these pages that we couldn’t really improve the quality of them (outside of improving the algorithms that generated the pages).

What we could control, however, were inbound links. In order to rank highly in Google’s algorithm, not only do you need good content, but you need other people to link to it. That’s a signal to Google’s algorithm that your content is authoritative and should show up in Google search results. If _The_ _New York Times_ links to your page about iPhone prices, that’s a sign that your content is probably reputable. 

So we decided to start a blog and write about our pricing data. Around this time, in 2011, the dating site OKCupid had written some very popular articles about dating based on the company’s data, and we figured we’d try the same thing with our used pricing data. It was an experiment to see if we could get links and improve our ranking in Google search results.

As we planned our blog, we began a twelve week “accelerator” program in the startup incubator Y Combinator. Because we were a part of this program, TechCrunch, the influential technology website, agreed to write about our launch.

TechCrunch staff told us the article would go live December 20. The article would be helpful, because a link to our site on TechCrunch would, in Google’s eyes, improve our credibility. We also decided that after the TechCrunch article went live, we’d write an article using our data, submit it to Hacker News, a popular social news site in the tech community, and hopefully reach more potential users. (And get more links from news sites that would discover Priceonomics.)

For some time, we contemplated what we could write about. Christmas was coming up and the Internet was buzzing with articles about gift ideas. Since Priceonomics was a pricing site for used products, we decided to write about used things you could buy as Christmas presents. Since we planned to submit the article to Hacker News (a website geared toward programmers and run by our investors at Y Combinator) we wrote about presents that “hackers” would like.

Our blog post -- “Buy Christmas Gifts for Hackers on Craigslist” -- was, of course, terrible. Who wants to buy used products for Christmas gifts? What does it even mean that a product is for a “hacker”? Did we think that _New York Times_ writers would see this and think, “We must write about this intriguing website that will tangentially help people buy used things for computer hackers”?

Here is the introduction of the blog post we wrote:

> You might not want to buy a Christmas present for your girlfriend’s mom on Craigslist. Your husband or aunt might scoff at a used trinket under their tree. For reasons best expounded by others, it’s against social norms to buy used goods for people as presents in the United States.  
> 
> But you know what, there are awesome things on Craigslist and they’re cheaper than buying them new. If Etsy has championed the “buy handmade” movement, at Priceonomics, we champion the “buy used” movement. 
> 
> You can get 200% more bang for your buck if you buy used. A $100 budget can get you $300 of stuff if you let someone else eat depreciation and pain of selling their old stuff in illiquid markets. And for her trouble, the seller gets cold hard cash for things they no longer need. It’s a thing of beauty. It’s even good for the environment!
> 
> Skeptical? Let’s walk you through items hacker friends would love to have, but you can’t afford to buy them new. Unlike your girlfriend’s mom, they might appreciate it if you hacked your Christmas budget and actually got them decent presents for once! 
> 
> Our (somewhat biased) advice: Find these items on Craigslist. Look them up on Priceonomics to make sure they’re a good deal. Buy them. Make your friend happy. Rinse, repeat.
> 
> \[The rest of the post was a list of products you could buy, along with a comparison of their new versus used prices.\]

The biggest problem with this post was that we _play-acted_ at being marketers. We were doing what we thought marketers did. Every company has a blog, and they all write things like this, so we figured that was what we were supposed to do. At the time, we weren’t experienced writers or content marketers, so we thought the point of a blog was to talk about your products and sell them. That’s what marketing is: salesy and boring.

Here’s the good news, though: we never published that moronic blog post. December 20 came and went, and TechCrunch didn’t publish the article announcing our launch. Our blog post about Christmas presents ran the risk of being irrelevant.

Finally, on the evening of December 22, 2011, the TechCrunch article about Priceonomics went live. The next day, we were set to post our “insightful” article about the price of used Christmas gifts, but we faced some nagging issues. First, it was December 23rd -- two days before Christmas and pretty late in the game to buy gifts on Craigslist. More importantly, we didn’t yet have enough data to calculate the prices of several products we were recommending.

So, we scrapped the blog post and started fresh at the last minute. What could we write about instead? 

Earlier in the month, we had run an interesting experiment. We employed our pricing data to find good deals on used Aeron office chairs on Craigslist, and then we resold them to other startups at a substantial mark-up. The process of finding these chairs, lugging them around San Francisco, and selling them to our friends for a profit was pretty entertaining, in a Tom Sawyer sort of way.

We wrote a blog post about this experience and called it “Adventures in Aeron Chair Arbitrage.” Here’s a condensed version of it:

> _"Buy low, sell high. Fear? That’s the other guy’s problem."_
> 
>  _- Louis Winthorpe III, Trading Places (1983)_
> 
> Here’s a story about the time we made $300 in profit buying used Aeron Chairs on Craigslist and reselling them to our Y Combinator classmates at marked up prices. Basically, we were experimenting if we could make a business out of arbitraging used goods. In short, we learned that lugging furniture around the Bay Area didn’t seem to be an especially scalable business model.
> 
> For a little back story, Priceonomics is building the price guide for everything. Just like Kelly Blue Book creates price reports for cars, we do the same for bicycles, computers, phones, tablets, cars, and much more.
> 
> Our first experiment we dubbed “Craigslist Arbitrage.” To do this, we would identify products with steady demand but high price variance. We could then buy items that were posted at bargain prices and later sell them at the market price. Also, much of our team takes particular delight in Craigslist scavenging. 
> 
> Perhaps because we were sitting on extremely uncomfortable chairs at the time, we decided to focus on Herman Miller Aeron Chairs. There’s a fairly steady demand for these chairs in the Bay Area, particularly among web startups. Whether Aeron chairs are good chairs or are worth the money is perhaps best discussed by others.  
> 
> We fired off an email to other startups we knew offering to sell them Aeron chairs for $450, knowing that we could buy them on Craigslist for $350 and pocket the $100 difference.
> 
> \[The rest of the post details our trials and tribulations -- bounced checks, lugging chairs around the city, and generally having a tough time.\]

_\*\*\*_

We hit “publish” on December 27th and submitted this post to Hacker News. Slowly, people upvoted the article until it was the most upvoted link on the site. For Priceonomics, it was a huge hit. It seemed unbelievable to us that something we could write would end up at the top of one of our favorite websites! We were complete amateurs at writing.

![](https://pix-media.priceonomics-media.com/blog/1085/HNRevised.png)

Our post was on the front page of Hacker News for almost 24 hours and reached a huge swath of the technology industry. _The Wall Street Journal_’s technology blog wrote about our story, many industry leaders shared it on Twitter, and almost instantly our search engine results improved.

While getting covered by TechCrunch sent 6,000 visitors to our site, we learned that a popular blog post on our website could send 20,000 visitors in a single day. It was better to write popular things on our website and attract our own crowd than to try to cajole journalists into writing about us. When we published interesting information on Priceonomics, the journalists came to us!

Sharing information on the Priceonomics Blog struck a nerve. Following the success of our first post, we focused on writing interesting blog posts that shared new information and attracted huge audiences. Though our company has since shifted gears, one thing has remained consistent: the Priceonomics Blog has devoted readers despite its corporate origins. Since we started it fours years ago, we’ve reached over 30 million people -- all for free. And the Priceonomics Blog just keeps getting bigger.

What if we had stuck with our original idea of the “Used Christmas Gifts for Hackers” blog post? Would the Priceonomics Blog have succeeded? Probably not.

The “used Christmas gifts” post was just a bad idea. Since we’ve been blogging for four years, and have developed an intuition for what hits, we can say this with near certainty. If you’re going to spend time and effort blogging, there is no point in writing things that aren’t good. Instead, spend a little more time and write something great. Just because almost every company has a boring corporate blog that no one reads doesn’t mean you need to emulate them. Trust us: they should not be your role models.

But there is another reason why the Priceonomics Blog would never have taken off if we published the crappy Christmas gifts blog post: spending time and effort on work that fails is discouraging. If you write a blog post and nothing happens, you begin to expect that future writing will fail as well. If our first blog post had failed, it’s unlikely we would have put as much effort into the second one. We would have written off blogging as a waste of time, as it is for 99.999% of corporate blogs ever started.

In organizational theory, there is something called the [Effort-Performance-Outcome](http://en.wikipedia.org/wiki/Expectancy_theory) theory: you will only put forth the effort necessary to succeed if you reasonably expect that effort will pay off. This explains why workers slack off once they believe that their output won’t be used for anything important. It also helps explain lots of social phenomena, like why the cycle of poverty is so difficult to escape: if you grow up somewhere where there are no examples  of hard work leading to success, there is no case to be made for hard work.

There is nothing less motivating than writing blog posts on the Internet for most corporate blogs, because no one ever reads those blog posts. We imagine that the people who work at those blogs start off earnest and chipper. Then they work really hard on posts like “10 Reasons You Should Give Enterprise Software Licenses as Christmas Presents,” only to hear crickets when they hit the publish button. Write multiple posts like this -- posts that don’t influence your company’s sales, exposure, or leads -- and you’ll quickly lose motivation.

The default state of the internet is that **no one cares**. So many articles, videos, and blog posts compete for people’s attention that average work often goes unrecognized. It’s pointless to publish anything that isn’t fantastic, because it will certainly be ignored.

We got lucky on two dimensions -- and without this luck, it’s unlikely that the Priceonomics Blog would have thrived. First, despite not having any real writing experience, we happened to write something _good_ (rather than something boring, like we’d planned). Second, things _happened_ when we wrote the blog post: we got lots of traffic, coverage, and customer interest. We were motivated to continue writing largely because we saw the benefits of doing so.

This book is about these two things: making good content, and creating a process that makes sure good things happen to your company when you publish it. It’s pretty simple, but that doesn’t mean it’s easy.

**Making Good Content**

There are many different ways to define “good content”. _The New Yorker_ writes great articles; _NPR_ makes wonderful podcasts; _Buzzfeed_ makes highly shareable lists; _The Onion_ writes delightful satire. But these aren’t the role models you should emulate if you’re trying to get customers for your business. 

Most companies try to copy other companies’ styles and formats. Most commonly, imitators try to be funny, because “funny” is one of the currencies of the web: people share things that are funny on social networks. But funny doesn’t get you attention from _The Wall Street Journal_, doesn’t inspire confidence in front of potential customers, and doesn’t highlight your company's expertise. Funny is what you get when you hire an ad agency full of creative 25-year olds -- people who don’t fully understand your business -- to manage your company’s content.

![](https://pix-media.priceonomics-media.com/blog/1085/Screenshot2015-12-0810.35.31.png)

Instead, your focus should be on making interesting things that _also_ have some benefit for your company. This approach narrows your focus, and that’s helpful for coming up with topics to write about. But beyond that, we know exactly what you, as a business, should write about.

You should write about information.

After spending years producing hundreds of reports and reaching tens of millions of people, you can trust us on this point: authentic information that your company has access to is the currency of truly valuable content marketing.

Sure, there may be more “shareable” content out there -- cute animal pictures, snarky commentary, celebrity gossip, and the like -- but that stuff won’t get you customers.

Information, on the other hand, will. It can be data that your company produces, insights you have because of your industry experience, or stories about the people you have access to. 

Data has always been at the core of Priceonomics, so it comes naturally to us to write about it and use it to promote our products. At Priceonomics, we’ve published data reports on topics like iPhone vs. Android pricing, Airbnb vs. hotel fees, and the San Francisco real estate market. 

But you can only write about your own data for so long before people (including you) get tired of it. To vary our content, we started diligently researching economic questions like “What happens to stolen bikes?” and “Are speed limits are too low?” We started digging into the stories of entrepreneurs, researchers, and everyday people. And we published what we found on our blog.

We built a framework for how companies can write about information: write about data, industries, or people. Make sure this is original information your company has authentic access to, and that the information is novel and interesting. **Your company is an expert on its own information -- that’s what you should write about**.

You might not want to copy everything we do. Frankly, we spend way more time on our blog than you should. We’re outliers. We’re obsessed with writing good things. Do we write good things so that we can sell more products, or do we sell products so we can afford to write good things? We’re probably closer to the latter camp. You should be closer to the former camp.

But if you spend just a fraction of the amount of time that we do on writing, you’ll produce some really great content. If you do it right, it will get you press mentions and customer leads in perpetuity. It will establish your company as a leader in certain areas. And it may even get you invited to speak at conferences in your field. 

But there is no point whatsoever to churning out average material. You _have_ to make _good_ things. This book will show you how to do that.

**A Process**

Many marketing books assume that you have a large, established marketing department and lots of money. This is not one of those books.

We find it frustrating when someone gives us blanket advice without properly explaining how to do it. This book is about the tangible ways you can build a process to make great content. How do you hire someone to do this job? How often should you publish? How much time should you spend making content? How should you come up with ideas? What format should you publish it in? How do you contact journalists you’d like to write about you?

Everything we know, we put in this book. 

**What Happened at Priceonomics**

We started the Priceonomics Blog so we could get links and improve the ranking of our Price Guides in Google search results. To some degree, we succeeded at this. Lots of people came to our site through Google. 

But this was not as helpful as we’d hoped. Our visitors were transient people who looked up a price and left. Later on, they barely remembered which website they had found the prices on. These casual visitors never translated to dedicated users of Priceonomics. 

In addition, whenever Google changed its algorithm for how it ranks search results (as it frequently does), it wreaked havoc on our user growth. Some days we’d come into the office and find that traffic was booming. Other days we’d come in and find that Google had decided to decimate our traffic. Things weren’t looking good for Priceonomics. Our original idea was falling apart. 

But our blog was our bright spot. 

Despite its status as the “marketing blog” of a corporation, it started attracting regular readers. We hit over a million visitors a month -- then two million! Our friends at other companies started asking us if we’d be willing to sell them data we were publishing on the site. At first we demurred, since we were not a data sales company. But as it became apparent that they’d be willing to pay what _we thought_ was an absurd amount of money, we agreed. Later companies showed interest in using our software and expertise so they could make their own data-driven content, and we turned that into revenue as well.

Suddenly we had a business model in place: we could write to our hearts’ content on the Priceonomics Blog, and occasionally use it to sell very valuable services to businesses. Shortly thereafter, we shuttered the consumer price guide that we had worked so hard to promote.

Instead of making content with the aim of getting links for Google searches, we decided to make great content simply because we loved to make great content. Sometimes, this content promotes our own products, but even this content is great because it uses all the interesting data our business generates. It’s authentic information that Priceonomics has access to and other people find valuable.

Our blog saved our company. It made us distinctive and let us stand out in a sea of other companies. We love our blog, we love writing about data, and we love telling stories. Our mission for the Priceonomics Blog is to bring new information into the world. This is a responsibility that we take very seriously, and we want others to copy what we did: create great content that shares information while generating sales.

Here’s how you do it.

[![](https://pix-media.priceonomics-media.com/blog/1085/Screenshot2015-12-0821.06.59.png)](http://eepurl.com/bVruOT)

**Chapter II: Information Marketing**

Have you ever tried to get a journalist to write about your company? It’s extremely difficult. You track down someone who writes about your industry, acquire her email address, carefully craft your pitch, and hit send -- and then nothing happens. 

Put yourself in the journalist’s shoes: every day, she receives hundreds of emails from people pitching story ideas. Each and every one of these emails is a request for her to do five to ten hours of work to research your company (or whatever it is you’re pitching), and then write and edit something accurate, interesting, and cohesive. Imagine if hundreds of people stopped you on the street every day, asking you to spend this much time and energy to help them out. You’d go crazy.

Getting someone to write about you is basically a sales process. You’re trying to convince her to part with her time, in exchange for something you have to offer. You better have a good reason for it to be in her interest to write about you, because you’re essentially cold-calling her like a telemarketer.

Sales is tough for a telemarketer. Blindly pitching journalists is equally challenging. Instead, you should get the journalist to _want to write about you_ by helping her do her job.

Like many great (and not-so-great) analyses, the core theory of this book centers around a two-by-two matrix. And like always, it’s good to be in the box that’s up and to the right.

![](https://pix-media.priceonomics-media.com/blog/1085/Screenshot2015-12-0811.36.30.png)

Traditional PR (public relations) is the telemarketing-like strategy. It’s a negotiation between you and a reporter. You are asking the reporter to help you out by writing about you, but what are you offering her in return other than a lot of work?

Journalists are most likely to write about you if it doesn’t create enormous amounts of work for them, and if the story is likely to get a lot of attention. Say you are a congressman and you accidentally share an indecent photo of yourself on Twitter. Everyone will write about you, because it takes very little effort to write the article, and it will assuredly be successful. (This particular scenario is what most people call a PR crisis.)

Then there are articles that require a lot of work for a journalist, but come with a high payoff for them. If you have a tip that could lead to a Pulitzer Prize, a journalist will invest months or even years on that story. But their bar is pretty high. Unless you’ve stolen secret documents from the NSA or have evidence of malfeasance by a major corporation, you probably don’t have the kind of information that will inspire a major investigative report. Even if you did, this scenario likely wouldn’t be an effective marketing channel for you.

Lastly, we come to what this book is about: marketing by sharing information. This involves taking information that your company has access to -- or can create -- that is _genuinely newsworthy_, and publishing it. The goal is to create something that helps other people (journalists) achieve their goals, while making your company look good.

This means providing data that journalists can use to easily write articles that are interesting and newsworthy. Take a scan through _The Wall Street Journal_: every article about an economic trend cites data published by a company or third-party organization. When you provide information like that, you are doing valuable work for other people. Journalists will start coming to you as a source. You are bringing something to the table instead of asking someone else to do work for you.

**Writing About Data in the Modern World**

Any discussion of content marketing using information needs to start with the OkCupid blog. OkCupid is a dating website that launched in 2001. At the time, most dating websites required a very expensive monthly subscription; OkCupid, by contrast, had a free version that many customers found useful.

Despite its cost advantage, OkCupid struggled to attract a critical mass of eligible hotties (users). They tried a series of marketing stunts -- a website that set people up on a “crazy blind date,” and a quiz site called “Hello Quizzy” -- until they stumbled on a more prosaic, yet effective form of marketing: writing about their data.

While OkCupid wasn’t huge compared to Match.com or eHarmony, it did have millions of datapoints about the most appealing topic in the world: sex. It had information about how attractive its users found each other and how often they messaged each other. OKCupid also knew variables like their age, ethnicity, and sexual orientation.

The average marketer might look at this data and say, “So what? We have data about people’s ages, big whoop.” But the OkCupid team used this data to tell stories that everyone wanted to talk about.

In one post, OkCupid used its data to definitively prove that 20-something-year-old heterosexual men wanted to date 20-something year-old women -- and that 50 year-old heterosexual men _also_ wanted to date 20-year-old women. No matter how old men get, OkCupid contended, they still prefer women in their twenties. Women, on the other hand, tend to seek out similarly-aged partners as they got older.

OkCupid’s bloggers didn’t stop there. They had data about how likely people of different races were to send and receive messages asking for dates. They found that, of all their heterosexual female users, African-Americans sent the most messages, but received the fewest. They wrote about what features in a message -- length, language, subject matter -- maximize the likelihood of someone responding. These posts received millions of views and tens of thousands of shares on social networks.

The OkCupid blog was a masterpiece of data and storying telling; it also attracted a lot of press attention. By the end of their run, our sources at the company told us that OkCupid’s bloggers had a list of 300 members of the press, each of whom requested to be notified whenever they published a blog post. Instead of cold emailing journalists and begging for attention, OkCupid found a way to make the journalists come to the them -- and it was all because they shared information. 

Writing about data isn’t easy. The OkCupid team had two people working on its blog full time, which is a huge investment for a startup company. Most people who look at data will find it boring or impenetrable. As we’ll talk about later in the book, it takes a lot of time to pull a story out of a data set and make sure it’s right. 

But there are interesting stories buried in company’s data, you just have to unearth them. As a mental exercise at Priceonomics, we often ask ourselves, “Can we make any company’s data interesting?” To answer this question, let’s imagine that we work at Dunder Mifflin, the notoriously boring, fictitious paper company featured in the television comedy, The Office. If you wrote for Dunder Mifflin’s company blog, what data would you write about? 

Here are a few ideas we quickly brainstormed:

*   The Death of Fax Machines: Our Annual Sales of Fax Paper From 1980 to Present
*   How Much Paper Do Accountants Use During Tax Season?
*   Who Uses More Paper: Doctors or Dentists?
*   Is the Paper Receipt Dying?
*   The Resurgence of Holiday Cards
*   The Rise and Fall of the ‘Thank You’ Note
*   Why Carbon Paper is Experiencing a Resurgence
*   Is Paper Getting Thinner?
*   The Death of the Phone Book: Annual Sales of Phone Book Paper from 1950 to Present

These are just ideas using the data generated from the company’s information systems. But they are also questions and topics that at least _some_ people in the world would be curious about, and Dunder Mifflin can address them.

But this book is not just about writing about data, it’s about writing about information. Data is one large and important type of information, but there are others. If you work in the paper industry, you know things (or can research things) about your industry that other people will want to know:

*   Why Do Certain Types of Paper Cause More Jams?
*   A History of the Most Expensive Paper
*   The Technological Innovation That Made Paper Thin
*   How Many Trees Does the Average Bathroom Use Per Year?
*   How to Make Ancient Egyptian Papyrus Paper At home
*   The Man Who Made a Billion Dollars Selling Paper
*   The Family Business That Changed the American Paper Industry
*   Why Is Paper Cheap but Ink Expensive?
*   What Kind of Paper Was the Constitution Written On?
*   The Most Valuable Paper Documents Ever Sold
*   How Cheap is Newsprint to Make?
*   The History of the Phone Book
*   What Is the Most Paper Ever Used in a Legal Case?

Once you write this stuff, where does it go? First, believe it or not, there are dozens of people who likely cover the paper industry and need things to write about. An analysis of trends in the paper industry helps them do their jobs. An interesting anecdote about paper is something they’d link to, because no one ever writes stories in which paper is the protagonist!

Beyond journalists, people like to read (and share) stories that are about their industries, hobbies, and home towns. If you work in the paper industry, how often are people writing awesome stories about the history of your industry, or using data about paper to make an interesting point? Or say there’s a lawyer who deals with tons of paper every day; when an article shows the data behind his misfortune, he finally has a way to convey that aspect of his job to his friends. Writing articles that people want to share is so important that we’ve devoted an entire chapter to the topic later in this book.

You can look at the paper industry and think, “_Gee, what a boring industry_.” But the reality is that there _are_ interesting stories there -- you just need to dig them out. To do this, you have to cultivate the attitude that everything is interesting. Every industry has a history, every set of data has an insight, and every person has a story.

You got that? Everything is interesting.

**Enterprise Data for Fun and Profit: The Airbnb Case Study**

One of the ways we make at Priceonomics by using web crawlers to extract data from web pages, which we then present in usable formats for our customers. Our customers ask us to do things like analyze hotel prices in Paris from hotel search websites, find out how much iPhone 5Cs with 32GB hard drives sell for on online auctions, or extract book reviews from the Internet.

The actual work we do for our customers involves countless hours of writing computer code to get the customized data they need. We get the most out of that work by finding ways to make it interesting so we can write content about it and acquire customers.

Let’s start with an example of one of our most successful content marketing campaigns: a comparison of the price of staying at hotel versus an Airbnb. 

Airbnb is a website that provides a platform for people to rent out extra rooms in their home, or rent out their entire home. So, when you’re traveling, you can stay at a hotel or at someone’s house -- an Airbnb. Intuitively, we know that it’s likely cheaper to stay at an Airbnb, especially if you’re only renting out a room. But we wanted to quantify, using data, how much cheaper it really was.

We used our company’s product (our web crawling service) to figure out the exact answer. We crawled through every Airbnb listing in the United States, as well as every hotel listing, to compare the prices. It took over two weeks of engineering time just to get the data, a week to analyze it to see if it made sense, and another week to visualize it and write a brief blog post describing the data.

Our post compared the price of staying at a hotel to the price of staying in a private room (in someone’s home) or an entire apartment rented through Airbnb -- for every major American city. At the time the article was published, it was 49.5% cheaper to stay in an Airbnb private room than at a hotel, and 21.2% cheaper to rent an entire apartment.

After we hit publish on the blog post, we _did a few things_ to make sure people saw it. We’ll discuss how to share your content marketing later in the book, but it’s nothing you can’t do yourself.

Like almost every successful content marketing campaign we’ve ever run, at first nothing happened. But gradually, after hitting the front page of Hacker News, the post got in front of the right people. An industry blog called Skift, which focuses on travel technology, wrote about it. Larger blogs like Lifehacker, Gizmodo, and GigaOm wrote about it. Then the big guys started covering it: CNBC, The Financial Times, TIME, Businessweek, and The Huffington Post all wrote about our study and gave a nod to Priceonomics.

The media firestorm cumulated when actor Ashton Kutcher, who is an investor in Airbnb, shared the report with his tens of millions of Twitter followers. To this day, when major media publications talk about Airbnb, they mention the Priceonomics report.

We did not solicit any of these media features through a pitch. The press came to us, instead of the other way around. Our data created an inbound public relations process.

Of the hundreds of media sites that wrote about our study, not a single one contacted us to inquire about the analysis. They simply took the chart and wrote a story, no questions asked. Why? Because we helped them do their job of creating interesting content, and we made it easier for them by supplying them with easy-to-understand, new information.

You can do this too. What if instead of pestering journalists to write about your company, you created content that was so good that they want to write about it? That’s the process we created; if you create great information, you have a chance of developing a similar process at your company.

**Other Data Marketing Success Stories**

This is not easy to do. But Priceonomics and OkCupid are not the only ones to pull it off.

The financial website FutureAdvisor publishes financial advice pertaining to investments and spending. We first heard of it when the company did a very clever piece of content marketing with its data. FutureAdvisor analyzed the credit card data of millions of customers to come up with interesting -- and publishable -- insights.

The company hit on an incredible idea when it decided to use its credit card statements to calculate how many people were using “ride sharing” services like Uber and Lyft. As it turns out, Uber was dominating the a market. People intuitively knew that Uber was a more successful company than Lyft, but now they had real data that confirmed that hunch. 

FutureAdvisor crafted its findings into a report -- “Study: Uber Pulls Ahead of Lyft in Riders and Revenue With 12x Lead in U.S” -- and disseminated it to the press. (We know they did so because they emailed the Priceonomics Blog asking if we wanted to write about the data).

That day, every technology site wrote about the Future Advisor study. It was great information that helped reporters do their job: write articles that are likely to be popular. In this case, the data was about Uber, one of the most talked-about technology companies in the world. And it was data that settled the “score” between Uber and its arch-rival, Lyft.

Real Estate companies are notoriously excellent at turning their data into media mentions. Read a _Wall Street Journal_ article about rising rent prices and you’ll likely see references to  companies that sell rental data to investors. Likewise, articles about rising real estate prices often reference the S&P Case Shiller Index, a data source owned by the company Standard & Poors (S&P). Data like this _helps_ reporters.

People love this stuff and journalists know it! At the end of the year, you always see lists like “The Most Popular Google Searches of the Year,” or “The Year’s Most Played Artists on Spotify” because journalists and the general public love getting information. This is precisely why people are always quantitatively ranking things like “The Top Schools in America,” or the “Most Diverse Cities in America.” 

If you write a story about data and make it accessible enough, you’ll appeal to that little data nerd that resides in everyone.

Many modern companies have proprietary access to information. You don’t have to be Google, Spotify, Priceonomics, or US News to have access to data. Everlane, a clothing company, produced awesome data showing how badly other companies rip you off when they sell you “designer” t-shirts. Mailchimp, a company that helps other companies send emails, analyzed the most effective times of day to send emails (as measured by how likely someone is to open it) and turned the results into fantastic content. 

This same strategy can be applied to any industry.

Say you own an ice cream shop. You know how many ice cream cones you sell per day. You can also look up the average temperature of each day. Create a chart with temperature on the x-axis and ice cream cone sales on the y-axis, and you’ve got excellent, data driven content.

If you run a tire company, use your data to calculate which season is the worst for flat tires. If you’re a fire insurance company, determine the cumulative probability of a 10-unit building having a fire sometime in the next 10 years, then gauge it with 100-unit or 1,000-unit buildings. If you’re an accountant, gauge the average number of emails you get everyday with the number you get as tax season approaches. 

Every company has access to information that contributes to a better understanding of the world, or at least of its little niche.

**Write About Information, Not Necessarily Data**

In our first Priceonomics blog posts, we wrote almost exclusively about our data. If you recall, to power our first product (a consumer price guide), we had built a data set of used item prices. We did popular analyses on the prices of used bicycles, used American cars versus used Japanese cars, used e-readers, and used televisions.

We were seeing a lot of interest and getting lots of attention from these articles, but we got bored of writing about the prices of used goods. We decided to instead write about a market that was relevant to Priceonomics. In hindsight, this was a great decision. You have to keep things interesting for yourself, otherwise your enthusiasm will wane, and it will show in your writing.

That said, we only started exploring broader questions about industries _after_ we’d mastered writing about our own data. We recommend you publish at least a dozen reports about your company’s data before you broaden your subject matter. Do not skip ahead just because it’s more fun than writing stories about your company’s data.

But which industry? We had lots of traffic from people who found our database of used bike prices, and many of us love bikes and commute to work. As we debated what we could write about, we started asking, why do so many bikes get stolen?

In our case, we’d written about bicycles before and knew that people loved talking about bikes. We had analyzed where the most fixed-gear bicycles (“fixies”) were sold in America and had used that to construct a tongue-in-cheek index of the most “hipster” cities. (A common stereotype is that hipsters like riding fixed gear bikes.) We were inundated with press, and every single “hipster blog” in Brooklyn wrote about us.

Bike theft was a mystery to us. We knew it was an epidemic in American cities, but we were confused by its prevalence. It’s not like bikes are a highly prized commodity among criminals like drugs or phones.

We did some secondary research on the market for stolen bikes, made a few calls, and used some economic frameworks to figure out why so many bikes get stolen. It was an in-depth process that could be construed as “journalism.”

When we published our blog post “What Happens to Stolen Bicycles?”, it became, by far, the most-viewed piece we had ever written. The post generated over 100,000 visitors in two days. It drew hundreds of links that improved the SEO rankings of our database of bicycle prices, and NPR even featured us on a radio segment.

Writing about the economics of stolen bikes rejuvenated our blog and gave us more industry-focused topics to write about -- topics which, in turn, helped bring new information into the world. We wrote a post called “Diamonds are Bullshit” that over one hundred thousand people shared on Facebook. We wrote about the cost of booking bands, which has been viewed almost two million times. We wrote an analysis on the mattress industry, which sent us a massive amount of traffic and got referenced on TechCrunch.

Since we opened the blog up to stories outside the company data set, the Priceonomics Blog has really taken off. But here’s the thing: this type of content does not directly lead to customer leads for your business. That’s not to say it isn’t valuable, but we don’t recommend you start out writing entertaining analyses on random topics. Our theory is that your content should be a mixture of things that promote your products and things that don’t. We’ll get deeper into this a bit later in the book.

If you’re ready to write a piece outside your data, you should ask yourself which topics your company can write about that take advantage of your industry knowledge or expertise. Previously, we gave all sorts of examples of topics Dunder Mifflin's blog could tackle. 

What if you ran a lawn mowing business? Would there be anything interesting to write about? We think so:

*   How much water do different types of grasses need? 
*   What is artificial grass, and how much does it cost to install? 
*   What’s the predominant type of grass in various regions of the US?
*   When did Americans start planting grass lawns?
*   Do people still use manual lawn mowers?
*   How much time will it take to mow a lawn depending on the square footage and layout?
*   Do lawn mowing companies have the right kind of insurance?

The average person would struggle to write these posts and present novel information. Someone in the lawn care industry could.

**What Other Kinds of Information Does Your Company Have?**

Your company has access to proprietary data you can write about. Because of your industry and functional expertise, you can also research your industry more effectively than the average journalist.

But information is not limited to data: you can also write about people’s lives. There is an expression that the plural of anecdote is data. Other people say that the plural of anecdote is _not_ data. Either way, telling a story that your company has access to can be powerful.

Recently, we sat down with someone at a technology startup who was struggling to find something to write about for the company’s blog. The company had raised a large round of venture capital, led by the storied investment firm Kleiner Perkins. The company’s founders had to figure out how to grow faster, and making great content was an essential part of their plan.

We suggested they write a post about what it’s like to to raise money from Kleiner Perkins. A few months later, this company published a great account of how its venture capital financing came about. They refined the idea we suggested, made it better, and told their story in a way that only they could. They posted the story on their company blog, and it generated exactly what they wanted: lots of media attention and visits.

Why was this a good piece of content? Well, the company knew what it was like to get Kleiner Perkins to invest, and lots of people want to know how they can get funding from one of the most successful investors in Silicon Valley. Authentic knowledge that your company has (or can acquire) is a very powerful form of information -- even if it’s just relating a subjective anecdote.

One of the core tenets at Priceonomics is that everyone has an interesting story. Every person you come into contact with on a daily basis has a deep-rooted story about heartbreak, triumph, tragedy, or comedy. 

The key to writing about stories, anecdotes, and small pieces of data is this: Stick to what you know. Don’t extrapolate and try to turn an anecdote about someone’s life into something else. If you want to talk about the founding of your company, just tell that story. Don’t try to stretch it into some defining moment of your industry, or a world-changing event; your audience will roll their eyes and dismiss your content as lame marketing. Just talk about what you know, and stick to the facts.

One of the most popular classes at Stanford University’s Business School is informally called “Touchy Feely.” The class consists of a group of students meeting several times a week and sitting in a circle without any agenda. Of course, everyone knows that the class is called “Touchy Feely,” so eventually people start talking about their feelings.

After a few weeks, the conversation shifts to how people feel about the other group members. Often, people start offending each other -- “You’re always cutting me off,” or, “You think you’re smarter than me because you worked in private equity and I worked at a non-profit”. Somehow all hell breaks loose, and a group of otherwise very polite people act very impolite.

But the tense discussions eventually reveal the magic formula for getting along with people. It’s an expression called “stay on your side of the net.” Don’t pretend you know what’s going on inside someone else’s head; you can only truly know what’s going on in your head. You don’t know that someone else thinks they’re smarter than you. You just know how you feel about something.

After the group grasps that they should talk about their feelings instead of accusing other people of feeling some way, everyone starts to get along. By the end of the class, the group has bonded and people form new friendships. Accusations like, “You think I’m not smart,” are rephrased to, “I feel like my intelligence is being disrespected.” When you stick to your side of the net, people listen to what you’re saying.

You don’t have to do marketing this way, but at Priceonomics we do. We tell you, “Here’s the information we have.” We’d never say, “Here are ten reasons you’ll _love_ this product,” because we don’t know you or what you love. We’d never say “Google is evil,” because we can’t prove that, and we don’t know anything about their motivations. Instead, we’d say, “The data shows Google has gradually de-emphasized organic search and now focuses on getting more clicks on its ads.”

Our style is to give you information and let you decide how you feel: Here is information about a book we just wrote, here’s why we wrote it, and here’s the link to buy it. Will you like it? How the hell are we supposed to know? That’s the Priceonomics way of doing content marketing.

So, what are easy ways you can write about this kind of story-based information on your company’s website? Start by interviewing people at your company and writing about them on your site. We know this may sound boring, but it doesn’t have to be! Everything is interesting, remember?

Find the person who has the most boring job at your company (system administrator?) and talk to her. How did she get interested in computer networks? Did a teacher or mentor spark her interest in technology? What sacrifices did her parents make to help her succeed? What is it like to be a system administrator? What are the aspirations of a system administrator? What is satisfying about the job?

You have to truly believe that everyone has an interesting story to tell, and that it’s your job to tell it properly. It’s so easy to look at someone’s job and think it’s boring -- but trust us, it’s not. Once you figure out how to pull this off, you can write about everyone at your company an executive, a salesperson, a receptionist. If you relate your employees’ stories in a heartfelt way, it will also highlight the human side of your company.

After you’re skilled at writing about the people who work at your company, start talking to your customers. In most cases, they’ll be thrilled to have you profile them. Tell their stories: what they do, their tribulations and triumphs. If they’re companies themselves, talk about how they were founded. Ask how they use your product, but don’t make that the centerpiece of their story. 

Some of the most popular articles we’ve written are simply the founding stories of companies. Here’s the introduction of a story about a company called AnyPerk, a company that offers employee benefits:

> Things were not going well for Taro Fukuyama and Sunny Tsang and their team during Y Combinator’s Winter 2012 batch. The team was accepted into the startup incubator as a dating site called Mieple. But the idea wasn’t getting traction with users, so they decided to pivot ideas just as they arrived in Mountain View for the start of the program.
> 
> During the first month of Y Combinator, Fukuyama and Tsang went through six pivots and evaluated seven different business ideas. First, the dating website premised on introductions through friends. Then, a service for getting introductions to investors. Next, a service for introductions to jobs. After they got tired of “introduction-based” business ideas, they tried a few random ones: a translation company, advice about which movies to watch, a service to learn new skills like cooking by Skyping with an expert. 
> 
> YC partners shot down many of the ideas, and the founders discovered the rest to be duds when they spoke to potential users. Toward the end of the first month, the team met with Paul Graham, the head of Y Combinator. He said they were currently the worst company in the batch and that they should take that as motivation to do better.
> 
> So they did better. The founders hit on their idea on the 7th try, one month into the three month incubator program. They founded AnyPerk, a startup that lets companies offer discounts and perks to their employees. The company now has 28 employees and makes serious recurring revenue every month. Two years after going through Y Combinator, they’ve jumped from the worst in the batch to near the top. 

AnyPerk is a company that offers employee discounts, but our telling of its story doesn’t start that way. It starts before that, and shows how the founders built a great company after initially failing. If you put in the effort, you can write an engaging article about almost any company. Heck, if producers can make a popular sitcom about a boring paper supply company called Dunder Mifflin, you can do the same!

Don’t believe that you can make the story of office supplies interesting? Here’s a chunk of an article we wrote on Priceonomics about the invention of Scotch Tape:

> In 1921, 3M hired three men to oversee product innovation -- a move that the company itself cites as “one of the most harmonic convergences in the annals of business.” Among the men was one Richard Drew.
> 
> From a young age, Drew expressed disinterest in the traditional American workplace. Throughout his youth and well into his teens, he pursued a career as a banjo player. After touring with local dance bands for a number of years, he scraped together enough money to pay for a mechanical engineering degree at the University of Minnesota. Just 18 months into the program, Drew grew dissatisfied and dropped out. Instead, he turned to the local classified ads, saw a listing for a job at 3M, and decided, on a whim, to go for it, complete with a quintessential application letter: 
> 
> “I have not as yet been employed in commercial work and am eager to get started. I realize that my services would not be worth much until a certain amount of practical experience is gained, and I would be glad to start with any salary you see fit to give...I am accustomed to physical labor, if this be required, as I drove a tractor and did general farm work.”
> 
> The 22-year-old was hired, and thrown into the lower dregs of 3M: for two years, he was tasked with testing out various grains of sandpaper. Eventually, the company sent Drew out to local auto shops, which were 3M’s most frequent customers at the time, to disseminate the sandpapers for testing.
> 
> At the time, in the 1920s, two-tone paint jobs on cars were all the rage. For auto workers, this was a total pain in the ass: To achieve this effect, they had to mask off parts of the car with butcher paper, newspapers, homemade glue, and heavy-duty surgical adhesive tape. When the tape was removed, it would often take with it chips of the freshly-coated paint. A vicious cycle would ensue of taping, painting, re-taping, and re-painting.
> 
> When Drew walked into the shop that day, he was greeted with the “choicest profanity” he’d ever heard: strong adhesive tape had, once again, botched the auto workers’ paint job. Instead of seizing the opportunity to sell the disgruntled workers sandpaper to remove paint, Drew had a completely unrelated revelation: what if he could design a superior, less aggressive tape -- a tape that didn’t ruin paint jobs?
> 
> He made a vow to the auto workers that he’d soon return with a solution, and darted out of the shop.

Drew came back with a newly-invented tape that helped auto workers paint cars more efficiently. This product, later named “Scotch Tape” by 3M, had many other applications. The rest is history.

Tape may be the most boring subject in the world. But this blog post generated over 100,000 visits to our site and thousands of social media shares. Even Office Depot and OfficeMax shared it with their followers on Twitter. It seems there aren’t many interesting articles about office supplies published online: we filled the niche.

\*\*\*

Information is valuable. It gives reporters something to write about; it brings potential customers to your door; and it establishes you as an expert on your subject.

The best kind of information to write about is data that your company has access to. Even still, because you’re a smart person working in a specific industry, you can talk about that industry and its issues in an informed way.  And if you cultivate a mindset that everyone has an interesting story, you can tell the stories of the people and companies that are important to you.

But as you write about information, it’s important to keep in mind that the upside of doing a good job is enormous, but the upside of doing an average job is absolutely zero.

[![](https://pix-media.priceonomics-media.com/blog/1085/Screenshot2015-12-0821.12.47.png)](http://eepurl.com/bVruOT)

**Chapter III: The World Is Flat  (The Level Playing Field)**

Peter Thiel, the billionaire founder of Paypal and the first investor in Facebook, believes that the best interview question is, “What is something you believe that nearly no one agrees with you on?”

Here’s something we believe: If you write something good, it doesn’t matter if it’s published on your obscure company blog or on _The New York Times_. It can be successful either way. In fact, it’s probably better for your company if you write incredible content on your blog rather than letting other publications publish them as op-eds or guest posts.

In this chapter, we’re going to walk you through how the content publishing world works. Consider it your playbook on spreading the things you’ve written. If you can pull it off, you may come to agree with us that publishing content in _The New York Times_ is for suckers.

**Where Traffic Comes from Today**

Thirty years ago, before the internet, the way you got your message out was to beg newspaper reporters to include it in their articles. This is called Public Relations, and companies with lots of money and relationships can still do this today.

With the emergence of the World Wide Web, huge websites and portals became popular. People would type in “nytimes.com,” or “slate.com,” or “yahoo.com” into their browser and read whatever was there. For a while, the best way to get your message out was still to use PR to get these new big players to write about it. But the media landscape was shifting.

Around the turn of the century, Google emerged as the dominant source of traffic and customers on the web. To get your message out, you could still engage in expensive outbound PR. But, if you were clever, you had a new way to reach customers: by churning out content and hoping that people found it through search queries. It didn’t really have to be great, but it had to have the right mix of keywords to exploit the search algorithm.

The front pages of news websites started seeing fewer and fewer visitors. Instead, most of the traffic these large news sites saw was from Google users searching for random articles. People would perform Google searches, get linked to a web page, briefly skim it, and then return to Google without really caring which website they just visited.

In the current era of publishing, social networks are surpassing Google search as the primary way people find content. Today, virtually every content website’s largest source of traffic is Facebook. Content has to be so exceptional that people will choose to go through the effort of sharing it with other people: On Facebook, Twitter, Reddit, Tumblr, and even email. Like before, when someone shares an article on Facebook, the reader clicks through, reads the article, and returns to Facebook without really caring which site they visited.

Even _The New York Times_’ homepage is dying. In the newspaper’s “innovation report,” published in 2014, the company reported that less than 1/3 of its visitors come directly to the front page. The rest find random articles through Google searches and social networks.

The reality today is that publishers of content don’t have many true “users” -- at least, not in the brand-loyalty, check-it-every-day sense that they did decades ago, before the internet, or even at the onset of the internet. When it comes to content, the sites that have many dedicated users are _platforms_: Facebook, Twitter, Google Search, Gmail. 

While this is bad news for say, traditional incumbents like _The Washington Post_, it’s not bad news for an upstart or a startup. You’re on a level playing field with the big guys, because your content can be amplified on the web using the same mechanism that they rely on. Your company can be an information source on par with the biggest traditional media sources in the world.

**How to Break News: Priceonomics on Square**

Priceonomics focuses on publishing _information_, not news. News is a form of information with a very short shelf-life. The “evergreen” content we focus on is relevant in perpetuity. But we can write about things that would be considered “news” -- and when we do, we can do it just like the big guys.

Sometimes news just falls into our laps. There’s a sandwich place we like to order lunch from around the corner from our office. One day, we noticed that they had an option where you could order sandwiches online, in advance, using the payment company Square, and then pick them up when they were ready. 

This was a brand new product from Square -- the sandwich place was doing a pilot of this new product, because Square’s offices are right above the deli.

A quick Google Search revealed that Square’s new service wasn’t yet public knowledge. So, we wrote a short blog post and published it on Priceonomics. At first, nothing happened. It was a boring post, and nobody seemed to care about some minor news development from a payment processing company.

But some people do care about a minor development from Square: many tech journalists cover the company as part of their beat. So again, we _did some things_ that got the story in front of the right people. Earlier in this book, we were coy about how to share content, but in this chapter, we’ll get more specific.

To get this story out, we wrote a tweet about the article, and appended the words “tip@techmeme.” What does this mean? Well, it meant that we were submitting a tip to the tech news aggregation site TechMeme, a website that lots of tech industry reporters read.

![](https://pix-media.priceonomics-media.com/blog/1085/Screenshot2015-12-0812.57.51.png)

An editor saw the tweet, decided our article about Square’s new product was newsworthy to Techmeme’s audience, and posted it to the site. From there, technology journalists discovered it, and Priceonomics was cited by TechCrunch, The Verge, Business Insider, and almost every technology blog.

This anecdote isn’t meant to suggest that you should break news on your corporate blog. We only did this because it took almost no time for us to write, and we were curious to see what would happen.

But this story offers two lessons. First, your content is competing on a level playing field. You can write something, publish it on your site, and it can be popular. It can go viral and be cited by mainstream media publications even if it’s posted on the blog of an unknown company.

Second, when you write something, even if you have a decently sized audience like we have at Priceonomics, you have to _do some things_ to make the content successful. So what exactly can you do?

When you write content, you must have a channel in mind for that content to spread. It’s important that you publish incredibly useful information, but unless you think about _how_ that content will be distributed to the audience you have in mind, nothing will happen.

You need a plan for how your content will spread, and it has be in place before you start writing.

**Content Channels**

If you talk to an advertising agency, it will be mostly about Facebook, the largest source of traffic to content sites on the Internet. Someone at an ad agency can make you a “social media” plan, but most of that plan will actually be about Facebook.

This is _not_ the way you should think about content.

Facebook is one of many channels on which content can spread. When you start thinking about how your content will spread, don’t start with Facebook. While Facebook is the largest source of third party traffic to Priceonomics, traffic from Facebook is a _symptom_ of other content channels working well -- not a primary _cause_ of our traffic.

For most of our most successful pieces, we did something else first -- something outside Facebook which made the article popular. Only then did people start sharing it on social sites like Facebook. Most articles we write aren’t popular on Facebook until many other things happen to make them viral.

After you write a piece of content, you need to take steps to get it in front of the right audience. Therefore, before you start writing, you need to have a plan for how the article will spread. If you can’t come up with a plan for an article, write a different article.

**Our First Content Grand Slam** 

The first enormously successful piece of content marketing we did was an analysis on the used sales prices of iPhones versus Android phones. We calculated how quickly each lost its value after initial purchase, and then we published a report about how iPhones had better resale values than Android phones.

Can you guess what happened next? That’s right: Nothing, until we _did some things_.

There are many blogs dedicated to writing about Apple and mobile phones, so we made a list of 50 of them and emailed each one individually about our study. We didn’t have any personal connections, so we emailed the generic addresses on their “Contact Us” pages.

Of about 50 emails, we got zero responses back. This was not ideal. The team had collectively spent hundreds of hours assembling the data, analyzing it, and figuring out how to present it. It would have been enormously demoralizing to strike out after putting in so much effort.

So we thought about other people we could email. We remembered that a reporter at TechCrunch (the 900-pound gorilla of tech blogging) had previously written about our launch. We wrote him to see if he’d be interested. Here’s a lightly edited version of what we wrote:

> Hi Josh,
> 
> Hope all is well, thanks so much for writing about us when we launched a month ago!
> 
> Just wanted to let you know Priceonomics just published some interesting data about how fast phones depreciate (iPhone v Android v Blackberry).
> 
> https://priceonomics.com/phones/#cell-phone-depreciation
> 
> Basically the iPhone dominates in resale value rankings. Thought you might be interested in this!
> 
> Best,
> 
> Rohin

His response a few hours later: “Great study, I'm writing it up now.” He then asked a few questions about the data and put together an article about our study. A few hours later, TechCrunch published an article, “Study: iPhone Resale Value 63% After One Year, Android 46%,” which cited and discussed the Priceonomics report.

The TechCrunch article led to an avalanche of coverage from sites like GigaOM, The Atlantic, CIO.com, PCWorld, TUAW, Apple Insider, Cult of Mac, Macworld, and ZDNet. Some of the websites that had ignored our pitches wrote about the Priceonomics cell phone study.

The piece was an epic success, but it nearly failed. If we hadn’t emailed dozens of reporters, all our hard work would have been for nothing. 

It isn’t enough to publish great information. You have to wage a campaign to make sure that the right people find out about it. More than 50 journalists rejected us, but finding one person who wanted to write about our study made the post successful. 

**\*\*\***

When you start writing content based on your company’s information, force yourself to send at least 50 personalized emails to people who you want to cover your report. Your email to them should be short and have one goal: pique their interest enough that they’ll click the link and read your content.

If you can get them to do that, then your email campaign is successful. If your content is good enough and journalists see it, they’ll write about it. But you need to be covered on both these fronts: the content has to be good and journalists need to see it.

This is what’s called an outbound process. You are “reaching out” to journalists to see if they are interested in your information. You don’t need to have personal relationships, or an expensive PR firm (which is not to say these things don’t help, _if_ you have the resources); you just need to present the information.

Having one or two people write about your information kicks off an avalanche of other people who write about you. This is the sweet spot: “inbound PR.” 

But the reality of inbound PR is that it doesn’t happen unless you put in work up front to make it happen. Sure, as you get larger and more successful, journalists will follow your company. If that happens, when you put out information, you don’t have to do _as much_ work. But you always have to do _some_ work so that the information you put out has a channel to spread.

We tell people to start off by emailing 50+ journalists, because it conditions you to work very hard to spread your content after you write it. People who enjoy making cool, data-driven content might hate cold emailing 50 people. But you have to put in unpleasant work if you want your content to be successful.

Another reason we tell people to email journalists after they publish a report is that we want you to anticipate what you’ll say to them _before_ you are creating the content. What’s the hook going to be? How will you pitch it to someone? Why will it be interesting?

The secret to getting inbound press is doing a little work that creates it. That means anticipating the channel that your content will spread through, and then making sure it gets there. Emailing journalists is a good way to start, but there are many other ways to spread your content.

**Supernodes of Distribution**

When it comes to content, the internet is just a network of people looking for stuff to read and videos to watch. Journalists are some of the network’s “supernodes”: they curate information and then relay it to _lots_ of other people. This is why emailing journalists is a good way to get your content to millions of people.

You want your content to reach millions of people, but that is prohibitively expensive for your company to do on its own. Instead, _focus your efforts on convincing a handful of people who are gateways to a larger audience_. That’s a much more tractable task.

Journalists are one form of supernode. They’ve been gatekeepers of audiences for hundreds of years, so everyone knows that they are influential, which is why people flood journalists’ inboxes with thousands of pitches. But journalists are not the most effective supernodes for Priceonomics.

The most powerful supernodes on the internet right now are social news sites. These are sites where users -- not journalists -- decide what is on the “front page”. They send your company far more traffic than a mention in _The New York Times_ or on CNN. Social news sites include huge sites like Reddit, Digg, and Hacker News. They also include medium-sized sites like Metafilter, Fark, Designer News, and Product Hunt.

Here’s how most social news sites work: A user of the site submits an article for people to read. If, in a short period of time, a few people “vote” in favor of the article, it will be featured on the front page of the site, usually near the bottom. Many people read the front pages of these sites. If your content is great, more people will vote for it. It will rise to the top of the rankings and drive tens of thousands of visitors to your site. If your content is average (or below average), it will be downvoted, and it will quickly fall off the front page.

In our view, these sites are the single most important part of content distribution. They make the problem of figuring out how to share great content much more feasible. When you reach out to journalists, the question is, “How do I get that _one_ journalist to write about my content?” That’s a hit or miss proposition. With content sites, your problem is, “How do I get handful of random people on the internet to like my content so it will make the front page of one of these sites?” They can be _any_ handful of random people that actively use the site, not a few specific gatekeepers.

The beauty of these sites is that they democratize the sharing of content. If your work is good and appeals to enough of their readership, it will get voted up. If it isn’t, it won’t. You can submit the content yourself, but before you do that, you should be an active user of the site. If you run a bicycle shop, spend time geeking out on the biking sections of Reddit (called sub reddits). Read the forums extensively, comment on posts, and submit things you think others will like.

Only submit your content to social news sites if you know they will like it. Do not ever post things that are purely self-promotional: you will be crucified by Internet commenters. To extend the bicycle shop-owner example, you would never post on Reddit, “Come to my bike shop and buy things.” Pure self-promotion does not conform to the community’s norms and will not be well-received.

If you started a bike shop, though, you likely have lots of information that people in the bicycling community might be interested in. How much did it cost you to start a bike shop? Where do you make money? How do you find a location for a bike shop? What people do you need to hire? Can you actually make money in the cycling industry?

At Priceonomics, we once wrote an article that featured some of our friends who had started a bike shop. We posted it to Reddit, and it was very popular. The article ended up being one of the biggest PR coups that bike shop ever had. Why? Because it featured lots of great information about starting a bike shop. Here’s a part of the article about HuckleBerry Bicycles:

> On day zero, when Huckleberry was still a concept, they calculated they needed $300-$350K to get started if they managed their costs aggressively and did a lot of the work themselves. That amount would allow them to find and renovate a retail spot, give them a budget for initial inventory, hire a first employee, and provide a buffer to last at least a year. 
> 
> Because two of the founders were former bike shop employees, they had a fairly accurate idea about what the costs would be. They projected they’d need a 2,000 square foot store and the rent would be $6-8K per month (they only targeted less expensive neighborhoods). Renovating the store would cost about $100K (they would do most of the work themselves). The initial inventory to get started would be about $75K, and $25-50K would cover miscellaneous expenses. They left themselves $100K as a buffer so they could confidently hire a fulltime employee to run their service center. As it turned out, their actual expenses were in line with their initial projections (nice work!).
> 
> The founders would put in a small amount of initial capital, but the vast majority would be financed. They considered bringing in additional equity investors, but ultimately decided they were looking for such a little amount of capital that it didn’t make sense to give away more of the company.

Once on Reddit, the article was shared all over Facebook and Twitter. The Atlantic even republished it. After its publication, some of the most important players in the bike industry reached out to this small shop. To this day, people come into the store saying they discovered them through this article. Why was this article so popular? Because it shared information that the founders at Huckleberry had special access to: hard data about how much it costs to start a bike shop.

Content that shares information is particularly well-suited to social news sites. The bullshit detectors of people on Reddit and similar sites keep the flimsy, promotional blog posts that most companies churn out at bay. Information that is a byproduct of your business, on the other hand, is _real_ and actually _helps people out_. To this day, when you search for information about starting a bike shop, our article about this bike shop is a top result on Google. This information does someone else a service.

If your content is fantastic, it will rise to the top of these news sites. When that happens, you get a torrent of traffic. 

At Priceonomics, we call this “The Bump.” When a critical mass of people see the article, everything good that can happen, will happen. Once 1000 or so people see the article at about the same time, that’s enough that the article will find its way to every journalist, partner, or potential customer interested in that content. Someone will email it to them, they’ll see it on Facebook or Twitter, or your article will show up when they Google something important.

Content marketing distribution is all about getting The Bump.

**Mastering Supernodes**

Here’s the good news about social news sites that send a lot of traffic and cause The Bump: if you write great content, you have a good chance of being rewarded. 

But for this to happen, certain other things have to happen first. Someone has to submit your content to the site. Within a small window of time, you need at least a handful of votes in order to make the front page. Once you get these votes, your content will rise and fall on its own merit.

How do you ensure this happens? It’s okay to post your own content to social news sites, but it looks bad if you’re constantly promoting yourself. Even if you do post your articles, it’s very hard to “game the system.” All these news sites have algorithms that detect when people create new accounts just to upvote stories. They can also tell when the same people tend to vote together, and they ban those accounts for being a voting ring. 

So you can’t game the system and artificially get The Bump from these sites. And unless the work you produce is absolutely stellar, there is no chance you’ll make it to the top of sites like Reddit, Hacker News, or Digg.

But once you get big enough and you have some readers, all this stuff starts to happen _for_ you. Other people start posting your articles to Reddit; random people who you don’t know leave comments on your articles; and journalists with large followings start following your company’s Twitter account. The amount of work you need to do to create The Bump decreases over time. If you consistently write great things, other people start _doing things_ that help you out.

Here’s how we evolved from a company that had to work for The Bump to one where it started happening on its own.

As we mentioned in the first chapter, Priceonomics started when we were accepted into the Y Combinator (YC) incubator program. Y Combinator, in addition to being an investment and mentoring company, runs a large social news site called Hacker News, where people submit and vote for content that is interesting to people who work at tech startups. Running the news site is Y Combinator’s own version of content marketing: the content and discussion attracts people interested in starting companies, some of whom Y Combinator hopes will apply to the startup incubation program.

This is to say, we were part of the Hacker News community before we started writing our own content. We weren’t the most active commenters, but our company was funded by YC, and we all read Hacker News _religiously_, like 20 times a day. So we had a very good idea about the kind of material that would be popular there, and what would be inappropriate.

When we started publishing articles and reports on our blog, we knew we wanted to post them to Hacker News and email tech journalists. We realized that the content we wrote should be the kind of content that we -- as avid readers of Hacker News -- would want to read.

This is a key point: as we created our content, we kept in mind the channels through which it would spread (Hacker News and technology journalists). It was a simple plan, but we had a plan.

The first five articles we wrote were: 1. Adventures in Aeron Chair Arbitrage (a story about scrappy entrepreneurs -- us); 2. Minimum Viable SEO (a primer on easy things startups can do to improve their search engine rankings, based on our experience); 3. The Fixie Bike Index (a quantitative analysis of where people buy fixed gear bikes, and therefore which city is home to the most hipsters); 4. How to Make It on Craigslist (the story of a person who turned his life around by starting a company); and, 5. Headphones for Hackers (a survey of the best headphones to use while coding).

Before we wrote each post, we anticipated which channel it could become popular on. Every one of those first five posts provided information about founding a company or technical advice, or was geared towards programmers -- we knew this was the kind of content that did well on Hacker News. There was a fit between our content and the channel. 

In the early days, the Priceonomics Blog got attention largely because Hacker News gave us The Bump that propelled us to greater recognition. At first, we only published blog posts every few weeks, so we submitted content on our own. Since we were in the Y Combinator program at the time, when our posts appeared on Hacker News, people took notice, which helped us get those few votes to make the front page. When the content was excellent, our blog posts skyrocketed to the top. When it wasn’t, our blog posts fell off the front page pretty quickly. Most of our content was excellent because we spent so much time making it.

Hitting the top of these social news sites is a much larger traffic event then being linked in an article by a journalist. Why? Well, say an article on TechCrunch gets 50,000 views. Only a very small percentage of the people who see the article will click through to your link (around 1% in our experience), so you’re only getting 500 people to your site. If you write an article that hits the top of a social news site, you’re getting those 50,000 people coming directly to your site. It’s 100x more traffic.

When we started publishing, we wrote new things every 2-3 weeks. That was infrequent enough that we had no qualms about posting our own articles to Hacker News. But eventually we focused more on our writing, hired full time writers, published articles every day, and stopped posting our content to sites like Hacker News and Reddit. When you publish that frequently, you can’t risk spamming these forums by submitting all of your content. We did not want to be clueless marketers without any self-awareness of appropriate submission decorum.

When we stopped submitting our own posts to social news sites, our traffic declined. We’d write something great and then...nothing would happen. In the past, when nothing happened, we’d _do things._ And one of our favorite _things_ to _do_ was to submit articles to social news sites. But we’d taken that option off the table.

Then, gradually, other people on the internet, people we did not know, starting _doing things_ for us. People who subscribed to our blog by email, Twitter, or Facebook would read our articles and then submit them to social news sites on their own. Eventually we got much more traffic from supernode sites like Reddit, Digg, and Hacker News, because other people shared our work there. Journalists started following us, and we received requests to republish our content at places like the The Atlantic, Gizmodo, Lifehacker, and Business Insider.

In the movie _The Jerk_, there’s a scene where the main character (a lovable idiot, played by Steve Martin) rushes to check out the latest version of the White Pages telephone directory. When he locates himself in the book, he exclaims: “I’m in print! Things are going to start happening to me now.”

And things started happening to us. When we published our content on the internet, amidst millions of other people doing the same thing, we started standing out. Our PR process, which had previously been “outbound” and involved reaching out to journalists or posting our articles to news sites, now became “inbound.” 

We produced quality information, and things started happening to us. Our fans submitted it to social news sites, journalists found it on their own, and the content started ranking highly in Google Search results. 

We’re very grateful to be where we are today. We’re still not at the stage where we can create crappy content that generates lots of traffic. (There are lots of media companies that do that, but we are hoping that it will be impossible soon.) If we make something _good_, though, good things happen.

But we still have to think about channels.

Even today, when we work on a piece, we ask the question, where will this article be popular? It’s a broader conversation now than it was in the early days. It’s no longer: “These individual writers might cover it.” Instead it’s: “Oh, I could see Digg liking it,” or, “This highly technical piece would be perfect for r/programming on Reddit!” It’s never a good sign if you’re writing something but can’t figure out where it will become popular. Just being “good” isn’t enough. Your articles need to be both “good” and tailored to a particular channel.

It’s wonderful that producing a dud (a piece of content that no one sees) rarely happens to us now. When does it happen? When we write something that’s really good without thinking about which channel it will spread on. When we forget that, we don’t give our content a chance to succeed.

Enough about us though. Should you post your company’s content to the social news sites that we’re calling supernodes?

Yes -- at least in the beginning. But you need to participate in the news site as a community member for a while before doing anything with your own content. Understand the content people like and the norms around posting it. If moderators keep deleting your content, figure out why. 

Maybe it’s not very good or it’s not appropriate for that particular channel. Maybe you need to find new forums for your work, or change the kind of content you’re writing. Or, if you’re like us and start creating lots of content, eventually you can stop trying to spread your articles everywhere and other people will do it for you. But that position is earned by consistently creating content that people love so much they’ll want to share it with others. Don’t try to game the system, because you can’t.

If you’re writing about information, you’ll find that there are a lot of receptive places out there for your content. It’s one of the few forms of marketing that is okay to post on these supernode sites. Why? Because information is valuable. If it’s presented in an unbiased way, it’s useful for companies to share it.

\*\*\*

In this chapter, we argued that there is less and less of a difference between publishing an article in _The New York Times_ or on your company’s blog.

It’s an open secret in the media that everyone relies on third parties -- primarily Facebook -- for almost all of their traffic. You can access those same sources of traffic.

An article in _The New York Times_ relies on people sharing it, just like an article on your blog. It’s a level playing field. If you write something great and anticipate where it will spread, you can reach millions of people.

[**![](https://pix-media.priceonomics-media.com/blog/1085/Screenshot2015-12-0821.19.53.png)**](http://eepurl.com/bVruOT)

**Chapter IV: Social Networks**

In the last chapter, we made the argument that your company’s content can be just as successful as content created by media companies. We’re not idly saying this: the average blog post at Priceonomics (a company blog most people have never heard of) gets about 25,000 views. That’s more than at most brand-name media companies. Every content site gets its traffic from big platforms: Buzzfeed does, _The New York Times_ does, we do, and so can you.

This chapter is about social networks, but mostly it’s about Facebook, which is the biggest source of traffic for most content blogs. In 2014, 1.8 million visitors came to Priceonomics from came from Facebook. Twitter placed a distant second with 10 times less traffic (that’s not to say that Twitter is unimportant, as we’ll discuss later). LinkedIn, Google+, and Tumblr each accounted for less than 1% of Priceonomics’ social traffic.

Facebook was the largest third party traffic source to Priceonomics by far, and yet we consider it less important to our success then the supernodes -- especially social news sites -- we discussed in the last chapter. Why? Things go viral on Facebook _only after a critical mass of people have seen it_. The Bump is a necessary, but not sufficient, step for something to go viral. Content almost never goes “viral” just because of Facebook: a large number of people need to be exposed to it first.

So, if The Bump is a necessary condition for something to go viral, why focus on Facebook at all? 

**The Power Law Applies to Everything, Including Content**

When early-stage venture investors think about their returns, they often think about something called ‘The Power Law’: If you invest in early-stage companies, most of your fund’s returns will be dominated by the one or two mega-successful companies you fund. If you invest in 50 early stage startups, 49 of them can fail and you’ll still make a lot of money if that 50th company turns out to be the next Google.

Just like venture investing, content marketing is a hit-driven business. In 2014, Priceonomics wrote 319 articles. Of those, 10 of them made up over 50% of our traffic. Those were our hits. And for the hits, the overwhelming source of traffic came from going viral on Facebook.

![](https://pix-media.priceonomics-media.com/blog/879/Screenshot2015-01-0809.55.55.png)

If your goal is reach, Facebook is very important. And if you want traffic from Facebook, you have to think of it as a channel into itself. It has its own set of dynamics, built around one particular question: “Why do people share?”

**Facebook is the New Television**

In prior generations, after parents put the kids to sleep, they’d crawl into bed, turn on the television, and zone out. In this author’s household, we pride ourselves on not having a TV in the bedroom. Instead, we grab our phones and check what content people are posting to Facebook. One night, we realized we’d spent an average of an hour on social media before bedtime. The phone has replaced the television set, and Facebook is the channel guide that tells you what content to consume.

While in the past you probably didn’t watch television at work, you now likely spend some time online clicking links to articles and reading content that people have shared on social networks during business hours. Buzzfeed, a website that reaches hundreds of millions of people and generates billions video views each month, was built on the premise of reaching [people who are](http://sara-weber.com/post/77404854237/buzzfeed-founder-jonah-peretti-about-being-bored) “bored at work.”

At Priceonomics, an overwhelming majority of our traffic comes from people during their working hours. This is especially true of traffic from Facebook, which, it’s worth noting, is mostly mobile traffic. 

Facebook has evolved quite a bit since its inception as an online directory of Harvard students. For most of its existence, it was a place where young people shared information about their lives with people they knew. Today, personal content makes up a sliver of the typical Facebook newsfeed. Links to written content, graphics and videos are more common. 

Why the shift? The entirety of Facebook’s motivation can be decoded from [this 2011 quote](http://robhof.com/2011/04/20/1014/) by the company’s Chief Operating Officer, Sheryl Sandberg: 

> _“\[Facebook is the\] demand generation. We’re not really demand fulfillment, when you’ve already figured it out what you’re going to buy–that’s search. We’re demand generation, before you know you want something, before you know you’re interested in something.”_

Today, the vast majority of “demand creation” advertising is spent on television. In order to earn some of that market share, Facebook has to serve the same kind of function that television does: it has to build a product through which people spend many hours consuming media content (and personal content), and insert rich advertising alongside it.

When Sandberg said this in 2011, Facebook was, by revenue standards, a tiny company, and many people doubted whether it could make money. But Facebook _actually pulled off_ what Sandberg hinted at: the company became an advertising juggernaut focused on creating demand, similar to how television advertising operates.

This offers a glimpse into the motivations of Facebook, which now operates as if it’s the new television. Content is shared on the site, the company’s algorithms decide what’s shown, and Facebook serves ads. One difference is that while cable companies had to pay for TV shows and other programming, Facebook gets the content for free.

Another difference: In the broadcast television world, cable companies decide what is shown on television. In the “Facebook television” world, the broadcast is automated based on user behavior. The more people share something, the more likely it is that Facebook’s algorithms will show it.

In this new world of social content distribution, anyone can be a star. You just need to make content that people want to share. When you post something, ask yourself this question: Is this the kind of content that Facebook will want on its platform in the long term, given its goal to be a hub for quality content that rivals television?

**Why Do People Share?**

If we were to give you two case studies about how to do content marketing in the modern world, the first, which we’ve already mentioned, would be the OkCupid blog. The second would be The Oatmeal.

Started in 2009 by cartoonist Matthew Inman, The Oatmeal -- basically a comics and comedy writing blog -- isn’t _exactly_ the paradigm most businesses should follow. But it is a very good example of a site that (a) understands why people share, and (b) seamlessly bundles commercial projects with content.

With millions of Facebook fans and Twitter followers, The Oatmeal has amassed a gargantuan following. But what’s special about The Oatmeal is that when Inman creates something, people -- followers or not -- _really_ share it.

His most popular cartoons have over one million Facebook Likes each. When he wrote a comic requesting that the founder of Tesla Motors, Elon Musk, donate millions of dollars to fund a museum about inventor Nicola Tesla, it went viral. Elon Musk actually donated the money. 

Matthew Inman is an incredible cartoonist. But he’s not some naive artist who thinks, “If I make good cartoons, people will come to my website.” He makes good cartoons _and_ he thinks about the distribution mechanisms by which people will share them. Originally, he started making comics for SEO purposes -- specifically, to generate links for a dating website, Mingle2. This led him to explore why people share content. Later on, he gave presentations at conferences about how he comes up with ideas that people can relate to and want to share. He thinks about _why_ people will want to share what he publishes.

What we share on social networks is a form of self expression. What matters when we share something is _how it makes us feel_. When we go on vacations, we share flattering pictures of ourselves that hide our second chins and pictures of ourselves having fun. We also share media and content that reflects how we want to be perceived. What’s more, we often share things that we have reactions to: _I read something online and now I feel disgusted. Or surprised. Or in awe. Or smart._

In addressing why people share things, the first question to consider is: “What does the reader _express_ by sharing this?”

The second question is: “Can the reader share this given their own cognitive constraints?” 

A more practical way of thinking about this second question is by asking whether this piece of content can be summarized in a sentence (e.g. a Tweet). Cognitively, can someone read your content and then come up with something interesting to say about it? More often than not, after you read something, you have nothing to say about it. If that’s the case, you will not share it.

One of the most popular data reports we ever published is an analysis of how much it costs to rent an apartment in each of San Francisco’s neighborhoods. We programed web crawlers to check apartment websites to see how much landlords were charging for apartments, analyzed the data, and put together a report on the prices. We called the first report “The San Francisco Rent Explosion.”

It spread like wildfire, garnering over 29,000 Facebook shares and 300,000 views. Not bad for what was essentially an advertisement for our data services business!

We knew the report had a good chance of going viral because, before we even started gathering the data, we considered: (1) What is a person expressing by sharing this?, and (2) What can they say about it?

People shared the article for a variety of reasons. The average person said something like, “Rent prices in San Francisco are ridiculous!” or some other variation of outrage over the high cost of living in the city. There was also some pride hidden in the outrage. Sharing the article was a way of subtly saying, “I’ve found a way to live in the most expensive city in the world.” Other people shared the article to express thanks for the rent-controlled apartment they had, or to launch a diatribe against the pricing inefficiencies of having rent control in the city.

Whatever people’s motivations, the data, and how we presented it, allowed readers to express their feelings by sharing the article. Moreover, we wrote the article so readers would actually have something to say about it. Though it was the product of millions of data points and weeks of analysis, someone could look at it and say, in simple terms, “Whoa, the rent has increased a lot in San Francisco.” Unless someone can easily go through the process of digesting an article into a shareable nugget, the article won't be shared, no matter how great it is.

Sometimes we forget this lesson. Two of the best pieces of writing we ever produced, a primer on the digital currency Bitcoin, and a profile of a Dutch art forger, both got the The Bump. But they were not widely shared on Facebook and Twitter.

Both articles were excellent pieces, but there was nothing for people to say after reading these articles other than “that was a good article.” More often than not, people need something to sink their teeth into -- something that they condense into a short summary to share on social media.

The repeated heartbreak of having a great post flop has taught us to ask the question “Why will someone share this?” _before_ we start writing. Very often, we can’t come up with an answer, so we’ll refine the idea until we do.

**Will it play on Facebook?**

We’re not saying you should create content _just_ because you think it will go viral on Facebook. History is littered with companies that made content to game the Facebook algorithm and get clicks. Eventually, when Facebook wises up and changes its algorithm, they’re left with nothing. Moreover, if you create content for marketing purposes, you want to publish content that attracts people who are authentically interested in your company’s information. Don’t try to trick people into visiting your site with articles like “9 Cats that Totally Love Enterprise Software.” 

But asking the question “Will someone share this?” should help you turn your _good_ content into _sharable_ content, sometimes in very simple but very real ways.

Let’s do a little quiz. Which of these two images do you think would get more shares?

![](https://pix-media.priceonomics-media.com/blog/1085/image0sdf0.png)

or

![](https://pix-media.priceonomics-media.com/blog/867/diversity.png)

Okay, have your answer in mind? The answer is that the second image -- the chart that lists the most diverse cities in America -- will definitely get more shares. Though the map looks cooler and requires more work, it will be much less successful.

Why is the list better? Let’s use our framework of 1.) What does the reader express by sharing this, and 2.) What can they say about it?

The map is pretty cool. The reader will share it because it looks neat, and he feels smart when he figures it out. But what can he say about it besides, “some places are more diverse than other places in America”? That’s not a very interesting point, and it’s unlikely that most people will spend time making that point in a Facebook comment. Maybe the people who live in those 10 cities will share it, but even then, who wants to say they live in one of the whitest cities in America? 

The chart, on the other hand, is something that many people would find worth sharing. Who? How about people who live in Sacramento? Here’s a city that no one ever really considers the best at anything, but it’s ranked number one for an important metric. Potentially, anyone living in a city towards the top of this list will share it. This chart, by the way, got over 15,000 Facebook shares and 280,000 views on our site.

The ranked chart is better because it gives people something easy and insightful to say: “I can’t believe Detroit is so low in diversity”, or “I know you people don’t think Long Beach is cool, but actually, we are.” There are also a bazillion cities on the list, so you have a chance people from many cities to share it. Lastly, people can share a ranking about diversity without the same risk of looking like a White Supremacist if they shared the "White Cities in America."

If you want your content to be seen, it’s not enough to write good things. It’s also not enough to write good things that have a channel through which they can get The Bump. To drive a lot of traffic, you have to write good things that have a channel through which they can get The Bump, _and_ there has to be a reason for people who read it to share it.

**Does Twitter Matter?**

Yes, Twitter matters. But _why_ it matters is a little complicated.

If you look at Priceonomics analytics data, Twitter doesn’t seem particularly valuable: In 2014, Facebook sent approximately 10 times more traffic than Twitter to Priceonomics. Moreover, Twitter only made up infinitesimal amount of our traffic for the year. If that disappeared, it wouldn’t really move the needle on our stats.

And yet, Twitter is very important.

The writers at Priceonomics are all addicted to checking Twitter, even though Facebook sends much more traffic. Most of the sharing on Facebook is private, so it’s nearly impossible to follow the conversation about your work there. But Twitter is a _public_ messaging system. When you publish content, Twitter is addicting because you can see what people -- including journalists and other influential people -- are saying about your articles. 

If your goal is to get the media to write about your data, you’ll find that Twitter is more important than Facebook. For journalists, Twitter _is_ social media. So, if you’re going to make the information-rich content we talk about in this book, then merely making excellent content isn’t enough. Spreading it through a supernode isn’t enough. And making it sharable on Facebook isn’t enough. It’s important that it spreads on Twitter as well.

Does Google Plus matter? No, it does not! Other platforms like Tumblr, Instagram, Youtube and Pinterest matter for some types of content. Our 4,000-word, long-form analyses don’t work on these mediums. If you’re making short infographics, or beautiful pictures, or video-content, these mediums do matter though. But those forms of content are not Priceonomics’ expertise, so we will not focus on them here.

\*\*\*

Making great content that reaches lots of people is not easy. This is especially true if you’re writing about information and using it to promote your business. But it is possible, and we do it regularly.

If you write great content, make sure it gets The Bump. If you want it to really take off, it needs to spread on social media. 

That’s how content distribution works. 

**\*\*\***

There’s an important lesson about writing buried in this discussion of distribution. If you write a good article and your audience, after reading through it, can’t come up with a pithy, shareable takeaway, did you actually write something good? Most likely, the reason your essay is not shareable is that it’s not very good. Your prose may be beautiful, and your information interesting, but that isn’t enough. Unclear, poorly-organized writing is bad writing. 

Humans have short attention spans, especially on the internet. You need to grab their attention right away, write clearly, and follow an organized structure. Your article should have a _point_, and you must hammer that point home.

[![](https://pix-media.priceonomics-media.com/blog/1085/Screenshot2015-12-0821.21.29.png)](http://eepurl.com/bVruOT)

**Chapter V: The Writer’s Playbook**

An internet audience is a distracted audience. Most of your readers are surfing the web instead of working. So right away, they’re distracted from your content by their job. (Or maybe they’re distracted from their job by your content.) 

The internet itself is also a noisy environment. It used to be that just the elite media outlets like _The New Yorker_ and _The Economist_ made great content. Now, everyone is! There’s incredible long-form content, short-form content, videos, podcasts, lists, and everything else under the sun. There’s also a lot of crap. All of this online content is competing for people’s attention.

**A Punchy Intro**

Start every essay by punching your reader in the face. Obviously not literally, but you need to grab his or her attention. Assume that a reader has stumbled upon your article by chance, and is seconds away from closing or burying the tab forever. The introduction is your opportunity to pitch them on why they should read it.

Most content lives and dies by its introduction. Here’s one of the best introductions we’ve ever written, for a piece called “Diamonds are Bullshit,” an article about the economics of diamond engagement rings:

> American males enter adulthood through a peculiar rite of passage: they spend most of their savings on a shiny piece of rock. They could invest the money in assets that will compound over time and someday provide a nest egg. Instead, they trade that money for a diamond ring, which isn’t much of an asset at all. As soon as you leave the jeweler with a diamond, it loses over 50% of its value. 
> 
> Americans exchange diamond rings as part of the engagement process, because in 1938, De Beers decided that they would like us to. Prior to a stunningly successful marketing campaign in 1938, Americans occasionally exchanged engagement rings, but it wasn’t a pervasive occurrence. Not only is the demand for diamonds a marketing invention, but diamonds aren’t actually that rare. Only by carefully restricting the supply has De Beers kept the price of a diamond high.
> 
> Countless American dudes will attest that the societal obligation to furnish a diamond engagement ring is both stressful and expensive. But here’s the thing - this obligation only exists because the company that stands to profit from it willed it into existence.  
> 
> So here is a modest proposal: Let’s agree that diamonds are bullshit and reject their role in the marriage process. Let’s admit that as a society we got tricked for about century into coveting sparkling pieces of carbon, but that it’s time to end the nonsense.

This essay has been viewed over 700,000 times and shared nearly 150,000 times. Why is it a great introduction? Because it effectively makes the case that the reader should spend more time reading the article. 

First, it starts with an interesting sentence: _“American males enter adulthood through a peculiar rite of passage: they spend most of their savings on a shiny piece of rock.”_ It’s the proverbial “shot across the bow,” signaling that this is going to be an article that’s critical of the diamond industry.

Second, it gives the reader some idea of where the article is going. Our introductions always telegraph what the article is about so that readers know that if they invest some time in this article, they’ll learn something interesting. In this case, they’ll learn that the practice of exchanging diamond engagement rings is a marketing invention, and the history of how it happened.

Third, the introduction piques their interest. It inspires curiosity. How did this societal brainwashing happen?

The introduction is by far the most important part of the article. Without a great one, no one will read your article. The second most important part of the article is the conclusion.

The conclusion matters because it reminds the reader what she should take away from the piece. It provides, on a silver platter, sharable nuggets of information that would be a hit on Facebook or at a cocktail party. The conclusion is the time to remind the reader about the information she just learned and why it matters. Here’s the conclusion to the Diamonds essay:

> We covet diamonds in America for a simple reason: the company that stands to profit from diamond sales decided that we should. De Beers’ marketing campaign single handedly made diamond rings the measure of one’s success in America. Despite its complete lack of inherent value, the company manufactured an image of diamonds as a status symbol. And to keep the price of diamonds high, despite the abundance of new diamond finds, De Beers executed the most effective monopoly of the 20th century. Okay, we get it De Beers, you guys are really good at business! 
> 
> The purpose of this post is to point out that diamond engagement rings are a lie - they’re an invention of Madison Avenue and De Beers. This post has completely glossed over the sheer amount of human suffering that we’ve caused by believing this lie: conflict diamonds funding wars, supporting apartheid for decades with our money, and pillaging the earth to find shiny carbon. And while we’re on the subject, why is it that women need to be asked and presented with a ring in order to get married? Why can’t they ask and do the presenting?
> 
> Diamonds are not actually scarce, make a terrible investment, and are purely valuable as a status symbol.
> 
> Diamonds, to put it delicately, are bullshit.

This conclusion not only repackages the essay so the reader can remember what it was about, but it primes the reader on what he should share -- in this case, some variant on the phrase “Diamonds are bullshit.” It’s not an accident that this article is one of the most shared essays ever written about diamonds in the history of the internet. This essay was optimized for shareability, and the conclusion hammered home the shareable message.

The introduction to an essay matters, and the conclusion matters. The essay’s topic sentences are equally important. When editing our work, sometimes we just read the introduction, the conclusion, and every topic sentence. The essay should stand alone on these segments.

Why? Because this mimics how people skim articles, and you have to assume that people are skimming your content. Remember, they’re at work -- not slowly reading your blog post about your company’s data as if it’s a novel by their favorite author.

Moreover, the focus on topic sentences improves your writing. It prevents you from burying important points in the middle of a long paragraph. If there is an important point you are trying to make, you need to bring it to the front of a paragraph to increase the probability that the reader actually reads the sentence.

Those are the Priceonomics rules for writing clearly on the internet. Focus on the introduction, conclusion, and topic sentences. Not only will your writing become much clearer, but it will improve the likelihood that people will actually read, understand, and share your content.

**Tone & Viewpoint**

The tone of your writing should be conversational -- it should read as if spoken. We grow up writing essays in school that are the opposite of how you should write for the internet. A formal academic tone is both boring to read and does your reader a disservice by making it impenetrable. That’s not to say that you shouldn’t sound intelligent; rather, you should sound how an intelligent person talks. 

Another factor to consider is the viewpoint of the piece: who is it written by? At Priceonomics, we banned the word “I” from our blog. We write every article as if Priceonomics, the entity, has written it. As a result, when we refer to ourselves, it’s always as “we.” We only put a byline at the end of the article. Until then, the reader should think, “I’m reading an article by Priceonomics.”

Aside from _The Economist_, which does not indicate the authors of its articles, few sites have adopted this convention. We started writing this way because originally just one person wrote on our blog, but many people’s work created the infrastructure that produced our data.

The “no first person” perspective has its benefits. First, we never give our opinions, because the words “I think” are banished. When you can’t state your opinion, you have have to use facts to prove your ideas. This seemingly arbitrary decision to not use the word “I” forces us to do more research to illustrate a point.

Since our articles are from the perspective of the corporation Priceonomics, and not from an individual named Zack, Rosie, or Alex, fewer internet trolls spend time on our website. Internet trolls, or people that say mean things solely to be mean, need another person to troll. Trolling a data company called Priceonomics, or a publication called _The Economist_, is much less satisfying for a bully than launching a vile, ad-hominem attack against a person.

Finally, since every article is written from the perspective of Priceonomics, every Priceonomics holds our company’s good name and brand in his or her hands. They take this very seriously. “Priceonomics” would never be irrational, make sloppy mistakes, be mean, or cherry pick something out of context to make a cheap point. It’s just not something “Priceonomics” would do. If people on your team hold your viewpoint and reputation sacrosanct, this helps you produce high quality work. If people on your team publish things that will jeopardize your company’s reputation (through inflammatory or low-quality work), then content marketing might not be for you.

Our perspective of using “we” instead of “I” is part of the Priceonomics culture. We’re not saying you should do it, but it helps us strip opinions out of our articles, build a case using facts, and focus on being right. 

**Being Right Is Important**

Every time you publish something on the internet, a thousand people will try to prove you wrong. And that’s only if you’re lucky enough that lots of people see it. If you publish something involving data, every single assumption you make will be challenged, every calculation will be double checked, and many people will go out of their way to scrutinize your conclusions.

Here’s the scariest part, it’s very unlikely that you’re the world’s leading expert on something you just published. If your report is popular, real experts will likely read it. In that case, you had better be right.

For us, the consequences of being wrong, especially when we publish reports involving data from the web, would be catastrophic. We make occasional typos and minor errors. But we can’t reach the wrong conclusion by messing up the data, doing bad research, or misinterpreting something. Our business is getting people data and accurately analyzing it for our customers. Some of our customers are hedge funds that trade hundreds of millions of dollars based on our data. Others are tech companies that use our data for integral parts of their business. 

So it’s very important that we are not wrong. There are only three guidelines for the Priceonomics Blog: 1.) Bring new information to the world 2.) Be interesting, and 3.) Be right. None of these points are negotiable, but it’s okay if we write something that turns out to not be interesting, or if we write about information that a lot of people already knew about. But it’s really bad if we write something that is substantively wrong. So far, that hasn’t happened, but we live in fear of one day being very wrong about something.

There is a secret to minimizing your chances of making a catastrophic mistake: keep the scope of your argument narrow. In every post, we try to make just one really good point, and make sure that we are right about that point. If we write about San Francisco rent prices, we don’t also talk about rent control, the impact of the tech industry, or the city government’s zoning regulations. We just focus on one thing: what’s happening to the price.

It’s much easier to be right when you’re just thinking about one thing. You can “sanity check” whether the conclusions make sense. When you write a blog post, think of it as a war between you and all the people who want to disprove your conclusions. Don’t fight a multi-front war; instead pick one battle that you’ll win because you understand it very well and you’ve checked your work.

Having this approach has helped Priceonomics write about controversial topics and still be right. In one article, we asked, “Do Elite Colleges Discriminate Against Asians?” This was an explosive topic to discuss, and you should probably avoid topics like this unless you’re crazy like us.

We reduced the essay to a much more narrow argument: _Some__Asian students believe that they are being discriminated against; the evidence that’s publicly available is very limited but confirms this viewpoint; colleges, who have not always always been very nice to minorities, keep their admissions a black box by having opaque admissions criteria._

The way we scoped our argument was actually fairly airtight: _The available data suggests Asians face discrimination._ We just had to be scrupulously careful about surveying the current data that is public. Maybe colleges aren’t discriminating against Asians, but if they aren’t, they haven’t released any exonerating data.

There is an important distinction between “data suggests discrimination” and “there is discrimination.” The former invites a civil conversation about what kind of data is available and how to interpret that data. The latter invites a debate about whether you discrimination exists, whether it’s justified, and all sorts of arguments you do not want to have. The language you use matters because it circumscribes the scope of what you’re trying to prove. Be careful which words you choose.

**Editorial Process**

Creating content isn’t exactly a team effort. Long-form, information-driven articles need a spark provided by an individual. You can’t make them in committee meetings. You need to think of ideas, research them, write them, and refactor the product until it’s good. Writing is, most often, a solitary activity.

But publishing something that puts your company’s reputation on the line is a team activity. Writers need feedback. They need someone to put their assumptions through the ringer. They need someone to make sure the article is good. They need editors.

No one at Priceonomics had a background in how “real” journalists work with editors when we created this process. Instead, we made up our own process. Occasionally we work with “real” journalists who have done freelance pieces for our blog, and they’ve told us our editing process is at least as rigorous as national media outlets. Our process is amateurish, but we care about what we publish, so it’s rigorous.

Every Friday, we hold content meetings to discuss our ideas. Some of our ideas are for short blog posts; others are for ambitious long-form essays; and some are for even more ambitious data reports. After we agree that a topic is a good idea, that writer pursues her idea, sometimes with an engineer who will help get the data. The writer is on the hook for publishing one or two short pieces and one ambitious piece every week, and that is his or her full-time job.

For a given article, most of the the writing experience is a solitary process: conducting interviews, assembling secondary research, writing an outline, writing a first draft, and iterating until he is ready to share it with someone else.

Then, that story gets shared with the “editor”, the person who will tear the first draft to shreds. First drafts are never great. The editor needs to help improve the organization of the article, offer suggestions for making the writing clear, and, most importantly, aggressively push back on whether everything in the article is correct.

The Priceonomics editing process is pretty brutal, but it turns drafts into great articles that are factually accurate. On our blog, we once “fake edited” a famously erroneous _Newsweek_ article that claimed to have identified the creator of Bitcoin, who has chosen to remain anonymous. In actuality, this journalist had not discovered the mysterious inventor of Bitcoin, and the article would never have made it through the Priceonomics editing process. Here’s how we would have edited pieces of it:

> **Newsweek**: _\[He\] has the thousand-mile stare of someone who has gone weeks without sleep_
> 
> **Priceonomics Editor**: Seems unlikely that we would know if he’s been sleeping. Cut, even if this is just a metaphor.
> 
> **Newsweek**: _It seemed ludicrous that the man credited with inventing Bitcoin... would retreat to Los Angeles's San Gabriel foothills, hole up in the family home and leave his estimated $400 million of Bitcoin riches untouched._ 
> 
> _It seemed similarly implausible that Nakamoto's first response to my knocking at his door would be to call the cops._
> 
> **Priceonomics Editor**: False equivalence? It’s not similarly implausible. I'd call the cops if someone I didn’t know showed up at my remote home asking questions.
> 
> **Newsweek**:_Tacitly acknowledging his role in the Bitcoin project, he looks down, staring at the pavement and categorically refuses to answer questions._
> 
> **Priceonomics Editor**: Cut this. This is a pretty big leap in the logic. How do you know he’s “tacitly” admitting something because he’s looking at the ground? 

When _Newsweek_ published this article, internet commenters and other media outlets quickly debunked its claims. But _Newsweek_ never would have published it if an editor had demanded evidence for each claim.

The most valuable role of the editor at Priceonomics is to assess every statement in the draft and see that it makes sense. Does it stand to logic? Is it making an assertion that can be proved with data, or by other sources? The editor makes the article more interesting through suggestions about organization and word choice, but his or her most important job is making sure that writers don’t publish something that’s not right.

After the editor takes a hatchet to the writer’s draft, the writer implements the changes and makes the article great. It’s not easy, but after one round of harsh feedback, the second draft is usually quite good. The editor takes another look and signs off that the article is good to go.

Copy editing and finishing touches then take place. We fix typos, insert images into the article (with care taken that these images are open source, or that we have paid or asked for the right to use them), and tidy up charts and data visualizations. Another person looks at the article with a fresh set of eyes to make sure it’s good to go. 

We then upload the article in our Content Management System. We originally hosted the blog on Tumblr, but we later built a custom CMS that’s built into our internal administration system. Anything is fine for publishing -- Wordpress, Tumblr, Squarespace. We do most of our writing in Google Docs and only paste it into the CMS at the very end.

**Titles**

The last step before we hit “publish” is to choose a title for each post. Titles are probably the most talked about step of content creation. There are “link bait” titles that over-promise in the title but then under-deliver in the content. There are “curiosity gap” titles, which pique your interest by promising surprising information that is only revealed in the article. And of course there are “listicle” titles that promise to show you “33 insanely cute pictures of puppies that will brighten your day.”

We wish we could tell you that all that matters is having great ideas, but that’s not true. Titles are important for two reasons.

First, you’ll be distributing your content largely on social news sites and social networks, and the only aspects of your article that will appear there are the title and maybe a small picture and description. People who see your article on social channels have to decide to click on it pretty much solely based on the title. So, if your title is vague or boring, it’ll get fewer clicks. But if you focus too heavily on maximizing click through, you’ll end up with “click bait” titles that will reflect poorly on your company and make you feel bad about your integrity.

Second, titles matter because no one wants to share an article that makes him look lame or boring. Remember, sharing is a form of self-expression, and titles are the most visible part of the article when it’s shared. Your title should make people feel good about sharing it. When someone shares your article, the title should express the feeling they want to share.

Different publications have different philosophies on how to choose titles. Media sites that have mastered click bait, the curiosity gap, and/or silly lists get a a lot of traffic. We’ll tell you how _we_ think about titles. The Priceonomics way isn’t necessarily the best way -- it’s just our way, and what we know best.

Our goal with a title is twofold: it should honestly convey what the article is about, and it should emphasis the point that we think the reader will share.

To us, it’s important that the title be a summary of the content for both ethical and practical reasons. For example, we wrote an article about the economics of the food truck industry with the goal of attracting smart readers who would want to share it. We wanted the title to reflect well on them, so we titled the article “Food Truck Economics” instead of “These Two DJs Quit Their Jobs to Sell Food and You’ll Never Guess What Happened Next.”

The second title might attract more clicks, and possibly more shares, but it’s inconsistent with the brand we want for Priceonomics. When we write an article, our goal is to write the most definitive article ever written about that subject. As such, the title needs to _sound_ definitive. We want our articles to “age well” and still be relevant in twenty years -- and the title is a large part of that.

Obviously, you should write the entire article in a way that’s consistent with the image you want your company to present. But the title is where there is the greatest temptation to compromise yourself. You’ll see other companies getting lots of shares with their cool, breezy titles and think, “We should copy this best practice and we’ll get more traffic!” Don’t do it. Find a titling style that is consistent with the kind of information you are presenting and the kind of readers and customers you’re trying to attract.

Our second criteria for coming up with a title is that it should convey the point that a reader is mostly likely to share. After all, when a reader is sharing your article, she is really just sharing your title. For example, we recently published an article about the Monty Hall statistics problem and controversy ensued when Marilyn Vos Savant (the person with the highest IQ in the world) published the answer in her newspaper column (no one believe her, even though she was right).

We could have made the title about the Monty Hall problem and how baffling it is. Instead, our was, “The Time Everyone ‘Corrected’ the World’s Smartest Woman.” Why? We had a hunch that the most shareable point in the article was: “oh, another case of men erroneously insisting that they know best.” The article got over fifty thousand shares and half a million views. The title primed that kind of sharing.

**Voice**

“Voice” sounds like a general, cliché term. But it’s still worth considering. At Priceonomics, our voice is objective and data-driven. Readers of our blog often tell us, “Priceonomics is like a smart friend you can count on to tell you what’s up.” We didn’t set out to be this way; rather, it was a natural outgrowth of trying to write about data and share information. 

Having a voice means that you can bring a consistent lens to the topics you write about and share a common style across the writing staff. As we edit, a common critique is, “That doesn’t seem like a very Priceonomics thing to say” -- especially if the logic behind a point isn’t airtight, or if the language isn’t precise. Writing becomes a lot easier when everyone is on the the same page about how it’s supposed to work.

[**![](https://pix-media.priceonomics-media.com/blog/1085/Screenshot2015-12-0821.23.02.png)**](http://eepurl.com/bVruOT)

**Chapter VI: Writing Hacks**

In the immortal words of Yoda, “Do. Or do not. There is no try.” Commit to writing, and then just do it.

Conventional wisdom says you should write every day. No matter how big, how small, how good, how bad -- just build the muscle memory of writing. 

We do not espouse that view. While it may be a great way to start a personal writing project and write about personal opinions or experiences, it’s not the ideal way for a company to approach content.

In our view, a company that wants to master content marketing needs an early taste of success. As you start creating content, focus on making one thing that sets the world on fire. The avalanche of press citations, the influx of visitors, the commentary about your work on Twitter -- it’s all addictive. Once you taste success, you will be addicted to publishing information that is well-received.

We’re certainly addicted. Our website had 2,000 visitors a month when we published our first article. We had no idea what would happen. Then the article generated 10,000 views. We tried to recreate it...and we did! 

We published more articles that got 10,000 views and dozens of press mentions. Then, we wrote several articles that got 20,000 views. Then 50,000 views, 100,000 views, 500,000 views, and 1,500,000 views. Each view was a unique visit to our website. Now when we write a blog post that gets 10,000 views, it feels underwhelming. Like, “Meh, that’s just okay.” This is the sign of an addict: you need more of the same thing just to feel as good as you used to.

If you’re going to work at making content, you may as well become addicted to success. Most journalists, serious writers, and creators feel this way. You should too. The way to get addicted to writing popular content is to make really good stuff. To [measure the outcomes](https://priceonomics.com/content-tracker-by-priceonomics/), and then push yourself to make even better stuff.

During our first year of writing blog posts at Priceonomics, each article we published took at least 40 hours to create. We spent most of that time analyzing the data, cutting it up in different ways, and figuring out how to tell an interesting story with it. We only spent the last 10 hours making the charts look pretty and physically typing out and editing the blog post.

A lot of blood, sweat, and tears goes into making a really good blog post. So, here’s a tip: when you first start, don’t write every day. Spend 40 hours on your first post, and then make it succeed. Do whatever it takes to find the story that’s in the data to make it genuinely interesting.

The returns on writing something great are enormous; the returns on writing something average are zero. 

After you write that first home run blog post and get the hang of how to create great content, you have to do it again. It’s not like building a product; you have to start again from scratch. The good news is that it gets easier every time. If you make good things, people will start following you on Twitter and Facebook, and subscribing to your email list. Eventually, your audience will voluntarily help distribute your content.

But to get there, you have to start publishing more often. You’ll get better at writing data-driven reports, and soon enough, you’ll be able to produce them faster while maintaining quality. You’ll become familiar with writing interesting articles about your industry and the people in it. 

What’s the optimal amount of content to publish? As much as you can, while keeping it really good! We usually publish one article a day now. We have a staff of full-time writers who are talented and committed, and after three years of writing, we’re pretty good at this whole publishing thing now.

Still, it’s pretty hard to come up with ideas for articles. So we’ve come up with some “hacks” or tricks for coming up with and implementing good ideas.

As we’ve mentioned before, there are three nodes of information we like to focus on: data, industries, and people.

**Writing About Data**

We’ve written about so many different topics on the Priceonomics Blog that we’ve noticed that many of our articles share similar themes.

When you write about data -- ideally data that your company has proprietary access to -- it’s helpful to have themes. Here are some common themes we constantly turn to for our data-driven posts:

_A Ranked order list_

Come up with an interesting metric and rank the results in a list. Examples we’ve used in the past include the most “hipster” cities, where BlackBerry phones are most popular, the most expensive neighborhoods to live in, the most expensive bands to book, and countries that drink the most coffee.

Lists are interesting, and people share them because lists include information that’s important to them (where they live, their school, their favorite music). If you make a list of the top cities in America for raising a goat, people in every city in America will share that article because you mentioned their city: “Dubuque, Iowa, is the the 7th best place to raise a goat in America. I’m so proud of my town!”

That said, do not abuse the power of the list by writing about things that have nothing to do with your company, product, or industry. Come up with a great insight from your data, then find a way to put it in an interesting format that will help it spread. 

_How much does something cost?_

People are extremely interested in how much stuff costs. Our article about the price of booking a band has received 1.8 million views so far, and it continues to attract substantial attention more than a year later. We’ve written articles analyzing how much the resale value of an iPhone is affected if you crack the screen, how the price of wine is determined, how much office space costs for a startup, and how much apartments cost in various American cities.

Prices and costs are something that people are curious about and your company might have good information about: the prices of medical procedures, of various types of paper, of particularly important car parts, of getting a cavity filled, and so on. People are curious!

_Data that proves someone’s strongly held intuition, or that disproves a weakly held one_

People love to have their own biases confirmed by data. For example, if you write a data-driven article comparing the prices of used Android phone and iPhones, it will give someone an excuse to say something like, “_I always knew Android phones were worse than iPhones, and now I have the data to prove it!”_ Or, if you write something about city diversity, someone might use it to say, _“My city has a lot of things going for it that no one ever gives us credit for, now I have some data to prove it!”_

No amount of data will convince someone to reverse a strongly held position just by reading a blog post. At the same time, people like to see their non-important intuitions busted. Put differently, people like to be surprised (e.g.: Wait a minute, UPS trucks don’t left to save fuel?’ or, ‘Most Hollywood movies are tinted with either a hint or blue or orange?’, or “The McDonalds Monopoly Game was rigged?). 

_A surprising trend_

Unless something is accelerating or decelerating so fast that it is shocking, writing about a trend is hard to pull off. When we wrote about San Francisco rent prices, we called it “The San Francisco Rent Explosion” to hammer home the point that rents were going up really fast.

So, if you’re want to feature data that shows that cases of measles are increasing, or that the temperature of the planet is increasing, you have to find a way to show some context about why your data is contributing to a huge trend.

_The relationship between two things_

Do you ever wonder why so many charts are confined to 2x2 matrices? That’s because people love thinking about the relationship between two items. These kind of charts also take a little bit of thinking to understand, so it’s satisfying when you figure out what the data actually means. 

Of course, figuring what to plot on the x-axis and what to plot on the y-axis is the tricky part. Say you’re an ice cream shop. If you put the average daily temperature on the x-axis, and sales per day on the y-axis, that’s the story of your whole business in one chart! 

**Writing About an Industry: What Information Is Valuable to Other People?**

When you’re an expert in an industry, you have hard-won information about how it works, and this is valuable to other people. Much of what you think is common knowledge could be extremely helpful or interesting to someone else.

For example, when you work at a company that sells software for sales people to use, you likely know a lot about how a salesforce works, how to hire people, what a VP of sales does, how to get leads, what software can automate calling people, and how to close a prospect. This kind of information is valuable to someone who is looking to build up his or her sales organization, and would incidentally be a great potential customer for you.

You possess a unique understanding of your industry and are in a good position to competently research a topic that other people are curious about.

Another example: you’re a vacation rental property management company. What are some questions that potential customers of yours (homeowners who need someone to manage their properties) would have? Consider the following: How does one handle the the taxes on income from Airbnb when renting out a home? This is a complicated matter that every single one of your potential customers is grappling with. Why not speak to a few accountants, research the issue, and present the answer?

The right information at the right time can be incredibly valuable to someone. Never underestimate that your company has a tremendous knowledge base about its industry that could be helpful to other people.

**Writing About People: The Hero’s Journey**

You may not realize this, but pretty much every movie or book about an epic adventure follows the same pattern. Star Wars, Harry Potter, The Odyssey -- they’re all essentially the same story.

![](https://pix-media.priceonomics-media.com/blog/1085/monomyths1.jpg)

The theory of the “Hero’s Journey,” or “Monomyth,” was popularized by American mythologist Joseph Campbell in his book, _The Hero with A Thousand Faces_. In it, Campbell details the 17 steps every protagonist goes through in a typical story line. After a while, we realized we were subconsciously applying this cycle to many of the articles on the Priceonomics Blog.

Here’s the essence of the Hero’s Journey: The hero is just a regular person. One day, an incident causes the hero to start a journey. There is a huge problem the hero tries to solve. It looks like the hero is going to solve it, but he fails. After failing time after time, the hero perseveres and triumphs.

![](https://pix-media.priceonomics-media.com/blog/1085/Screenshot2015-12-0814.43.36.png)

This pattern is a framework to tell the story of every single entrepreneur, inventor, or everyday person. Each of us is on our little Hero’s Journey. 

We most often use this framework (subconsciously, mind you) when we’re telling the stories of how companies are founded. You should go to every single one of your customers and ask if you can profile how they were founded. They will all be flattered; you’ll have incredible content for your site; and you can showcase that you have actual customers.

Here’s the introduction from an article we wrote about a company that makes jellyfish tanks (notice the hero cycle at play):

> When Alex Andon got his first order for a $25,000 jellyfish tank installation, he was excited. He also had a problem. He didn’t know anything about jellyfish or how to make a jellyfish tank. He had a hunch that people wanted to keep jellyfish as pets, so he had created a test website and bought $100 in Google search ads. Lo and behold, his phone started ringing with enquiries and he got his first order for the $25,000 jellyfish tank.
> 
> Today, Alex’s company Jellyfish Art is the leading company in the jellyfish pet space. In fact, they’re pretty much the only company in the space. When they launched over four years ago, the only way to keep jellyfish at home was to pay a custom installer $10,000-$25,000. After starting as a custom installer, Alex later developed a desktop jellyfish tank that brought the price of jellyfish ownership down to $500. 
> 
> Along the way, he launched one of the first popular Kickstarter campaigns, received funding from Y Combinator, and created a market that didn’t exist before.
> 
> This is the story of Alex Andon and Jellyfish Art -- the world’s only jellyfish startup.

We might as well be talking about Luke Skywalker: the story is the same. You can write about your customers this way too, as well as inventors in your industry, key figures, and even employees at your company. 

**Finding Themes and Ideas**

You’ll find that it’s hard to figure out what to write. After banging our heads on the wall, we finally looked back at articles we had published in the past that were popular. That exercise revealed the themes we were hitting on over and over. So, when we need ideas, we look to this list of themes for inspiration:

The Underground Economy of X: _drugs, food carts, collecting cans_ 

Entrepreneur as a Hero: _jellyfish, Anyperk, Experiment.com_

The Rise and Fall of Y: _bowling, claymation_

X is Bullshit: _diamonds_, _wine, art, bottled water, mattresses_

The Weird History of Z: _wartime cats, timothy dexter_ 

Corporate Adventure Story: _Soros v. Bank of England, Porsche v. VW_

An Interesting Subculture: _Phish_

It Turns Out This Thing We All Believe is Wrong: _UPS trucks, movie posters_

A Story in One Chart: v_inyl records, diversity_

The Invention of Y: _Aeropress, Slinky, sliced bread, Slurpees_

These are some of the themes we’ve come up with at Priceonomics. We put together the list after the fact, but it helps with brainstorming. These are our own idiosyncratic themes, and we urge you to come up with your own.

**Coming up with Ideas**

Perhaps the most important part of writing is starting with a good idea. If you’re going to invest 40+ hours on a research report or blog post, you’re doomed if you put a lot of effort into an idea without legs. 

It would be even worse if you devoted a lot of time to an idea that could be offensive or cast your company in a bad light. This happens all the time, and it can be cataclysmic for a company. You should assume that anything potentially negative or insulting _will be construed that way_, and avoid it at all costs. You need good ideas, and you also need to avoid really bad ones. 

Coming up with good ideas takes some judgement, but you can build your judgement by reading great content. Pick one of the “Supernode” social news sites we listed earlier, hang out there, and get a feel for the types of articles that go viral. Go to a sub-Reddit, pick a subject that matters to you, and view the articles with the highest votes ever. What is it about those ideas that makes them spread?

This isn’t a difficult concept to grasp: write about great information in a way that is shareable, and make your content so great that everyone has to read it, including your target market. 

But what’s more difficult is execution. And to achieve this, you have to hire great people.

[![](https://pix-media.priceonomics-media.com/blog/1085/Screenshot2015-12-0821.25.59.png)](http://eepurl.com/bVruOT)

**Chapter VII: Hiring**

How hard do the writers who are in charge of your company’s blog work?

The writers at Priceonomics practically kill themselves to do good work. They routinely pull all-nighters to finish stories, spend weeks tracking down interviews, and write and rewrite drafts until they are truly great. Story ideas are proposed, shot down, debated and refined until we have really great topics. 

We can’t understate how much effort the writers at Priceonomics put into their work. This is key to our success: talented people expend effort in a system that rewards that effort. In this case, the reward is reaching millions of people (as well as helping generate revenue for the company). Writers want to reach people. If your blog is a ghost town, there is little incentive to make great things -- no one will ever find them!

Most of this book so far has been about creating the systems that make great content spread. But how do you find the talented, motivated people who will thrive in such a system?

There a lot of options out there for creating “content marketing,” but most of them are garbage. Many companies suggest the way to dominate at making content is to hire freelancers; this is not what you should do in the beginning.

Trust us: in order for your content to succeed, the people who make it have to really care about your business. They need to understand the information your company has and how it can be made interesting. Hiring a freelancer to “write content” for you will, at best, just fill your blog with interesting stuff that lacks the nuance of being related to the information your business produces. At worst, a freelancer will just write words on a page because you’re paying per word.

If someone doesn’t care deeply about your problems, don’t expect him to solve them. Someone at your company need to start working, invest those 40 hours in making one piece of content, and get good at it. If you’re a small startup, that means a founder of the company; if you’re a large company, that means someone who’s in charge of marketing.

So either you, or someone who really cares about the success of your company, should be in charge of making the content. Now, how do you find someone like that?

**The Hiring Process**

There are three things we ask for when we look for a writer to join the Priceonomics team: a resume of some sort, a writing sample, and a list topics the applicant would like to write about.

The resume is the least important of the three. We typically look at it for 15 seconds. We may scan it to see a candidate’s experience, but it’s not crucial compared to the rest of the application.

The writing sample is much more important. Can the applicant send you one thing that is clearly written and interesting? In our experience, maybe 5% of applicants can do this.

By far the most important part of the application is the ideas section. Most of the ideas people pitch just aren’t that exciting, interesting, or relevant to your company. When the right writer applies, you know it because their list of ideas just works.

When we hired our first writer, Alex Mayyasi, we brought him in for an interview based on the strength of the ideas in his application. Here’s what he suggested:

\*\*\*

_The Finances of Homeless Haight Street Kids_

I live on Haight Street & I've wanted to know since I moved in.

_Can Aging Wine Pay for Itself?_ 

A billionaire showing me his wine cellar once said the increasing value of his aging wine covered the cost of all the wine he drank each year. I want to run the numbers to see if he was drunk, a genius, or both. Is wine way too complicated for a price guide at this point?

_The Supply and Demand of War: How Much does an AK Cost Around the World?_ 

Data will be scarce, but with gun control in the news, this will be very pitch-able to the press. Obama is mulling gun control to prevent domestic gun violence, but what if we apply the same logic to reducing the global arms trade to prevent civil wars?

_What Could an American Kidney Market Look Like?_ 

Inspired by an op-ed by a friend of mine at GiveWell.

_Should You buy booze from a grocery store or liquor store?_ 

Anecdotal evidence says it's a price (grocery store) versus selection (liquor store) trade-off, but I want a data-driven answer. Even if this involves me going liquor store to liquor store and writing down prices. Or should I say, especially if it involves me going liquor store to liquor store.

_Coachella or Electric Daisy Carnival? The Best Value Music Festivals_

Synthesize based on ticket price, cost of accommodation, food & drink prices inside event, and ease of getting to event by driving or public transport

_Third Party Ticket Vendors. How to See the Game Without Getting Gouged_

I assume sites like StubHub are scum, driving up ticket prices to our favorite playoff games and T Swift concerts. I'd like to investigate the industry and see which sites inflate ticket prices the least and how alternatives like ebay stack up.

_What Should You (and What Should You Not) Buy at Ikea?_ 

I imagine this being posted in late May as a fake commencement address to graduating college seniors, advising them on whether to furnish their first apartments at Ikea. Reference will likely be made to 30 Rock on the Ikea curse.

_Food Truck-onomics._ 

How much does a food truck save financially versus a restaurant's costs? Do we or do we not see those savings in the Bay Area food truck scene? Are hipster foodies driving up prices? Am I a hipster foodie?

_Stuff Rich People Like_

Soethby's lists expected prices for items along with a description and a record of the final bid at auction. It would be interesting to see if any 1% insights can be gleaned -- maybe by looking at whether auctions succeeded or failed at meeting their expected output before, during, and after the financial crisis.

_What Should the "New Girl" Apartment Really Look Like?_ 

Have you ever watched a sitcom like _Friends_ or _New Girl_,and thought that there is no way in hell that the characters could really afford that apartment? Well, I have. And between screenshots of the apartment, rent listings, salary information, and Priceonomics data on home furnishings, I should be able to prove it.

\*\*\*

After receiving hundreds of writer applications, this was by far the best set of ideas about topics for the Priceonomics blog. The ideas were what jumped out in this application. After we hired Alex, a number of these topics become very successful Priceonomics blog posts.

But a bunch of good ideas is not enough evidence for you to hire someone. If a candidate passes the screen of a good application, then we will schedule a 30-minute Skype interview to discuss the applicant’s ideas, background, and any previous writing experience.

While only 1% or so of applicants will get one of these interviews, about 50% of them make it to the next stage. This is the most important stage, and we highly recommend you do it.

We then pay the applicant to do a freelance article for us. Jointly, we pick an idea, tell him or her how much we will pay, and come up with a process for working together. Then, the applicant goes off and writes the article.

A number of surprising things happen during this process. First, about one third of the candidates disappear and don’t complete the assignment. That is good information to find out before hiring someone!

Another third of the applicants produce work that is substantially worse than you were expecting. These are people who looked great on paper, had great writing samples, and aced the interview, but the quality of work they produced simply wasn’t there.

The final third of applicants do a good job. That’s not to say the articles they write are incredible, but they show promise. We work very hard with these folks on editing their articles, then publish them on Priceonomics. That gives us and the candidate a very a good sense of what it’s like to work together.

In Alex’s case, we paid him to research the first topic he suggested about youth homelessness in the Haight-Ashbury neighborhood of San Francisco, and he published a post called “The Street Kids of San Francisco.” Here is the introduction:

> In San Francisco’s Haight-Ashbury district, youth homelessness is impossible to overlook. Spend any time in the neighborhood, and you’ll notice that hundreds of “street kids” – young homeless in their teens and twenties toting large backpacks and sleeping bags – call Haight Street and Golden Gate Park their home.
> 
> In this neighborhood, street kids are a constant presence. They hang out on most corners, asking for spare change or leftover food, smoking weed, and occasionally proffering drugs for sale. The area is famous as the epicenter of the 1960s hippy movement, but the street kids mix uneasily with today’s gentrified Haight - an area of $1,500 rents, trendy nightspots, and Google buses whisking engineers away for the workday. Even in a city known for homelessness, they stand out as unusually young and numerous.
> 
> Encountering San Francisco’s street kids can prompt uncomfortable reactions: guilt and pity at the sight of their poverty, unease with their drug dealing, even nervousness and fear due to their unpredictability, uncouth appearances, and penchant for vicious-looking dogs. It is easiest to ignore them, and most locals admit that they have never had a conversation with them.
> 
> At Priceonomics, we wanted to know more about these street kids that we see everyday but know almost nothing about. Who are they? How do they make money, and how do they get by? Why are they here? So, we decided to ask them.
> 
> Our initial hypothesis was that life on Haight Street would be a grim, Dickensian hellhole. Instead, we discovered a world of misunderstood, modern-day nomads, blithely toeing the line between poverty, drug dealing, and hippie nirvana. Most of them seemed to be having fun.

This ended up being a phenomenally popular article, but that was beside the point. First, the drafts that Alex produced showed potential. Second, we worked together on making them better so that when he published the article, it was really good. We got a window into working with each other. 

After the publication of the article, we had Alex come in for a “final round” of interviews where we spent half a day interviewing him. But this was mostly a formality to check that he was nice person; the real interview was the freelance writing assignment. We made Alex an offer, and he accepted.

We’ve run this process with every writer we’ve hired. Most of the freelance pieces that the writers have produced for us weren’t as popular as this one, but that was okay. What was more important was that we could tell they had talent and that they would get their hands dirty doing research. Whatever you pay the applicant to do a freelance piece for you will be the best money you’ve ever spent in the recruiting process.

In our experience, it takes about two months for a new writer to hit his or her stride. We’ve hired some of the most talented, ambitious, and brilliant writers you can imagine, and it still takes time for them to find their voice, acquire a sense of which topics will work, and figure out how to get the information they need.

It’s a lot of work to find the right people and train them. But there is a reason that most corporate blogs get no traffic: if it were easy, everyone would be doing it! Throughout this book, we’ve argued that there is a fundamental change going on, and that there is a level playing field for content creators. The time to take advantage of that and pour your heart into making awesome content is now. 

Your company blog is competing against content from media sites jammed full of ads, annoying popups, and desperate monetization schemes. Your content is funded by product sales, which can be an enormous advantage.

[**![](https://pix-media.priceonomics-media.com/blog/1085/Screenshot2015-12-0821.27.31.png)**](http://eepurl.com/bVruOT)

**Chapter VIII: Why This All Matters**

The biggest traffic day ever at Priceonomics coincided with our most profitable sales day -- but these two milestones weren’t as related as one would think. Let’s explore what happened.

That day, a Priceonomics writer dashed off a short blog post called “How Much Does it Cost to Book your Favorite Band?” The post -- a price list of the approximate booking costs of hundreds of bands -- wasn’t our most exciting piece of writing, but it went more viral than anything we’d ever posted.

We didn’t realize it at the time, but the article had very favorable characteristics for being shared. It contained novel pricing information that people were curious about, and it mentioned so many bands that everyone who read the post could identify with at least one of them. Hundreds of thousands of people shared the article on social networks with some variant of the phrase, “Hey, my favorite band \[The Counting Crows, Slipknot, Justin Bieber, etc.\] costs $XXX amount to book for a private concert! Who wants to chip in and book them?” The article was our first post to generate over one million visits, and it is well on its way to two million visits. 

That same day, practically every hedge fund in New York contacted Priceonomics about our data crawling service. Huh?

Earlier in the day, Google had updated its search results algorithm, and the change had reeked havoc on many websites. One of those was RetailMeNot, a publically traded company that specialized in publishing coupon codes that consumers could use to get discounts on online purchases. (Companies provided RetailMeNot with a referral fee, which served as its main source of revenue).

The update to Google’s algorithm was rumored to have hit RetailMeNot’s SEO (search engine optimization) particularly hard. If the rumor was true, that meant the company's main source of traffic (Google) had been negatively impacted, leading to less revenue.

Practically every analyst on Wall Street Googled the term “RetailMeNot SEO” to get a sense of how the company was doing in Google’s search results. And what was the top result? An article by Priceonomics called “The SEO Dominance of RetailMeNot.”

Like almost all of our articles, this post shared information on something not well known. But, quite explicitly, this article advertised the data acquisition services of Priceonomics. And even though it was an advertisement for our company, the article had gone viral. Many news sources covered it, and it was therefore the top result in Google Search. Here’s the introduction to the piece:

> You’re about to buy a delightful pair of pants from your favorite online store when you notice a small box near the checkout button labelled “Promo Code.” Hmmm, you think, I was about about to pay full price for these slacks like a complete sucker when there are discounts to be had. Miffed, you do a quick Google search for coupon codes.
> 
> When you search for an online coupon, more often than not, you’ll land on the promo code aggregation site RetailMeNot. RetailMeNot is a publicly traded company that is worth $1.8BN after its IPO last year. In 2013, the company did over $200 million in revenue; RetailMeNot has turned coupon codes into big business.
> 
> According to company filings, RetailMeNot currently gets 63% of its traffic from search engines and 96% of its revenue from affiliate commissions on sales. The key factor in RetailMeNot’s success is reliably topping the Google search results for coupon code related queries, and then turning that traffic into referral fees. 
> 
> Given Google’s dominance in search, conquering its results page can be incredibly lucrative. But in RetailMeNot’s case, it’s also a lot of eggs in one basket. A shift in the Google search algorithm could cause the entire company to collapse. Traffic would plummet, leading to declining revenues and a flailing stock price. 
> 
> **Priceonomics Data Services analyzes SEO (search engine optimization) risk of publicly traded companies for our hedge fund customers.** We crawl the web to see how these companies’ search engine results are improving or deteriorating. For SEO-driven businesses like Zillow, Yelp, and TripAdvisor, this is an important leading indicator of their fortunes.
> 
> Since RetailMeNot is an SEO-driven company par excellence, we thought it would be interesting to showcase some of the work we do. Let’s review 1) The clever way RetailMeNot makes money 2) Just how important search engine traffic is to its business, and 3) Exactly how dominant the company is at winning the SEO game for coupon codes. 

The second to last paragraph (emphasis added), explicitly lays out that we are selling this service that crawls search results and figures out how companies are performing. The conclusion of the article also provides a link to get in touch with us if you want to sign up for our service.

So when Google changed its algorithm, everyone on Wall Street found our article (ironically, by doing a Google search), and we were flooded with customer enquiries. Many of them became highly lucrative customers of our data services. 

But how did our article about RetailMeNot stand out in the noise and vast sea of the internet?

First, most of our content doesn’t directly promote our services. It’s just great content made for our regular readers. We would have no regular readers if we only wrote about Priceonomics Data Services in every blog post. Because we have regular readers, the occasional articles that promote our services actually get read.

Second, the article was a great source of information. We explained the RetailMeNot business model and provided data illustrating our points better than anyone in the world. There is literally no one else that could have both explained the business and had the technical capabilities to get the right data. Only Priceonomics could do it.

Third, this article, which was an explicit advertisement for our business, was better than the average article on Priceonomics -- and the average article quality is really high. If you’re going to write content that even remotely promotes your business, the quality of it has to be higher than competing information from professional news sites. Your blog needs to be great, and blog posts that promote your business need to be even greater. 

Finally, we chose a topic that promoted our business, but we had no bias about the topic. Our data showed RetailMeNot dominating SEO. Then, when Google’s algorithm changed, we showed how that negatively impacted RetailMeNot’s SEO results. We had no bias as to whether or not the company was doing well, as it didn’t matter for the service we were offering. As a result, the reader could trust us.

So, this was our highest traffic day of the year because of the article about band prices, and also our most lucrative day of the year because the RetailMeNot post paid off. But were the events related?

Yes. The RetailMeNot article was immensely popular only because we had earned a reputation for distributing high quality information. People shared it on social networks, it rose to the top of the Hacker News, and earned it’s way to the top of the Google search results.

The insane amount of effort our team puts into writing great work pays off when we write things that make us money -- and those articles, which are advertisements for us, are popular as a result. But you can’t have a high-quality source of information if all you're doing is promoting yourself.

**Decisions Must Be Made**

When you start off with content marketing, we suggest you use your company’s data to see if you can create data-driven stories. Why?  If you can pull that off, then you know how to write content that promotes your company and is interesting. If you can’t pull that off, it’s probably not even worth your time to focus on content.

But if you can write content that promotes your company and is awesome, then you have some questions to answer. 

First, how much of your content should talk about information that promotes your company’s services and how much of your content should just be interesting information about your industry and the people in it? At Priceonomics, roughly 10% of our posts are advertisements for our service; the other 90% aren’t.

Remember, we are crazy about writing great content. We’d do it for free if that were economically viable. You need to pick your own ratio and it probably shouldn’t look like ours.

Regardless of the ratio you choose, the less promotional your content feels, the larger audience the you can build up. If you write promotional content that is better than non-promotional content and distribute it to a large audience, those articles will actually be immensely popular. In our articles that promote Priceonomics (like the RetailMeNot example above), most of the post is great data and analysis. It just so happens that data is a product of the service we are offering.

Second, you have to figure out how to not be biased. No one’s going to read your company’s blog if it’s about topics that you obviously have a vested interest in. For example, Verizon recently had an ill-conceived plan to launch a tech website that would subtly influence the debate on Net Neutrality. Similarly, the oil behemoth Chevron is funding a newspaper that it hopes will be the main source of journalism in the town where it’s headquartered. There is obviously going to be a bias. In both the Verizon and Chevron cases, the public backlash has been immense.

We write about information in which we have no particular bias. There are some topics where we might have an interest in promoting a viewpoint because that viewpoint would be advantageous to our business. We avoid those topics. Instead, we write about the information that is _produced by_ our service. We suggest you do the same.

**Should Content Marketers Feel Icky?**

Articles you read online are considered “content.” Yet books, food, fashion, and art are not. What makes one form of media content and the other art?

At Priceonomics, we believe that content is media that is basically free to consume but that is bundled with something else that costs money. Almost every media site follows this formula: produce articles that are free to read, and bundle them with advertisements that cost money to buy.

By this definition, a book isn’t content, because you pay for it -- it’s something you’re buying directly. Same with a painting, or a piece of clothing, or a meal. If you’re not paying for it directly, it’s content. 

Most content sites bundle their work with advertising. That’s their prerogative. At Priceonomics, we have a different model: we bundle our content with things we sell. Content attracts an audience to our website, and we sell things to a percentage of them. 

Content marketing has, in some regards, an icky reputation along the lines of, “This company is writing interesting content so it can manipulate me into buying things, or thinking good things about them.” To us, that’s actually less icky than the common perception of traditional content sites: “This company is writing interesting things so they can sell my attention and personal data to other companies.” 

Not only could you argue that the advertising-supported content business model is morally gross, but it’s becoming a commodity. Anyone, not just established media sites, can write content -- and they are all trying to get advertisers to put ads on their pages. There are so many sites out there generating so many page views that the price of an advertisement keeps plummeting. That’s great for advertisers, but it’s become nearly impossible to make a living by making content unless you just churn it out and indulge in the basest practices for getting page views.

In our view, maybe journalists who write content to draw an audience that can be sold to advertisers are the ones who should feel icky. Why not instead write content and make money mostly by selling valuable things?

Incredible content companies are funded this way. Bloomberg has far-reaching journalism operations, and it’s funded mostly by selling subscriptions to its financial terminals. Humans of New York is a Facebook and Tumblr page of pictures, but it’s funded by selling books. The Oatmeal is a free online comic, but most of its revenue is from selling physical manifestations of the comics (books, games, posters). Priceonomics is a blog about information, and most of our revenue is from selling specific kinds of valuable information and services to businesses.

Are the above companies content marketers, or people who make great content who fund that content by selling things? Our take is that it would be an insult to the quality of the content we produce to put commodity ads on it. So we instead pursue a more thoughtful business model of selling products.

Listen, we’ve maintained throughout this book that we’re crazy. We truly believe the content we write is _better_ than what you’d find in a newspaper or magazine because we’re not trying to drum up page views for the sake of displaying ads. We’ve cautioned you against copying us too much, because we’re fanatical about what we do. But here is something we do suggest you emulate: take an insane amount of pride in your work. Don’t consider yourself a “lowly content marketer”; consider yourself someone who makes great content that happens to be monetized through product sales instead of advertisements. Your peers are writers at _The New Yorker_, _WSJ_, and _The__Economist_, and you should strive to make your work better than theirs in your area of expertise.

And that’s the most important point in this book: your work has to be great to stand out. But if you make something great, your content is on level playing field with the work produced by the most prestigious news sites and analysts in the world.

Moreover, you have an advantage, because you have access to information through your company and your industry experience. Great information spreads, but only if you design it to spread. It needs to be packaged into a great story, and you need to anticipate the channels through which it will spread. Being talented and doing a good job isn’t enough; you need to have a plan.

Write about information. Make it great. Have a plan.

\*\*\*

_This post was written by [Rohin Dhar](https://twitter.com/rohindhar). Thanks to the whole Priceonomics team for reading, editing, and creating the knowledge in this handbook._

_Want to learn more about the Priceonomics Data Studio, our content marketing agency that helps turn company data into awesome content? Send us a message [**here**](https://priceonomics.com/contact/) or at [**info@priceonomics.com**](mailto:info@priceonomics.com)._ 

_To download all the chapter summaries and a "one-page writer's checklist", [**click here**](http://eepurl.com/bVruOT)._

* * *

* * *
