> Want to learn how to create SEO content? In this guide, we’ll teach you all you need to know, from keyword research to SEO copywriting!

# How to Write SEO Content (That Ranks #1 in 2020) - Apollo Digital
Writing content is easy.

Writing high quality, authoritative, SEO content that ranks #1 on Google is NOT.

As someone who delves into SEO, you probably have some experience with this:

_You write up a quick blog post._

_You use Yoast to optimize the whole thing for search._

_You hit publish, do some backlink outreach, and start waiting for those sweet rankings to kick in._

_Except, they don’t._

Days, weeks, months pass and nothing happens.

> “_Well, I guess SEO is all luck after all!_” - You giving up on SEO, 2020.

Except, it’s not. To even stand a chance at ranking for something competitive, you’ll need to create really good SEO content. We’re here to teach you how.

In this guide, we’re going to cover:

*   **[Keyword Research.](#keyword-research)** Before you even start writing content, you need a solid SEO strategy behind it. We’re going to teach you how to do keyword research fast and simple.
*   **[How (and Why) to Create Content Outlines \[+Template & Real-Life Example\].](#content-outlines)** With SEO content, you can’t just write whatever and expect it to rank. You need to write with the Googler’s intent in mind. Meaning, what YOU write should be what THEY’RE looking for.
*   **[SEO Copywriting 101 - How to Write Good Content.](#seo-copywriting)** At the end of the day, SEO writing is still writing. We’re going to teach you how to create interesting, engaging content.
*   **[Making Reading Fun Again with Content User Experience (UX).](#content-ux)** Even if your content is the most interesting, insightful information in the world, the reader’s going to drop unless it’s well-formatted. We’re going to teach you how to make your content more engaging by improving the formatting and layout.
*   **[Create a Content Black hole with Interlinking.](#interlinking)** You want your blog to be a black hole: the Googler lands on one of your articles and then ends up reading the rest of your blog for the next 6 hours. We’re going to explain how to make that happen with a proper interlinking strategy.

How to Do Keyword Research (the Easy Way)
-----------------------------------------

The first step in any SEO strategy is **keyword research**.

Here’s how to do that fast and easy.

### Step #1 - Create an SEO Sheet

Start off by creating a Google sheet - this is going to double as your content calendar.

You can either create the sheet from scratch or just use our [ready template](https://docs.google.com/spreadsheets/d/1OVP7u1bxe7A9nlkQXgHUtM50MtJrWFzJ73i7EVZ12z0/edit?usp=sharing).

![seo content sheet](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

Split the sheet by these columns:

*   **Keyword**
*   **LSI Keywords -** keywords semantically related to the main keyword We’ll get to these in just a bit.
*   **Publishing Stage** \- is the post in draft, editing, or ready to be published?
*   **Article title**
*   **Link**
*   **Post Type** - is it a landing page? An article? A comparison guide? Depending on the type, you might want a different formatting and a different writer.
*   **Keyword Volume** - # of searches per month.
*   **Cost Per Click (CPC)** \- how much would this keyword cost if you were bidding on it? The higher the CPC, the more likely it is for the keyword to be well-converting. After all, if your competition is paying top dollar, chances are, they’re making money from it.
*   **Topic Cluster** \- the category for the blog post.
*   **Comments** \- any specific instructions you might have for your writing team about this topic.

### Step #2 - Borrow Ideas from Competitors

You could start off by creating your entire keyword strategy from scratch - looking up what your customers are looking for, finding similar keywords, and so on.

But why do that if someone already did all this research for you: your competitors.

From our experience, 90% of the keywords you’ll target are a mix of the keywords your top 3-5 competitors are targeting.

So, you can just borrow some ideas from them, collect all of them in one sheet, and voila! You have a very comprehensive SEO strategy.

Now, let’s go through this whole process step by step. Let’s say, for example, you’re in the **project management niche**.

With a bit of Googling, you’ll see that SmartSheet is dominating a ton of project management keywords.

So, plug SmartSheet in your favorite SEO tool (ours is SEMrush) and see what you get...

![semrush screenshots](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

Now, go through the first 10-20 pages and extract all the relevant keywords to your SEO sheet.

Done? Good!

Now, go to the “Domain Overview” page for SmartSheet...

![smartsheet on semrush](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

...And look up their top SEO competitors in the “Competitive Positioning Map.”

![semrush competitive positioning ](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

Pick 1-2 of the competitors in the same niche, and extract their keywords.

Keep in mind, though, that not all competitors listed are going to be relevant. What this graph means is that these companies compete over **similar keywords,** not that they’re actual competitors.

In our case, we wouldn’t use Vertex42, for example, as they do Excel Templates, and only a handful of their keywords are relevant for a PM software solution.

On the other hand, template.net is mainly about resume templates, so that’s not exactly what we need either.

Projectmanager.com, however, is a perfect fit - it’s a PM software mainly ranking for relevant keywords within the niche.

![project manager semrush results](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

Check their top ranking keywords and extract the more relevant keywords as a .CSV or .XLS file.

Now, check the competitors for projectmanager.com, and repeat the same steps.

Keep going until you’ve covered most keywords in your niche.

### Step #3 - Do Some Manual Research

At this point, your SEO sheet should be around **90% done**.

To get this to 100%, you’ll have to do some manual work.

Run your keywords through [UberSuggest](https://neilpatel.com/ubersuggest/) and it’s going to help you find similar keyword ideas:

![ubersuggest screenshot](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

Once you get to the point where you’re running out of ideas, you can just call it a day with the sheet and get to writing.

Your SEO sheet doesn’t have to be exhaustive since the beginning. You’re always going to discover and add new keywords along the way.

How to Use LSI Keywords
-----------------------

Once you’ve got a comprehensive keyword sheet, it’s time to fill in the LSI keywords.

LSI keywords are sub-topics related to your main keyword. Mentioning them in your article helps Google understand the context behind your article.

Let’s say, for example, you’re writing about content marketing. Some of the LSI keywords could be “content marketing examples” and “types of content marketing.”

As a rule of thumb, we’d recommend mentioning LSI keywords 1-3 times in your article, and when relevant, as headers.

To find LSI keywords for your topic, simply Google your keyword, scroll down, and check “Searches related to your keyword:”

![lsi keywords on google](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

Or, if you want a more comprehensive list, you can use a tool like [LSI Graph](https://lsigraph.com/).

![lsi keywords on lsi graph](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

Run each of your keywords through LSI graph, and add all relevant LSIs to your SEO sheet.

How (and Why) to Create Content Outlines
----------------------------------------

Before you start writing, it’s a good idea to think of **WHAT** you’re going to write.

Sure, you can wing it - come up with some random content related to the keyword, turn it into an article, and call it a day. This, however, won’t get you rankings.

Before you start writing any given piece of content, we’d recommend you to create an outline.

In a nutshell, an SEO content outline is a list of all topics and subtopics you need to cover for your article to be better than everything that currently ranks for the given keyword.

Whether you’re writing the content yourself or working with a team of writers, an outline ensures that you’re going in the right direction.

To give you a better idea of how an outline works, you can check out an [example here](https://docs.google.com/document/d/1uYkBicKRWEzWuJc_AKlJdK22v3TgbnzrF3j42UdBTrk/edit?usp=sharing).

![technical seo outline screenshot](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

As you can see, the outline is very specific. It mentions **exactly** what the writer has to talk about in each section.

So, how do you create a solid content outline?

Here’s our step-by-step process…

### Step #1 - Basic Structure & Content

Let’s say you’re writing about **Business Process Management (BPM)** \- a very high-competition keyword, high volume (3,600 monthly searches), and high CPC ($35 CPC).

If you’re a company offering BPM software, ranking for this keyword could drive tons of qualified leads to your website.

For those of you who don’t know much about enterprise software, BPM software is a system that helps you digitize your processes. You map a process using the software, and it ensures that the process is executed right and on-time.

If you went through the top 5 search results for this keyword...

![business process management results](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

You’d discover that most of these pages cover:

*   What is BPM?
*   Is BPM the same as task management?
*   Why is BPM used
*   How do you do BPM?
*   What types of BPM are there
*   Benefits of BPM
*   How to use BPM for specific departments
*   How to pick a BPM tool
*   Upsell for some specific software

Borrow all the main topics you’d think are important for your users and use it to create a general structure for your article.

Now, at this point, you might be tempted to call it a day and just do what your competition is doing.

That’s not enough, though. You want YOUR content to be significantly better than whatever is currently ranking.

So, the next step is…

### Step #2 - Find More Ideas with LSI Keywords

If you Google “Business Process Management,” you’ll see that some related search phrases are:

![business process management lsi keywords](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

While some of these are not relevant (pdf, certification), others can be very good ideas for content to include in your article. Specifically:

*   BPM Examples
*   Five steps in BPM
*   BPM Life cycle
*   Principles of BPM
*   BPM Integration

Then, if you’d check LSIgraphs, you’d find pretty much the same…

![business process management lsi graph](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

Take all these LSI keywords, and add them to your outline.

### Step #3 - Find Your Reader’s Pain-Points on Quora

To really create good content, it needs to be addressing your reader’s pain points.

The best way to do this is through Quora.com. People ask questions about anything there. Just look up your keyword, and voila!

![business process management quora](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

You will have all you need to know.

Some of the most common questions we found are:

*   What is Business Process Management?
*   What’s the Basecamp of BPM?
*   What is a good hosted piece of business process management software?
*   What are the benefits of using BPM?
*   How do small businesses use BPM?
*   Why is BPM important?
*   What problem does BPM solve?
*   What’s the difference between workflow software and BPM?

You can also check which answers have the most upvotes, so you know what kind of an answer your audience expects.

### Step #4 - Dive Deeper with Reddit

Now, depending on the topic, you could also look up some questions on Reddit.

This is usually more useful for more popular topics (you know, not associated with enterprise software?).

Let’s say, for example, you’re creating an article on “**How to write a cover letter**.”

If you Google’d: “site:reddit.com cover letter”

![cover letter search results](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

You’d find a ton of discussions on the topic (which you can incorporate in your article).

### Step #5 - Putting it all Together

Finally, you take the structure you compiled in step #1 and add all the other points you discovered in steps #2 through #5.

Nice job, you’re good to go!

Following the above process, here’s what our outline for “Business Process Management” would look like:

[Link.](https://docs.google.com/document/d/1YryE6ExoaINr-I3ISGUO8BKbYALflw4oaP9p9wNLMn0/edit?usp=sharing)

![business process management outline screenshot](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

...And there you have it!

Now, all you have to do is give this outline to the right writer, and the final piece will have a good shot at ranking.

SEO Copywriting 101 - SEO Content Writing Tips
----------------------------------------------

Now, this is the hardest part, especially if you don’t have much experience with writing.

While you can learn SEO best practices overnight, you can’t just become a good writer by reading an article.

With the right tips and tricks, though, you can definitely improve your writing skills.

In this section, we’re going to teach you how to write better SEO content.

### Tip #1 - Write For Your Audience

Keep in mind the target audience for the article, and tailor your style accordingly.

Are you writing for 20-30 year old, hipster designers?

Use a casual style, make pop culture references, jokes, and so on.

On the other hand, if you’re writing for CXOs and senior management, you’d want to go for a more formal and serious tone.

### Tip #2 - Keep Your Audience’s Knowledge in Mind

Ask yourself: how much does your reader know about the topic you’re writing about?

Let’s say, for example, you’re trying to rank for “keyword research.”

It’s safe to say that your readers are just getting started with SEO and want to learn everything from the basics, to top industry-leading strategies.

On the other hand, if you’re writing about “backlink strategies,” it’s safe to assume that the reader knows what’s a backlink (so you can skip over the basics).

### Tip #3 - Grammarly & Hemingway Are Your Friends

This one’s pretty much a must: always run your SEO content through Grammarly and Hemingway.

Grammarly helps you catch all of those pesky typos that managed to sneak past.

Hemingway, on the other hand, makes sure your content is easy to read by spotting:

*   Passive voice
*   Complex phrases
*   Hard & very hard to read sentences

![Hemingway screenshot](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

Create Convincing Headlines
---------------------------

Your headline plays a huge role in whether your SEO content ranks or not.

Having an enticing headline can significantly boost the % of people who click your link over the others, which in turn, affects your rankings.

Here’s how, exactly, this works:

Let’s say your article is ranked #4, and your CTR is 20%.

If the average CTR for ranking #4 is 8%, Google will know that your article probably deserves a better ranking.

And you want to hear some good news?

Writing killer headlines is **_easy_**.

Heck, it’s not just easy, it’s formulaic.

When coming up with headlines for clients, we combine the following…

*   **Keyword** \- As a given, the keyword itself has to go in the title. Otherwise, why would the Googler even click your result?
*   **Numbers** \- \[mention how numbers drive clicks + how odd numbers are better\]
    *   Top 51 Tips on How to do X
    *   101+ Productivity Tips to Get Things Done
    *   How to Make a Resume \[99+ Real-Life Examples\]
*   **Results** \- The result of the keyword. What’s going to happen if they carry out the action correctly? For example:
    *   How to Write a Cover Letter \[to Land the Job\]
    *   How to Write Good Headlines & 2x Conversions
*   **Year** \- For some specific topics, timeliness matters. So, you can include the year in the headline. For example:
    *   39+ SEO Techniques \[Updated for 2020\]
    *   21+ Business Process Management Tools for 2020.

Sounds simple, right?

As a best practice, come up with 4-5 different headlines and pick the one that sticks with you the most.

As for length, we’d recommend going for 50 - 60 characters. The limit is around 70, but if you keep it under 60, you can expect [90% of your titles to display properly](https://moz.com/learn/seo/title-tag).

![serp snippet optimization tool screenshot](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

You should also keep track of your CTR on an on-going basis through Google Search Console.

Even if your headline sounds amazing in theory, you might learn that your audience doesn’t think the same way.

If your CTR is low for a specific position, it might be time to try something else.

Not sure what’s a “good” CTR? Here are some averages per positions, courtesy of [WordStream](https://moz.com/blog/higher-organic-click-through-conversion-rates-rankings):

![organic search average ctr chart](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

Keep in mind, though, that you shouldn’t change your article headline more often than once or twice a month. After all, you need to wait for a reasonable timeframe to see if your changes had an impact or not.

If you want to test more headlines in a much shorter time frame, here’s what you can do:

Set up search ads for the keyword with a minimum budget, see which headline performs better as an ad, and use the winner as the headline for the article!

Not sure what’s the “minimum budget?” Here’s how to figure that out.

First, check what’s the average CPC for the keyword you want to rank for. Then, multiply that by the number of clicks you want to buy.

A reasonable amount would be around 50 to 100. So, if your CPC is 2 USD, that’s be $100 - $200 ad spend in testing.

Or, check out our [PPC calculator](https://docs.google.com/spreadsheets/d/1j4holxUnYFEw3YB2eBJJHF6Lx_Sau907riIyzBtbWng/edit?usp=sharing). Input your numbers, and you’ll see how much you’d need to spend to get the data you need.

[![ppc sheet screenshot](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)](https://docs.google.com/spreadsheets/d/1j4holxUnYFEw3YB2eBJJHF6Lx_Sau907riIyzBtbWng/edit?usp=sharing)

How to Win at Content UX
------------------------

When you hear the term “UX,” you’re probably thinking of software, right?

Well, that’s not necessarily the case. User Experience applies to pretty much every aspect of the web - including your blog posts.

Here are some tips on how to improve your blog’s readability...

### Blog Formatting Checklist

The #1 thing you need to do is make sure the blog itself is formatted right.

Look at the following example. How likely is it for you to actually put the effort into it and read it?

![bad content ux example](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

[Image by Backlinko](https://backlinko.com/hub/content/content-strategy)

Now, compare that to something you find on a top marketing blog, like Backlinko or GrowthSprout:

![good content ux example](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

See the difference?

Here are all the formatting edits you have to make to make your blog 10x more readable:

*   Apply a 60-65% content width for your blog pages
*   Pick a good-looking font. We’d recommend Montserrat, PT Sans, and Roboto. Alternatively, you can also check out your favorite blogs, see which fonts they’re using, and do the same.
*   Use a reasonable font size. Most top blogs use font size ranging from **16 pt** to **22 pt**.
*   Use 2 - 3 sentence paragraphs. This makes your content a lot easier to read. No one likes blocky, gigantic paragraphs.

### Use (Relevant) Images When Possible

The (right) picture is worth a thousand words.

Including images in your blog posts can help you convey ideas better, as well as break up large chunks of text.

There’s a reason most online marketing bloggers advocate [using images as often as possible](https://www.shoutmeloud.com/4-ways-how-images-enhance-your-blog.html).

Keep in mind, though, that you should only add images that are relevant to whatever you’re writing about.

There’s never a reason to add a stock photo of _Office People Smiling_ in the middle of a blog post!

### Upgrade Your SEO Content With Custom CSS Boxes

You’ve probably noticed the example boxes and pro tip boxes scattered all over this article.

Well, we recommend using them to pretty much all of our clients.

Custom CSS graphics create a much better reading experience and allow you to plug in specific bits of information here and there (even if the context doesn’t allow it).

As a given, depending on your industry, you might want to use different CSS boxes. You wouldn’t use “Case Study,” for example, if you’re writing about “how to lose weight.”

Some of our favorite CSS boxes are…

*   Pro Tip

Let’s say you’re explaining a specific, step-by-step topic, and then all of a sudden, you have to mention a best practice or two without breaking the flow.

That’s when you use a pro-tip box.

![pro tip box](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

*   Template Box

For all copy-paste freebies. For example, email templates.

![template box](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

Turn Your Blog into a Black Hole With Interlinking
--------------------------------------------------

The longer visitors stick around your website, the better rankings you’ll get.

So, you want all your content pieces to be well-linked to each other.

Whenever someone lands on your link, you want them to read the entire page, AND end up browsing the rest of your pages.

The “**how**” here is pretty straightforward: whenever you have articles related to each other, make sure they’re linked in a very convincing way.

To give you a good idea of how to do it right, though, we’ll cover a practical example...

#### \[Case Study\] TV Tropes

![](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

Ever heard of TV Tropes?

They’re the world’s [#1 wiki on pop culture tropes](https://tvtropes.org/).

If you’ve ever visited their website, you’ve probably experienced this: you land on a single article, and then before you know it, you’ve spent the past 5 hours reading through everything on their website.

So, how do they do this?

All of their content pieces are linked together extremely well.

![tvtropes batman screenshot](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

Each piece of content is linked to **5 - 15+ related articles**, making it pretty much impossible **not** to binge read.

And to no one’s surprise, TV Tropes is doing extremely well with SEO (**over 6 million** in monthly organic traffic)...

![semrush results](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

Conclusion
----------

...and that’s a wrap.

We hope you found this guide practical.

If you liked our approach to SEO content, there’s a lot more to come! Sign up for our newsletter to stay updated.

We’re not going to send you anything that’s not going to **rock your socks off**, that’s a promise!

Now, back to you. Do you have any more questions on how to write SEO content? Let us know in the comments, and we’ll give you some tips!