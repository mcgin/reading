> Want to learn how to grow your blog from 0 to 200K monthly visitors in under 2 years? Check out our SEO case study on helping Tallyfy.

# SEO Case Study - 0 to 200,000 Monthly Organic Traffic
Want to get better rankings on Google?

Can’t blame you.

When done right, SEO can be a game-changer...

*   You rank for top keywords
*   Leads come knocking on YOUR door (instead of the other way around)
*   Sales go through the roof

Unless you have a lot of experience with SEO, though, the road to getting there can seem very uncertain…

...which is why we created this guide - a complete step-by-step SEO case study on how we took Tallyfy, a SaaS process management software, from **~8,000** monthly traffic to **about 200,000** in less than 2 years.

![SEO case study - monthly traffic growth graph](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)
--------------------------------------------------------------------------------------------------------------------------------

Currently, Tallyfy content is ranking for some very competitive keywords including:

![SEO Case Study: Top ranking keywords semrush](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

...and some other well-converting, high-CPC keywords...

Keyword

Volume

Ranking

CPC

Business Process Analysis

720

1

$7.32

BPR

8,100

4

$12.28

Business Automation Software

70

1

$25.12

BPM Solutions

590

3

$20.13

Business Process Modeling

1,600

2

$14.43

If Tallyfy wanted to advertise on all the keywords they’re ranking for right now, they’d be spending over $369,000 a month.

So, want to know what’s the secret sauce that we used to deliver these killer results for Tallyfy?

Here’s a step-by-step breakdown of everything we did:

But before we get into all that, let’s give you some context...

SEO Case Study - Intro to Tallyfy
---------------------------------

Tallyfy, a SaaS startup based in St. Louis, is a [workflow and business process management (BPM) software](https://tallyfy.com/).

![Tallyfy Homepage](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

When we started working together, Tallyfy was in heavy development mode. Their #1 priority was to create a killer product while focusing on user acquisition later on.

There were a ton of high-traffic, well-converting keywords in the BPM niche…

*   Business process management - 4.400 monthly traffic
*   Sop - 49,500 monthly traffic
*   Workflow diagram - 3,600 monthly traffic
*   Process analysis - 2,900 monthly traffic
*   Business process reengineering / BPR - 8,100+ monthly traffic
*   Process improvement tools - 480 monthly traffic
*   Workflow management software - 880 monthly traffic
*   And more.

So, SEO was THE perfect marketing channel.

In mid-2017, when we started working with Tallyfy, they already had an SEO strategy in place.

They had a keyword tracking sheet, which looked something like this...

![Example keyword research sheet](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

And a team of writers creating content.

While they had a good start, there was a lot to improve in their SEO strategy - and that’s where we come in.

Tallyfy hired us to...

*   Audit their keyword and content strategy
*   Oversee their team of freelance writers and ensure they create content that ranks
*   Create and publish up to 10 content pieces per month

...With the overarching goal of reaching 50k monthly traffic or more within the year (which we beat 2x). Here’s how we did it…

Step #1 - Auditing Existing Keyword Strategy
--------------------------------------------

As we mentioned before, when we started working with Tallyfy, they already had done their keyword research.

They had around 300 keywords covering several different niches.

Some relevant:

*   Project management
*   Process management
*   Workflow management

Others, not so much:

*   Content marketing
*   SEO
*   Computer science

So, the first thing we did was to **root out all irrelevant keywords**. Anything that wasn’t associated with process management had to go.

When getting started with SEO, we always recommend our clients to stick to 1 - 2 niches at most.

Sure, ranking on SEO topics could get Tallyfy SOME relevant traffic, but the chances of ranking on these keywords were extremely slim.

Google keeps your website’s niche in mind. Are you writing a lot about project management (PM)? You’re probably a project management blog.

Your PM articles might rank extremely well, but if you, out of nowhere, publish a post about SEO, it won’t rank.

Another issue that we found was that some [keywords were cannibalizing each other](https://www.oncrawl.com/oncrawl-seo-thoughts/what-is-keyword-cannibalization/).

Here’s what we mean by that.

Tallyfy had 3-4 different variations of the keyword “process management,” each with a different article.

*   Business process management
*   Process management
*   BPM

All 3 of these keywords mean literally the same thing, and Google knows this.

So when the Google Bot crawls the website, it gets confused - which of these 3 articles is the MAIN article? Which one should it rank?

So, for all such cases, we combined the different keywords into one main keyword.

In the above example, we created a [comprehensive guide to Business Process Management](https://tallyfy.com/guides/business-process-management-bpm/) and created 301 redirects from the other articles to this new page.

#### **PRO TIP**

Not sure what’s a 301 redirect? Here’s a primer.

There are 2 types of page redirects:

1.  **301 - Permanent redirect.** Google knows that the page is now defunct, and all backlinks pointed towards it now count for the page you redirected to.
2.  **302 - The redirect is temporary.** For example, you’re A/B testing 2 pages by redirecting half of the traffic to another page.

So, when unpublishing your blog posts, you want to use 301 redirects.

Once we fixed up the existing keyword sheet, we did some competitive keyword research.

We ran Process.st, Kissflow, and several other competitors through SEMrush and extracted 100 - 200 new keyword ideas…

![](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

Finally, we divided all keywords into 3 priority categories…

1.  **Easy wins -** These keywords are medium to high traffic, low competition, and high CPC. They’re the highest priority content pieces, so they are to be written ASAP.
2.  **Rewarding challenges** \- These keywords are harder to rank, but can lead to insane results. They’re also medium to high traffic, high CPC, but are a lot more competitive (and hard to rank for, if you’re just starting out with SEO). As such, they were a secondary priority.
3.  **Nice-to-haves** \- Pretty much everything else goes here. These keywords are worth pursuing, but they’re not as important as the other 2 priorities. They’re usually low to medium volume/relevance / CPC.

Step #2 - Content Audit
-----------------------

As we mentioned before, when we stepped in, Tallyfy already had a writing team creating content.

They had approximately 100 published blog posts, and the writers were pumping out new content on a weekly basis.

So, the next step was to audit all existing content and see if we were going in the right direction.

We discovered that the main issues with existing content were...

*   **Content wasn’t aligned with the keyword search intent**. For example, the keyword was “business process management,” and the article was about the benefits of process management (and not about business process management in general).
*   **Some content were opinion pieces (instead of educational)**. More often than not, opinion pieces don’t rank on Google. For example - let’s say you want to rank on the keyword “Resume.” What **WOULD** rank is either a resume builder or a guide on how to make a resume. What would **NOT** rank is an opinion piece on how The Resume is Dead (Use This Instead).

So, our next step was to figure out what to do with all this content (and get the most out of them).

We went through all the published blog posts and divided them into the following categories:

*   **Great** - These articles are pretty spot-on, they might just need a couple of edits.
*   **Salvageable** - The article probably won’t rank for the keyword, but it can be refactored into something better (and parts of the existing content piece can be reused).
*   **Unsalvageable** - The content piece really misses the mark, and can’t be reused.

We kept all the “great” content pieces, and eventually revamped everything that was “salvageable.”

As for “**unsalvageable**” content pieces, we either rewrote them, or 301 redirected them to better content pieces.

Step #3 - New Content Marketing Direction
-----------------------------------------

After we’d audited all existing content, we had to make sure that Tallyfy’s writing team would create better content from then on.

The main issue was that the writers weren’t SEO specialists: they knew how to write good content, but good content is NOT always the same as SEO content.

So, to give them a push in the right direction, we implemented 2 strategies:

*   Creating content outlines. For each article assigned to a writer, we gave them an outline that covered all the main info they had to cover in the article.
*   Established writer guidelines that explained all the main requirements for writing good SEO content.

Here’s how we did each...

### Creating Content Outlines

The main difference between SEO content and generic content is that the first is written with user search intent in mind.

You need to keep in mind what the Googler is looking for when they search for any given keyword. Then, you create content based on that intent.

Most writers who aren’t that experienced with SEO mess up here. They create content that THEY think should rank, without considering what the Googler is actually looking for.

At the time, Tallyfy’s writing team was making this exact mistake.

To fix this, we started creating outlines for each keyword. The outline had info on EXACTLY what topics the writer was supposed to cover in order for this article to rank.

To give you a practical example of what our outlines looked like, you can check out this one we created for the keyword “Technical SEO:”

[Link.](https://docs.google.com/document/d/1scJEjANJY61X1uZpv7TPQln0mLZODGUaHMEhtGelYdI/edit?usp=sharing)

### Establishing Writer Guidelines

The outlines helped with the concept - it made sure that the writer wrote the right content for the corresponding search term.

We also had to ensure that they wrote it the right way. I.e. create content that’s interesting/easy to read, optimized for search, and so on.

For this, we created a set of guidelines to keep the writers on track...

#### **Tallyfy's Writer Guidelines**

*   Use 3-4 sentences per paragraph. This makes the content more bite-sized and easier to read.
*   Use visual content when possible. Think, graphs, charts, and any other types of illustrations.
*   DON’T ever use generic stock photos (office people smiling). They don’t add any value and are just distracting.
*   When picking which article to write from the SEO sheet, prioritize by:
    *   1st Priority: Easy wins
    *   2nd Priority: Rewarding challenges
    *   3rd Priority: Nice to haves
*   For each content piece, follow the outline to the T. Don’t add or subtract any of the sections.
*   Avoid passive voice when possible.
*   Keep your target audience in mind. For most articles, it’s going to be middle-aged executives reading up on BPM or BPI. So, maintain a serious, semi-formal tone.
*   Don’t ever fluff. The reader is an expert, they’ll know if you’re making something up.
*   Use custom formatting (process boxes, pro tip boxes, interlinking CSS, etc.) when possible.
*   For SEO, follow the YoastSEO instructions to the T.
*   When you’re done with the article, run it through Hemingway and Grammarly.

As a general thing, we always recommend our clients to start off their content marketing with a set of writer guidelines.

This ensures that the writers you hire really understand what you’re looking for.

You can’t just find a random writer on Upwork and ask them to do their magic - they need to know what they’re supposed to help you with, and how.

Step #4 - Ongoing Interlinking
------------------------------

Proper interlinking is an essential part of any SEO strategy.

New to SEO?

An internal link is a link from one page on a given domain to another. Think, backlinks, but from your website to your website.

“How important can THAT be,” you might ask.

Well, NinjaOutreach managed to [increase their organic traffic by 40%](https://ninjaoutreach.com/internal-linking-case-study/) JUST by improving its internal links.

So, our task at Tallyfy was:

1.  Ensure that new content links to existing posts
2.  Improving internal linking for existing pages and posts
3.  Making sure that new posts are linked to from older content pieces

The 1st was easy enough to accomplish - we added a clause in the writer guidelines…

_“For each new content piece, link to other relevant Tallyfy articles. I.e. You’re writing about BPM, you’d link to all associated topics:_

*   _Business process improvement_
*   _Bpm tools_
*   _Process mapping_
*   _and so on...__“_

To make sure that the writers did this right, we also started giving them a list of keywords they needed to mention (and link to) in each article.

Now, we had to deal with task #2 - interlinking all existing web pages. Here’s the exact step-by-step process we followed...

1\. Pick an article to interlink. For example, let’s take “[business process improvement](https://tallyfy.com/business-process-improvement-bpi/).”

2\. Google its keyword on the Tallyfy domain with the following query:

`site:tallyfy.com “[keyword]”`

So in our case, that’s:

`site: tallyfy.com “business process improvement”`

![](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

3\. Go through all of these pages and link the article you’re currently interlinking. In our case, we’d open each of these, Control + F “process improvement,” and link to the process improvement article.

4\. Do this for all related phrases. For example, “business process improvement” means the same as “BPI” and “improve process.”

![](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

Once we were done with the initial interlinking run (i.e. ALL existing articles were properly linked to each other), we set up a process for interlinking new content.

Whenever we published a blog post, we carried out the exact process we mentioned before.

#### **BEST PRACTICE**

To make sure that we were doing interlinking as best as we could, we did a bi-annual check-up. I.e. we went through ALL the published content and ensured that they were linked to all relevant articles.

Step #5 - Improving Blog Visuals & Readability
----------------------------------------------

Content readability matters.

After all, even if you write the most insightful blog post in the world, but it looks like this…

![](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

Image caption: Image courtesy of [UX Planet](https://uxplanet.org/common-webpage-design-mistakes-59eed9831bd7?gi=33fc1b08109f).

...No one’s going to read it.

Before we started working with Tallyfy, their blog post format was very plain.

Long paragraphs of text, combined with a TON of blatantly stock images, doesn’t make for a good reading experience.

To fix this, we did 2 things…

1.  Banned stock photos and encouraged writers to use relevant graphs, charts, tables, etc. For that, we used Draw.io (for charts and graphs) and Canva (for anything more graphic).
2.  Created custom CSS boxes. Graphic boxes created with CSS to make generic text more visual. Here are a couple of examples...

### **Process Expert Tips**

![](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

The main point of these boxes is that they allow you to mention a random (but important) fact associated with the topic without breaking the flow.

They can also be used to highlight a very important point that you want to draw attention to.

### **Interlinking CSS**

![](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

We already talked about the importance of interlinking.

In order to make our top articles more “clickable,” we created a custom interlinking format. It’s a simple CSS change that helps highlight the sentence by adding an icon, increasing the font size and font-weight.

We used this format for specific situations. For example, in the above example, we’re talking about workflow and BPM software.

Before moving on to the next point in the article, we let the reader know that they can find a comparison guide to different BPM tools on the Tallyfy blog.

This is a LOT more clickable than if we just linked “bpm tools” somewhere randomly in the guide.

### **Process Boxes**

![](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

Most of the content pieces we created had practical examples of real-life processes.

So, to make it look better than just simple bullet points, we created a custom flex-box with CSS to nest them all in. The best thing about it? It’s responsive.

This looks a ton better than bullets, and on top of that, we used it as an upsell for the Tallyfy software.

Step #5 - Keep Track and Improve Headlines
------------------------------------------

Article headline plays a huge role in whether the content is going to rank or not. Here’s how that works…

Let’s say your article is ranked #4 with an average CTR of 20%.

Google benchmarks YOUR average CTR to that of your competition in the same ranking. I.e. your article ranked #4 has a 20% CTR, while your competitors have 12%. This means that your content is more relevant, and hence, should rank higher (as long as other SEO metrics say the same).

So, we kept track of article CTRs through Google Search Console and made adjustments when needed.

Whenever the CTR for any given article was lower than the position CTR average…

![Organic Search Query Data - CTR vs. Ranking](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

We changed the headline, tracked it for 2 - 4 weeks and saw whether that would lead to an improvement. If it did not, we kept testing new headlines until we found something that worked.

Step #6 - Continuous Monitoring and Improvement
-----------------------------------------------

You can never just “finish” your SEO.

You need to continuously monitor your progress and make improvements and adjustments when possible.

We kept track of all our rankings to ensure that the content we were publishing was ranking.

If any given content link wasn’t ranking, we followed the following checklist to find the reason:

*   Is the content as comprehensive as it could be? is there anything we could add?
    *   If not, refactor the article and make improvements. I.e. make it more visual or comprehensive, add more information, etc.
*   Is the content matching the keyword it's supposed to rank on?
    *   If not, rewrite the article with the keyword in mind.
*   Is the content interlinked across the website?
    *   If not, do an interlinking run for the article.
*   Is the article headline “clickable?"
    *   If not, test other headlines.
*   Does it have the right amount / quality of backlinks? if the competition has 500+ on a page, and we have 2, we’re probably not going to rank
    *   Do some backlink outreach for the article.

Conclusion
----------

And that’s a wrap!

We hope you loved our SEO case study.

It wasn’t too complex, right? What we did wasn’t rocket science:

We just followed all the industry-leading best practices, which led us to drive **amazing** results for Tallyfy.

Want your business to **WIN** at SEO? We’d love to help!

Get in touch with us through our [contact page](https://apollodigital.io/contact).

  Want to learn more about SEO or digital marketing in general? Follow our blog for some of the best content you’ll ever read!